import React from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Routes from "./routes/Routes";

// Styles
import "./styles/global.scss";
import "react-responsive-carousel/lib/styles/carousel.min.css";

function App() {
  return (
    <>
      <Routes />
      <ToastContainer />
    </>
  );
}

export default App;
