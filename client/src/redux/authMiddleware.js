import { logout } from "./slices/user/thunks";

const authMiddleware = ({ dispatch }) => (next) => (action) => {
  if (action.payload === "Unauthorized") {
    dispatch(logout());
  } else {
    next(action);
  }
};

export default authMiddleware;
