import { createAction } from "@reduxjs/toolkit";

// Online
export const addOnlineUser = createAction("room/addOnlineUser");
export const removeOnlineUser = createAction("room/removeOnlineUser");
