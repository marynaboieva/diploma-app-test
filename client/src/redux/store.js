import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";

import rootReducer from "./rootReducer";
// Middleware
import authMiddleware from "./authMiddleware";

const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware().concat(authMiddleware),
});

if (process.env.NODE_ENV === "development" && module.hot) {
  module.hot.accept("./rootReducer", () => {
    const newRootReducer = require("./rootReducer").default;
    store.replaceReducer(newRootReducer);
  });
}

export default store;
