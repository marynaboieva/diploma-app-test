import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
// Actions
import {
  getRequests,
  createRequest,
  cancelRequest,
  acceptRequest,
  declineRequest,
} from "./thunks";

export const requestsAdapter = createEntityAdapter({
  selectId: (request) => request._id,
});

const initialState = { error: null, loading: "idle" };

const requestsSlice = createSlice({
  name: "friends.requests",
  initialState: requestsAdapter.getInitialState(initialState),
  reducers: {
    addRequest: (state, action) =>
      requestsAdapter.upsertOne(state, action.payload),
    removeRequest: (state, action) =>
      requestsAdapter.removeOne(state, action.payload),

    reset: () => requestsAdapter.getInitialState(initialState),
  },
  extraReducers: {
    [String(getRequests.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      loading: "rejected",
    }),
    [String(getRequests.pending)]: (state) => ({
      ...state,
      loading: "pending",
    }),
    [String(getRequests.fulfilled)]: (state, action) => {
      requestsAdapter.setAll(state, action.payload);
      state.error = null;
      state.loading = "fulfilled";
    },

    [String(createRequest.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      submit: "rejected",
    }),

    [String(createRequest.fulfilled)]: (state, action) => {
      state.error = null;
      requestsAdapter.upsertOne(state, action.payload);
    },

    [String(cancelRequest.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
    }),
    [String(cancelRequest.fulfilled)]: (state, action) => {
      state.error = null;
      requestsAdapter.removeOne(state, action.payload);
    },

    [String(acceptRequest.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
    }),
    [String(acceptRequest.fulfilled)]: (state, action) => {
      state.error = null;
      requestsAdapter.removeOne(state, action.payload);
    },

    [String(declineRequest.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
    }),
    [String(declineRequest.fulfilled)]: (state, action) => {
      state.error = null;
      requestsAdapter.removeOne(state, action.payload);
    },
  },
});

export const { reset, addRequest, removeRequest } = requestsSlice.actions;
export default requestsSlice.reducer;
