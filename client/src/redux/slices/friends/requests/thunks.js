import { createAsyncThunk } from "@reduxjs/toolkit";
// Services
import BaseAPIService from "../../../../services/BaseAPIService";

export const getRequests = createAsyncThunk(
  "friends.requests/getRequests",
  async (_, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: "requests/",
        method: "get",
      });
      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const createRequest = createAsyncThunk(
  "friends.requests/createRequest",
  async (id, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: `requests/`,
        method: "post",
        data:{id}
      });
      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const cancelRequest = createAsyncThunk(
  "friends.requests/cancelRequest",
  async (_id, { rejectWithValue }) => {
    try {
      await BaseAPIService.request({
        url: `/requests/${_id}`,
        method: "delete",
      });
      return _id;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const acceptRequest = createAsyncThunk(
  "friends.requests/acceptRequest",
  async (_id, { rejectWithValue }) => {
    try {
      await BaseAPIService.request({
        url: `requests/${_id}/accept`,
        method: "patch",
      });
      return _id;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const declineRequest = createAsyncThunk(
  "friends.requests/declineRequest",
  async (_id, { rejectWithValue }) => {
    try {
      await BaseAPIService.request({
        url: `requests/${_id}/decline`,
        method: "patch",
      });
      return _id;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);
