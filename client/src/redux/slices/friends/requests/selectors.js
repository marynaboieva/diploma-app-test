import { requestsAdapter } from "./requestsSlice";

export const { selectById, selectAll } = requestsAdapter.getSelectors(
  (state) => state.friends.requests
);

export const selectRequestsState = (state) => state.friends.requests;
