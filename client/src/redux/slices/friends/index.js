import { combineReducers } from "@reduxjs/toolkit";
// Slices
import friendsListReducer from "./list/friendsListSlice";
import requestsReducer from "./requests/requestsSlice";
import searchReducer from "./search/searchSlice";

const friendsReducer = combineReducers({
  list: friendsListReducer,
  requests: requestsReducer,
  search: searchReducer,
});

export default friendsReducer;
