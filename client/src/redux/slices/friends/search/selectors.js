import { usersAdapter } from "./searchSlice";

export const { selectById, selectAll } = usersAdapter.getSelectors(
  (state) => state.friends.search
);

export const selectSearchState = (state) => state.friends.search;
