import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
// Actions
import { searchFriends } from "./thunks";

export const usersAdapter = createEntityAdapter({
  selectId: (room) => room._id,
});

const initialState = {
  username: "",
  page: 0,
  limit: 10,
  total: 0,
  error: null,
  loading: "idle",
};

const searchSlice = createSlice({
  name: "friends.search",
  initialState: usersAdapter.getInitialState(initialState),
  reducers: {
    setUsername: (state, action) => ({
      ...state,
      page: 0,
      username: action.payload,
    }),
    setLimit: (state, action) => ({ ...state, limit: action.payload }),
    setPage: (state, action) => ({ ...state, page: action.payload }),
    reset: () => usersAdapter.getInitialState(initialState),
  },
  extraReducers: {
    [String(searchFriends.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      loading: "rejected",
    }),
    [String(searchFriends.pending)]: (state) => ({
      ...state,
      loading: "pending",
    }),
    [String(searchFriends.fulfilled)]: (state, action) => {
      const { count, users } = action.payload;
      usersAdapter.setAll(state, users);
      state.total = count;
      state.error = null;
      state.loading = "fulfilled";
    },
  },
});

export const { reset, setUsername, setPage, setLimit } = searchSlice.actions;
export default searchSlice.reducer;
