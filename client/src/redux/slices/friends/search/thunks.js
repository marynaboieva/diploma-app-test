import { createAsyncThunk } from "@reduxjs/toolkit";
// Services
import BaseAPIService from "../../../../services/BaseAPIService";

export const searchFriends = createAsyncThunk(
  "friends.search/searchFriends",
  async ({ username = "", limit, page }, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: "users/me/friends/search",
        method: "get",
        params: { limit, page, username },
      });
      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);
