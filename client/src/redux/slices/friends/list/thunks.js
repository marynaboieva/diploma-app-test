import { createAsyncThunk } from "@reduxjs/toolkit";
// Services
import BaseAPIService from "../../../../services/BaseAPIService";

export const getFriends = createAsyncThunk(
  "friends.list/getFriends",
  async (_, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: "users/me/friends/",
        method: "get",
      });
      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const deleteFriend = createAsyncThunk(
  "friends.list/deleteFriend",
  async (friend_id, { rejectWithValue }) => {
    try {
      await BaseAPIService.request({
        url: `users/me/friends/${friend_id}`,
        method: "delete",
      });
      return friend_id;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);
