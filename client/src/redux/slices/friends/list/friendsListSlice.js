import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
// Actions
import { getFriends, deleteFriend } from "./thunks";
import { addOnlineUser, removeOnlineUser } from "../../../actions";

export const friendsAdapter = createEntityAdapter({
  selectId: (room) => room._id,
});

const initialState = { error: null, loading: "idle" };

const friendsListSlice = createSlice({
  name: "friends.list",
  initialState: friendsAdapter.getInitialState(initialState),
  reducers: {
    reset: () => friendsAdapter.getInitialState(initialState),
    addFriend: (state, action) =>
      friendsAdapter.upsertOne(state, action.payload),
    removeFriend: (state, action) =>
      friendsAdapter.removeOne(state, action.payload),
    setUsersInfo: (state, action) => {
      friendsAdapter.upsertMany(state, action.payload);
    },
  },
  extraReducers: {
    [String(getFriends.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      loading: "rejected",
    }),
    [String(getFriends.pending)]: (state) => ({ ...state, loading: "pending" }),
    [String(getFriends.fulfilled)]: (state, action) => {
      friendsAdapter.setAll(state, action.payload);
      state.error = null;
      state.loading = "fulfilled";
    },

    [String(deleteFriend.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
    }),
    [String(deleteFriend.fulfilled)]: (state, action) => {
      friendsAdapter.removeOne(state, action.payload);
      state.error = null;
    },

    [String(addOnlineUser)]: (state, action) => {
      friendsAdapter.upsertOne(state, {
        ...action.payload.user,
        online: true,
      });
    },
    [String(removeOnlineUser)]: (state, action) => {
      friendsAdapter.updateOne(state, {
        id: action.payload.user_id,
        changes: { online: false },
      });
    },
  },
});

export const { reset, setUsersInfo, addFriend, removeFriend } =
  friendsListSlice.actions;
export default friendsListSlice.reducer;
