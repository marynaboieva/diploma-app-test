import { friendsAdapter } from "./friendsListSlice";

export const { selectById, selectAll } = friendsAdapter.getSelectors(
  (state) => state.friends.list
);

export const selectFriendsListState = (state) => state.friends.list;
