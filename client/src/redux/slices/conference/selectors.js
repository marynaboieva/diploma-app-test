import { participantsAdapter } from "./conferenceSlice";

export const { selectById, selectAll } = participantsAdapter.getSelectors(
  (state) => state.conference
);

export const selectConferenceState = (state) => state.conference;
