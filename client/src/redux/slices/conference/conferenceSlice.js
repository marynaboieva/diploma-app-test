import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
// Actions
import {
  addPeer,
  removePeer,
  removeRoom,
  setActiveSpeaker,
  addSource,
  removeSource,
} from "../rooms/roomsActions";

export const participantsAdapter = createEntityAdapter({
  selectId: (user) => user._id,
});

const initialState = {
  id: "",
  activeSpeaker: null,
};

const conferenceSlice = createSlice({
  name: "conference",
  initialState: participantsAdapter.getInitialState(initialState),
  reducers: {
    setPeers: (state, action) => {
      participantsAdapter.setAll(state, action.payload);
    },
    setStatus: (state, action) => {
      return {
        ...state,
        id: action.payload.room_id,
        status: action.payload.status, // pending, idle, rejected, fulfilled
      };
    },
    reset: () => participantsAdapter.getInitialState(initialState),
  },
  extraReducers: {
    [String(addPeer)]: (state, action) => {
      if (state.id === action.payload.room_id && state.status === "fulfilled") {
        participantsAdapter.upsertOne(state, action.payload.user);
      }
    },
    [String(removePeer)]: (state, action) => {
      if (state.id === action.payload.room_id) {
        participantsAdapter.removeOne(state, action.payload.user_id);
      }
    },
    [String(setActiveSpeaker)]: (state, action) => {
      if (state.id === action.payload.room_id) {
        state.activeSpeaker = action.payload.user_id;
      }
    },

    [String(removeRoom)]: (state, action) => {
      if (state.id === action.payload.room_id) {
        state = initialState;
      }
    },

    [String(addSource)]: (state, action) => {
      participantsAdapter.updateOne(state, {
        id: action.payload.user_id,
        changes: { [action.payload.source]: true },
      });
    },

    [String(removeSource)]: (state, action) => {
      participantsAdapter.updateOne(state, {
        id: action.payload.user_id,
        changes: { [action.payload.source]: false },
      });
    },
  },
});

export const { reset, setStatus, setPeers } = conferenceSlice.actions;
export default conferenceSlice.reducer;
