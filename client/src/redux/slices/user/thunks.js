import { createAsyncThunk } from "@reduxjs/toolkit";
// Services
import BaseAPIService from "../../../services/BaseAPIService";
import { setToken } from "../../../services/TokenService";

export const login = createAsyncThunk(
  "user/login",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: "/login",
        method: "post",
        data: { email, password },
      });
      setToken(data.token);
      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const signup = createAsyncThunk(
  "user/signup",
  async ({ email, password, username }, { rejectWithValue, dispatch }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: "/signup",
        method: "post",
        data: { email, password, username },
      });
      dispatch(login({ email, password }));
      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const logout = createAsyncThunk("user/logout", async () => {
  setToken("");
  return;
});

export const getMe = createAsyncThunk(
  "user/getMe",
  async (_, { rejectWithValue, dispatch }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: "/users/me",
        method: "get",
      });
      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);
