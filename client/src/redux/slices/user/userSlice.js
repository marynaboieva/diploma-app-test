import { createSlice } from "@reduxjs/toolkit";
// Actions
import { login, logout, signup, getMe } from "./thunks";
// Services
import { getToken } from "../../../services/TokenService";

const initialState = {
  name: "",
  email: "",
  id: "",
  submit: "idle",
  token: getToken(),
};

const userSlice = createSlice({
  name: "user",
  initialState,
  extraReducers: {
    [String(signup.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      submit: "rejected",
    }),
    [String(signup.pending)]: (state) => ({ ...state, submit: "pending" }),

    [String(login.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      submit: "rejected",
    }),
    [String(login.pending)]: (state) => ({ ...state, submit: "pending" }),
    [String(login.fulfilled)]: (state, action) => {
      const { user_id, email, username } = action.payload.user;
      return {
        ...state,
        id: user_id,
        email,
        name: username,
        token: action.payload.token,
        error: null,
        submit: "fulfilled",
      };
    },

    [String(getMe.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      loading: "rejected",
    }),
    [String(getMe.pending)]: (state) => ({ ...state, loading: "pending" }),
    [String(getMe.fulfilled)]: (state, action) => {
      const { _id, email, username } = action.payload;
      return {
        ...state,
        id: _id,
        email,
        name: username,
        error: null,
        loading: "fulfilled",
      };
    },
    [String(logout.fulfilled)]: (state, action) => ({
      ...initialState,
      token: "",
    }),
  },
});

export default userSlice.reducer;
