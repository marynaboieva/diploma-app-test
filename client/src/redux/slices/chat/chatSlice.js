import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
// Actions
import {
  getMessages,
  deleteMessage,
  editMessage,
  sendMessage,
  markAsRead,
} from "./thunks";
import {
  editedMessage,
  deletedMessage,
  newMessage,
} from "../rooms/roomsActions";

export const messagessAdapter = createEntityAdapter({
  selectId: (user) => user._id,
});

const initialState = {
  error: null,
  loading: "idle",
  id: "",
  page: 0,
  limit: 50,
  total: 0,
};

const chatSlice = createSlice({
  name: "chat",
  initialState: messagessAdapter.getInitialState(initialState),
  reducers: {
    setPage: (state, action) => ({ ...state, page: action.payload }),
    reset: () => messagessAdapter.getInitialState(initialState),
  },
  extraReducers: {
    [String(getMessages.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      loading: "rejected",
    }),

    [String(getMessages.pending)]: (state) => ({
      ...state,
      loading: "pending",
    }),

    [String(getMessages.fulfilled)]: (state, action) => {
      messagessAdapter.upsertMany(state, action.payload.messages);
      state.id = action.payload.room_id;
      state.total = action.payload.total;
      state.error = null;
      state.loading = "fulfilled";
    },

    [String(deleteMessage.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
    }),

    [String(deleteMessage.fulfilled)]: (state, action) => {
      if (state.id === action.payload.room_id) {
        messagessAdapter.removeOne(state, action.payload.message_id);
        state.error = null;
      }
    },

    [String(sendMessage.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
    }),

    [String(sendMessage.fulfilled)]: (state, action) => {
      if (state.id === action.payload.room_id) {
        messagessAdapter.addOne(state, action.payload.message);
        state.error = null;
      }
    },

    [String(editMessage.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
    }),

    [String(editMessage.fulfilled)]: (state, action) => {
      if (state.id === action.payload.room_id) {
        messagessAdapter.updateOne(state, {
          id: action.payload.message._id,
          changes: { body: action.payload.message.body, edited: true },
        });
        state.error = null;
      }
    },

    [String(markAsRead.fulfilled)]: (state, action) => {
      if (state.id === action.payload.room_id) {
        messagessAdapter.updateOne(state, {
          id: action.payload.message._id,
          changes: { read: true },
        });
        state.error = null;
      }
    },

    [String(newMessage)]: (state, action) => {
      if (state.id === action.payload.room_id) {
        messagessAdapter.addOne(state, action.payload.message);
      }
    },

    [String(deletedMessage)]: (state, action) => {
      if (state.id === action.payload.room_id) {
        messagessAdapter.removeOne(state, action.payload.messageId);
      }
    },

    [String(editedMessage)]: (state, action) => {
      if (state.id === action.payload.room_id) {
        messagessAdapter.updateOne(state, {
          id: action.payload.message._id,
          changes: { body: action.payload.message.body, edited: true },
        });
      }
    },
  },
});

export const { reset, setPage } = chatSlice.actions;
export default chatSlice.reducer;
