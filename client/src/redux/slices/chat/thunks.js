import { createAsyncThunk } from "@reduxjs/toolkit";
// Services
import BaseAPIService from "../../../services/BaseAPIService";

export const getMessages = createAsyncThunk(
  "chat/getMessages",
  async ({ room_id, page, limit }, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: `/rooms/${room_id}/chat`,
        method: "get",
        params: { page, limit },
      });
      return { room_id, messages: data.messages, total: data.total };
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const sendMessage = createAsyncThunk(
  "chat/sendMessage",
  async ({ room_id, body }, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: `/rooms/${room_id}/chat/`,
        method: "post",
        data: { body },
      });
      return { message: data, room_id };
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const deleteMessage = createAsyncThunk(
  "chat/deleteMessage",
  async ({ room_id, message_id }, { rejectWithValue }) => {
    try {
      await BaseAPIService.request({
        url: `/rooms/${room_id}/chat/${message_id}`,
        method: "delete",
      });
      return { room_id, message_id };
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const editMessage = createAsyncThunk(
  "chat/editMessage",
  async ({ room_id, message_id, body }, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: `/rooms/${room_id}/chat/${message_id}`,
        method: "put",
        data: { body },
      });
      return { room_id, message: data };
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const markAsRead = createAsyncThunk(
  "chat/markAsRead",
  async ({ room_id, message_id }, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: `/rooms/${room_id}/chat/${message_id}/read`,
        method: "patch",
      });
      return { room_id, message: data };
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);
