import { messagessAdapter } from "./chatSlice";

export const { selectById, selectAll } = messagessAdapter.getSelectors(
  (state) => state.chat
);

export const selectChatState = (state) => state.chat;
