import { createAction } from "@reduxjs/toolkit";
// Steaming
export const addPeer = createAction("room/addPeer");
export const removePeer = createAction("room/removePeer");
// Consumer events
export const addSource = createAction("room/addSource");
export const removeSource = createAction("room/removeSource");

// Participant
export const addParticipant = createAction("room/addParticipant");
export const removeParticipant = createAction("room/removeParticipant");
// Delete room
export const removeRoom = createAction("room/deleteRoom");
// Active speacker
export const setActiveSpeaker = createAction("room/setActiveSpeaker");

// Chat
export const newMessage = createAction("chat/newMessage");
export const deletedMessage = createAction("chat/deletedMessage");
export const editedMessage = createAction("chat/editedMessage");
