import { combineReducers } from "@reduxjs/toolkit";
// Slices
import roomsSlice from "./list/roomsSlice";
import createRoomSlice from "./create/createRoomSlice";
import selectedRoomSlice from "./selected/selectedRoomSlice";
import invitesRoomSlice from "./invites/invitesSlice";
import searchSlice from "./search/searchSlice";

const roomsReducer = combineReducers({
  list: roomsSlice,
  create: createRoomSlice,
  selected: selectedRoomSlice,
  invites: invitesRoomSlice,
  search: searchSlice,
});

export default roomsReducer;
