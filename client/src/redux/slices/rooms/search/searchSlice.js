import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
// Actions
import { searchParticipants } from "./thunks";

export const usersAdapter = createEntityAdapter({
  selectId: (room) => room._id,
});

const initialState = {
  username: "",
  page: 0,
  limit: 10,
  total: 0,
  error: null,
  loading: "idle",
};

const searchSlice = createSlice({
  name: "rooms.search",
  initialState: usersAdapter.getInitialState(initialState),
  reducers: {
    setUsername: (state, action) => ({
      ...state,
      page: 0,
      username: action.payload,
    }),
    setLimit: (state, action) => ({ ...state, limit: action.payload }),
    setPage: (state, action) => ({ ...state, page: action.payload }),
    reset: () => usersAdapter.getInitialState(initialState),
  },
  extraReducers: {
    [String(searchParticipants.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      loading: "rejected",
    }),
    [String(searchParticipants.pending)]: (state) => ({
      ...state,
      loading: "pending",
    }),
    [String(searchParticipants.fulfilled)]: (state, action) => {
      const { count, users } = action.payload;
      usersAdapter.setAll(state, users);
      state.total = count;
      state.error = null;
      state.loading = "fulfilled";
    },
  },
});

export const { reset, setUsername, setPage, setLimit } = searchSlice.actions;
export default searchSlice.reducer;
