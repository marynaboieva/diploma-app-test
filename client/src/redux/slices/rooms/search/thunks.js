import { createAsyncThunk } from "@reduxjs/toolkit";
// Services
import BaseAPIService from "../../../../services/BaseAPIService";

export const searchParticipants = createAsyncThunk(
  "friends.search/searchParticipants",
  async ({ username = "", room_id, limit, page }, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: `rooms/${room_id}/participants/search`,
        method: "get",
        params: { limit, page, username },
      });
      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);
