import { usersAdapter } from "./searchSlice";

export const { selectById, selectAll } = usersAdapter.getSelectors(
  (state) => state.rooms.search
);

export const selectSearchState = (state) => state.rooms.search;
