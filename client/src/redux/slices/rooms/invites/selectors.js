import { invitesAdapter } from "./invitesSlice";

export const { selectById, selectAll } = invitesAdapter.getSelectors(
  (state) => state.rooms.invites
);

export const selectInvitesState = (state) => state.rooms.invites;

export const selectReceivedInvites = (state) => {
  const invites = selectAll(state);
  return invites.filter(({ receiver }) => receiver._id === state.user.id);
};

export const selectInvitesByRoomId =(id)=> (state) => {
  const invites = selectAll(state);
  return invites.filter(({ room, sender }) => room._id === id&&sender._id===state.user.id);
};