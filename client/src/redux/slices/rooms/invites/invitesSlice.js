import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
// Actions
import {
  getInvites,
  createInvite,
  cancelInvite,
  acceptInvite,
  declineInvite,
} from "./thunks";

export const invitesAdapter = createEntityAdapter({
  selectId: (Invites) => Invites._id,
});

const initialState = { error: null, loading: "idle" };

const invitesSlice = createSlice({
  name: "rooms.invites",
  initialState: invitesAdapter.getInitialState(initialState),
  reducers: {
    addInvite: (state, action) =>
      invitesAdapter.upsertOne(state, action.payload),
    removeInvite: (state, action) =>
      invitesAdapter.removeOne(state, action.payload),

    reset: () => invitesAdapter.getInitialState(initialState),
  },
  extraReducers: {
    [String(getInvites.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      loading: "rejected",
    }),
    [String(getInvites.pending)]: (state) => ({
      ...state,
      loading: "pending",
    }),
    [String(getInvites.fulfilled)]: (state, action) => {
      invitesAdapter.setAll(state, action.payload);
      state.error = null;
      state.loading = "fulfilled";
    },

    [String(createInvite.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      submit: "rejected",
    }),

    [String(createInvite.fulfilled)]: (state, action) => {
      state.error = null;
      invitesAdapter.upsertOne(state, action.payload);
    },

    [String(cancelInvite.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
    }),
    [String(cancelInvite.fulfilled)]: (state, action) => {
      state.error = null;
      invitesAdapter.removeOne(state, action.payload);
    },

    [String(acceptInvite.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
    }),
    [String(acceptInvite.fulfilled)]: (state, action) => {
      state.error = null;
      invitesAdapter.removeOne(state, action.payload._id);
    },

    [String(declineInvite.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
    }),
    [String(declineInvite.fulfilled)]: (state, action) => {
      state.error = null;
      invitesAdapter.removeOne(state, action.payload);
    },
  },
});

export const { reset, addInvite, removeInvite } = invitesSlice.actions;
export default invitesSlice.reducer;
