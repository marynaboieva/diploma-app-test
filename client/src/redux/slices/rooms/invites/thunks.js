import { createAsyncThunk } from "@reduxjs/toolkit";
// Services
import BaseAPIService from "../../../../services/BaseAPIService";

export const getInvites = createAsyncThunk(
  "rooms.invites/getInvites",
  async (_, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: "invites/",
        method: "get",
      });
      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const createInvite = createAsyncThunk(
  "rooms.invites/createInvite",
  async ({ receiver_id, room_id }, { rejectWithValue, dispatch, getState }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: `invites/`,
        method: "post",
        data: { receiver_id, room_id },
      });

      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const cancelInvite = createAsyncThunk(
  "rooms.invites/cancelInvite",
  async (_id, { rejectWithValue, dispatch, getState }) => {
    try {
      await BaseAPIService.request({
        url: `/invites/${_id}`,
        method: "delete",
      });

      return _id;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const acceptInvite = createAsyncThunk(
  "rooms.invites/acceptInvite",
  async (_id, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: `invites/${_id}/accept`,
        method: "patch",
      });
      return { _id, room: data };
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const declineInvite = createAsyncThunk(
  "rooms.invites/declineInvite",
  async (_id, { rejectWithValue }) => {
    try {
      await BaseAPIService.request({
        url: `invites/${_id}/decline`,
        method: "patch",
      });
      return _id;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);
