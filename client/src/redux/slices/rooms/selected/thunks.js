import { createAsyncThunk } from "@reduxjs/toolkit";
// Services
import BaseAPIService from "../../../../services/BaseAPIService";

export const getRoomData = createAsyncThunk(
  "rooms.selected/getRoomData",
  async ({ id }, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: `/rooms/${id}/`,
        method: "get",
      });
      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const kickOutUser = createAsyncThunk(
  "rooms.selected/kickOutUser",
  async ({ room_id, user_id }, { rejectWithValue }) => {
    try {
      await BaseAPIService.request({
        url: `/rooms/${room_id}/participants/${user_id}`,
        method: "delete",
      });
      return { room_id, user_id };
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const deleteRoom = createAsyncThunk(
  "rooms.selected/deleteRoom",
  async (id, { rejectWithValue }) => {
    try {
      await BaseAPIService.request({
        url: `/rooms/${id}`,
        method: "delete",
      });
      return id;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);

export const leaveRoom = createAsyncThunk(
  "rooms.selected/leaveRoom",
  async (id, { rejectWithValue }) => {
    try {
      await BaseAPIService.request({
        url: `/rooms/${id}/leave`,
        method: "put",
      });
      return id;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);
