import { participantsAdapter } from "./selectedRoomSlice";

export const { selectById, selectAll } = participantsAdapter.getSelectors(
  (state) => state.rooms.selected
);

export const selectSelectedRoomState = (state) => state.rooms.selected;

export const selectStreamingUsers = (state) =>
  selectAll(state).filter(({ streaming }) => streaming);
