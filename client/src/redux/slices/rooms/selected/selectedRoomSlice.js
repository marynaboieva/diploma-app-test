import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
// Actions
import { getRoomData, kickOutUser, deleteRoom, leaveRoom } from "./thunks";
import {
  addPeer,
  removePeer,
  addParticipant,
  removeParticipant,
  removeRoom,
  setActiveSpeaker,
} from "../roomsActions";

import { addOnlineUser, removeOnlineUser } from "../../../actions";

export const participantsAdapter = createEntityAdapter({
  selectId: (user) => user._id,
  sortComparer: (a, b) => (a.online === b.online ? 0 : a.online ? -1 : 1),
});

const initialState = {
  error: null,
  loading: "idle",
  data: null,
};

const selectedRoomSlice = createSlice({
  name: "rooms.selected",
  initialState: participantsAdapter.getInitialState(initialState),
  reducers: {
    reset: () => participantsAdapter.getInitialState(initialState),
    setUsersStatus: (state, action) => {
      const changes = action.payload.map(({ _id, ...rest }) => ({
        id: _id,
        changes: rest,
      }));
      participantsAdapter.updateMany(state, changes);
    },
  },
  extraReducers: {
    [String(getRoomData.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      loading: "rejected",
    }),
    [String(getRoomData.pending)]: (state) => ({
      ...state,
      loading: "pending",
    }),
    [String(getRoomData.fulfilled)]: (state, action) => {
      const { participants, ...data } = action.payload;
      state.error = null;
      state.loading = "fulfilled";
      state.data = data;
      participantsAdapter.upsertMany(state, participants);
    },

    [String(kickOutUser.fulfilled)]: (state, action) => {
      if (state.data?._id === action.payload.room_id) {
        participantsAdapter.removeOne(state, action.payload.user_id);
      }
    },

    [String(leaveRoom.fulfilled)]: (state, action) => {
      if (state.data?._id === action.payload) {
        return participantsAdapter.getInitialState(initialState);
      }
    },

    [String(deleteRoom.fulfilled)]: (state, action) => {
      if (state.data?._id === action.payload) {
        return participantsAdapter.getInitialState(initialState);
      }
    },

    [String(addPeer)]: (state, action) => {
      if (state?.data?._id === action.payload.room_id) {
        participantsAdapter.upsertOne(state, {
          ...action.payload.user,
          online: true,
          streaming: true,
        });
      }
    },
    [String(removePeer)]: (state, action) => {
      if (state?.data?._id === action.payload.room_id) {
        participantsAdapter.updateOne(state, {
          id: action.payload.user_id,
          changes: { streaming: false },
        });
      }
    },
    [String(setActiveSpeaker)]: (state, action) => {
      if (state?.data?._id === action.payload.room_id) {
        state.activeSpeaker = action.payload.user_id;
      }
    },

    [String(removeRoom)]: (state, action) => {
      if (state.data?._id === action.payload.room_id) {
        state = initialState;
      }
    },
    [String(addParticipant)]: (state, action) => {
      if (state.data?._id === action.payload.room_id) {
        participantsAdapter.upsertOne(state, action.payload.user);
      }
    },
    [String(removeParticipant)]: (state, action) => {
      if (state.data?._id === action.payload.room_id) {
        participantsAdapter.removeOne(state, action.payload.user_id);
      }
    },
    [String(addOnlineUser)]: (state, action) => {
      if (state.data?._id === action.payload.room_id) {
        participantsAdapter.upsertOne(state, {
          ...action.payload.user,
          online: true,
        });
      }
    },
    [String(removeOnlineUser)]: (state, action) => {
      participantsAdapter.updateOne(state, {
        id: action.payload.user_id,
        changes: { online: false },
      });
    },
  },
});

export const { reset, setUsersStatus } = selectedRoomSlice.actions;
export default selectedRoomSlice.reducer;
