import { createAsyncThunk } from "@reduxjs/toolkit";
// Services
import BaseAPIService from "../../../../services/BaseAPIService";

export const createRoom = createAsyncThunk(
  "room/createRoom",
  async ({ title }, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: "/rooms",
        method: "post",
        data: { title },
      });

      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);
