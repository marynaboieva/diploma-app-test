import { createSlice } from "@reduxjs/toolkit";
// Actions
import { createRoom } from "./thunks";

const initialState = { error: null, submit: "idle" };

const createRoomSlice = createSlice({
  name: "rooms.create",
  initialState,
  reducers: {
    reset: () => initialState,
  },
  extraReducers: {
    [String(createRoom.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      submit: "rejected",
    }),
    [String(createRoom.pending)]: (state) => ({ ...state, submit: "pending" }),
    [String(createRoom.fulfilled)]: (state, action) => {
      state.error = null;
      state.submit = "fulfilled";
    },
  },
});

export const { reset } = createRoomSlice.actions;
export default createRoomSlice.reducer;
