import { createAsyncThunk } from "@reduxjs/toolkit";
// Services
import BaseAPIService from "../../../../services/BaseAPIService";

export const getRooms = createAsyncThunk(
  "rooms.list/getRooms",
  async (_, { rejectWithValue }) => {
    try {
      const { data } = await BaseAPIService.request({
        url: "/rooms/",
        method: "get",
      });
      return data;
    } catch (err) {
      return rejectWithValue(err?.response?.data || err?.message);
    }
  }
);
