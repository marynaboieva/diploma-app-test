import { roomsAdapter } from "./roomsSlice";

export const { selectById, selectAll } = roomsAdapter.getSelectors(
  (state) => state.rooms.list
);

export const selectRoomsListState = (state) => state.rooms.list;
