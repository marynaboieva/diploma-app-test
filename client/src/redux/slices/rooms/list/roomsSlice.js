import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
// Actions
import { getRooms } from "./thunks";
import { createRoom } from "../create/thunks";
import { acceptInvite } from "../invites/thunks";
import { deleteRoom, leaveRoom } from "../selected/thunks";
import {
  addParticipant,
  removeParticipant,
  addPeer,
  removePeer,
  removeRoom,
  setActiveSpeaker,
  newMessage,
  deletedMessage,
} from "../roomsActions";
import { addOnlineUser, removeOnlineUser } from "../../../actions";
import { markAsRead } from "../../chat/thunks";

export const roomsAdapter = createEntityAdapter({
  selectId: (room) => room._id,
});

const initialState = { error: null, loading: "idle" };

const roomsSlice = createSlice({
  name: "rooms.list",
  initialState: roomsAdapter.getInitialState(initialState),
  reducers: {
    reset: () => roomsAdapter.getInitialState(initialState),

    setRoomsInfo: (state, action) => {
      roomsAdapter.upsertMany(state, action.payload);
    },
    addRoomInfo: (state, action) => {
      roomsAdapter.upsertOne(state, action.payload);
    },
  },
  extraReducers: {
    [String(getRooms.rejected)]: (state, action) => ({
      ...state,
      error: action.payload,
      loading: "rejected",
    }),

    [String(getRooms.pending)]: (state) => ({ ...state, loading: "pending" }),

    [String(getRooms.fulfilled)]: (state, action) => {
      roomsAdapter.upsertMany(state, action.payload);
      state.error = null;
      state.loading = "fulfilled";
    },

    [String(newMessage)]: (state, action) => {
      roomsAdapter.updateOne(state, {
        id: action.payload.room_id,
        changes: {
          messages: (state.entities[action.payload.room_id]?.messages ?? 0) + 1,
        },
      });
    },

    [String(deletedMessage)]: (state, action) => {
      roomsAdapter.updateOne(state, {
        id: action.payload.room_id,
        changes: {
          messages: (state.entities[action.payload.room_id]?.messages ?? 0) - 1,
        },
      });
    },

    [String(markAsRead.fulfilled)]: (state, action) => {
      roomsAdapter.updateOne(state, {
        id: action.payload.room_id,
        changes: {
          messages: (state.entities[action.payload.room_id]?.messages ?? 0) - 1,
        },
      });
    },

    [String(acceptInvite.fulfilled)]: (state, action) => {
      roomsAdapter.addOne(state, action.payload.room);
    },

    [String(createRoom.fulfilled)]: (state, action) => {
      roomsAdapter.addOne(state, action.payload);
    },

    [String(leaveRoom.fulfilled)]: (state, action) => {
      roomsAdapter.removeOne(state, action.payload);
    },

    [String(deleteRoom.fulfilled)]: (state, action) => {
      roomsAdapter.removeOne(state, action.payload);
    },

    [String(addPeer)]: (state, action) => {
      roomsAdapter.updateOne(state, {
        id: action.payload.room_id,
        changes: {
          streaming:
            (state.entities[action.payload.room_id]?.streaming ?? 0) + 1,
        },
      });
    },
    [String(removePeer)]: (state, action) => {
      roomsAdapter.updateOne(state, {
        id: action.payload.room_id,
        changes: {
          streaming: state.entities[action.payload.room_id]?.streaming - 1,
        },
      });
    },
    [String(setActiveSpeaker)]: (state, action) => {
      roomsAdapter.updateOne(state, {
        id: action.payload.room_id,
        changes: { activeSpeaker: action.payload.user_id },
      });
    },

    [String(removeRoom)]: (state, action) => {
      roomsAdapter.removeOne(state, action.payload);
    },
    [String(addParticipant)]: (state, action) => {
      roomsAdapter.updateOne(state, {
        id: action.payload.room_id,
        changes: {
          participants: state.entities[action.payload.room_id].participants++,
        },
      });
    },
    [String(removeParticipant)]: (state, action) => {
      roomsAdapter.updateOne(state, {
        id: action.payload.room_id,
        changes: {
          participants: state.entities[action.payload.room_id].participants - 1,
        },
      });
    },
    [String(addOnlineUser)]: (state, action) => {
      if (action.payload?.room_id) {
        roomsAdapter.upsertOne(state, {
          id: action.payload?.room_id,
          changes: {
            online: (state.entities[action.payload.room_id]?.online ?? 0) + 1,
          },
        });
      }
    },
    [String(removeOnlineUser)]: (state, action) => {
      roomsAdapter.updateOne(state, {
        id: action.payload.room_id,
        changes: {
          online: state.entities[action.payload.room_id]?.online - 1,
        },
      });
    },
  },
});

export const { reset, setRoomsInfo, addRoomInfo } = roomsSlice.actions;
export default roomsSlice.reducer;
