import { combineReducers } from "@reduxjs/toolkit";
// Slices
import roomsReducer from "./slices/rooms";
import userReducer from "./slices/user/userSlice";
import friendsReducer from "./slices/friends";
import conferenceSlice from "./slices/conference/conferenceSlice";
import chatSlice from "./slices/chat/chatSlice";

const rootReducer = combineReducers({
  rooms: roomsReducer,
  user: userReducer,
  friends: friendsReducer,
  conference: conferenceSlice,
  chat: chatSlice,
});

export default rootReducer;
