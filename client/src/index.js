import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
// Redux
import { Provider } from "react-redux";
import store from "./redux/store";
// Webrtc
import WebrtcContextProvider from "./webrtc/WebrtcContext";
import SocketContextProvider from "./socket/SocketContext";
import MediaContextProvider from "./webrtc/MediaProvider";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <SocketContextProvider>
        <WebrtcContextProvider>
          <MediaContextProvider>
            <App />
          </MediaContextProvider>
        </WebrtcContextProvider>
      </SocketContextProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

serviceWorker.unregister();
