export const randomArray = (length) =>
  [...new Array(length)].map(() => Math.random().toString(36).substring(2, 15));
