import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => {
  return (
    <>
      <h5>There is no such page</h5>
      <p>
        What are you doing here?) <Link to="/friends">Go home!</Link>
      </p>
    </>
  );
};

export default NotFound;
