import { EuiButtonEmpty, EuiButton } from "@elastic/eui";
import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
// Components
import UserItem from "../../components/users-list/UserItem";
// Actions
import {
  cancelRequest,
  acceptRequest,
  declineRequest,
} from "../../redux/slices/friends/requests/thunks";
// Selects
import { selectUser } from "../../redux/slices/user/selectors";

const RequestItem = ({ request }) => {
  const user = useSelector(selectUser);
  const { sender, receiver, _id } = request;
  const isIncoming = receiver._id === user.id;

  const dispatch = useDispatch();

  const onCancel = useCallback(() => {
    dispatch(cancelRequest(_id ));
  }, [dispatch, _id]);

  const onDecline = useCallback(() => {
    dispatch(declineRequest(_id));
  }, [dispatch, _id]);

  const onAccept = useCallback(() => {
    dispatch(acceptRequest(_id));
  }, [dispatch, _id]);

  return (
    <UserItem
      user={isIncoming ? sender : receiver}
      extraAction={
        isIncoming ? (
          <>
            <EuiButton color="secondary" onClick={onAccept}>
              Accept
            </EuiButton>
            <EuiButtonEmpty color="danger" onClick={onDecline}>
              Decline
            </EuiButtonEmpty>
          </>
        ) : (
          <EuiButtonEmpty color="danger" onClick={onCancel}>
            Cancel
          </EuiButtonEmpty>
        )
      }
    />
  );
};

export default RequestItem;
