import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { toast } from "react-toastify";
// Actions
import { searchFriends } from "../../redux/slices/friends/search/thunks";
import {
  setUsername,
  setPage,
  reset,
} from "../../redux/slices/friends/search/searchSlice";
// Components
import UserSearch from "../../components/users-search/UserSearch";
// Selects
import {
  selectAll,
  selectSearchState,
} from "../../redux/slices/friends/search/selectors";

const FriendsSearch = () => {
  const { loading, username, page, limit, total, error } =
    useSelector(selectSearchState);
  const users = useSelector(selectAll);

  const dispatch = useDispatch();

  const handleSearch = useCallback(
    (newUsername) => {
      dispatch(setUsername(newUsername));
    },
    [dispatch]
  );

  const onPageClick = useCallback(
    (newPage) => {
      dispatch(setPage(newPage));
    },
    [dispatch]
  );

  useEffect(() => {
    dispatch(searchFriends({ username, page, limit }));
  }, [username, page, limit, dispatch]);

  useEffect(
    () => () => {
      dispatch(reset());
    },
    [dispatch]
  );
  useEffect(() => {
    if (error) {
      toast.error(error);
    }
  }, [error]);
  return (
    <UserSearch
      users={users}
      username={username}
      loading={loading}
      limit={limit}
      page={page}
      total={total}
      onPageClick={onPageClick}
      handleSearch={handleSearch}
    />
  );
};

export default FriendsSearch;
