import React from "react";
import { useSelector } from "react-redux";
import { EuiHeaderSection, EuiBadge, EuiIcon } from "@elastic/eui";
import NavButton from "../../components/nav-button/NavButton";
import friendsIcon from "../../icons/friends.svg";
// Selects
import { selectAll } from "../../redux/slices/friends/requests/selectors";

const HomeHeader = () => {
  const requests = useSelector(selectAll);

  return (
    <EuiHeaderSection side="right" className="home-page-navigation">
      <EuiIcon size="l" type={friendsIcon} style={{ marginRight: "10px" }} />
      <NavButton to="/friends">My</NavButton>
      <NavButton to="/requests">
        Requested
        {!!requests.length && (
          <EuiBadge className="small-badge" color="danger">
            {requests.length}
          </EuiBadge>
        )}
      </NavButton>
      <NavButton to="/search">Find</NavButton>
    </EuiHeaderSection>
  );
};

export default HomeHeader;
