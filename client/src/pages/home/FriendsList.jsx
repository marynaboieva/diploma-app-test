import React, { useCallback } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
// Components
import UsersList from "../../components/users-list/UsersList";
import { EuiEmptyPrompt, EuiButton } from "@elastic/eui";

// Selects
import {
  selectAll,
  selectFriendsListState,
} from "../../redux/slices/friends/list/selectors";

const FriendsList = () => {
  const { loading } = useSelector(selectFriendsListState);
  const friends = useSelector(selectAll);

  const history = useHistory();

  const onClick = useCallback(() => {
    history.push("/search");
  }, [history]);

  return (
    <UsersList
      users={friends}
      loading={loading}
      emptyPrompt={
        <EuiEmptyPrompt
          iconType="users"
          title={<h2>You do not have any friends yet</h2>}
          body={<p>You can search for users by their usernames</p>}
          actions={<EuiButton onClick={onClick}>Find friends!</EuiButton>}
        />
      }
    />
  );
};

export default FriendsList;
