import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
// Components
import UsersList from "../../components/users-list/UsersList";
import { EuiEmptyPrompt } from "@elastic/eui";
import AddFriendIcon from "../../icons/add-contact.svg";
import RequestItem from "./RequestItem";
// Selects
import {
  selectAll,
  selectRequestsState,
} from "../../redux/slices/friends/requests/selectors";

const renderItem = (request) => (
  <RequestItem key={request._id} request={request} />
);

const RequestsList = () => {
  const { loading, error } = useSelector(selectRequestsState);
  const requests = useSelector(selectAll);

  useEffect(() => {
    if (error) {
      toast.error("Failed", error);
    }
  }, [error]);

  return (
    <UsersList
      users={requests}
      loading={loading}
      renderItem={renderItem}
      emptyPrompt={
        <EuiEmptyPrompt
          iconType={AddFriendIcon}
          title={<h2>You have no friendship requests</h2>}
        />
      }
    />
  );
};

export default RequestsList;
