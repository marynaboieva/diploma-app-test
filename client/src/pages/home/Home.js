import React from "react";
import { EuiPageContent } from "@elastic/eui";

const Home = ({ children }) => {
  return (
    <EuiPageContent verticalPosition="center" style={{ flexGrow: 1 }}>
      {children}
    </EuiPageContent>
  );
};

export default Home;
