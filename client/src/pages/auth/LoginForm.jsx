import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import {
  EuiForm,
  EuiFormRow,
  EuiFieldText,
  EuiFieldPassword,
  EuiButton,
} from "@elastic/eui";
// Actions
import { login } from "../../redux/slices/user/thunks";
// Selectors
import { selectUser } from "../../redux/slices/user/selectors";

const LoginForm = () => {
  const { submit } = useSelector(selectUser);
  const { register, handleSubmit } = useForm();
  const dispatch = useDispatch();

  const handleLoginSubmit = useCallback(
    (data) => {
      dispatch(login(data));
    },
    [dispatch]
  );

  return (
    <EuiForm component="form" onSubmit={handleSubmit(handleLoginSubmit)}>
      <EuiFormRow label="Email">
        <EuiFieldText
          inputRef={register({ required: true })}
          name="email"
          type="email"
          placeholder="name@example.com"
        />
      </EuiFormRow>

      <EuiFormRow label="Password">
        <EuiFieldPassword
          inputRef={register({ required: true })}
          name="password"
          type="password"
        />
      </EuiFormRow>
      <EuiFormRow>
        <EuiButton isLoading={submit === "pending"} type="submit">
          Log in
        </EuiButton>
      </EuiFormRow>
    </EuiForm>
  );
};

export default LoginForm;
