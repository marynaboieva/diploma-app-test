import React, { useCallback, useEffect, useMemo } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { EuiTabbedContent } from "@elastic/eui";
import LoginForm from "./LoginForm";
import SignupForm from "./SignupForm";
// Selectors
import { selectUser } from "../../redux/slices/user/selectors";

const tabs = [
  { id: "/login", name: "Log in", content: <LoginForm /> },
  { id: "/signup", name: "Sign up", content: <SignupForm /> },
];

const AuthPage = () => {
  const { error, token } = useSelector(selectUser);

  const history = useHistory();
  const location = useLocation();

  const selected = useMemo(
    () => tabs.find(({ id }) => id === location.pathname),
    [location.pathname]
  );

  const handleTabChange = useCallback(
    (tab) => {
      history.push(`${tab.id}`);
    },
    [history]
  );

  useEffect(() => {
    if (error) {
      toast.error(error);
    }
  }, [error]);

  useEffect(() => {
    if (token) {
      history.push("/");
    }
  }, [history, token]);

  return (
    <EuiTabbedContent
      selectedTab={selected}
      onTabClick={handleTabChange}
      tabs={tabs}
    />
  );
};

export default AuthPage;
