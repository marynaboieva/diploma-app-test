import React from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
  EuiHeaderSection,
  EuiHeaderSectionItem,
  EuiLoadingContent,
  EuiTitle,
} from "@elastic/eui";
import CallButton from "./components/CallButton";
import RoomMenu from "./sidebar/RoomMenu";
// Hooks
import useCall from "./useCall";
// Selectors
import { selectSelectedRoomState } from "../../redux/slices/rooms/selected/selectors";

const RoomHeader = () => {
  const { id } = useParams();
  const { loading, data } = useSelector(selectSelectedRoomState);

  const { toggleCall, loading: conferenceLoading, isActive } = useCall(id);

  return (
    <>
      <EuiHeaderSection className="room-title">
        {loading === "pending" ? (
          <EuiLoadingContent lines={1} />
        ) : (
          <EuiTitle size="s">
            <h1>{data?.title}</h1>
          </EuiTitle>
        )}
      </EuiHeaderSection>
      <EuiHeaderSection grow={false}>
        <EuiHeaderSectionItem>
          <CallButton
            disabled={loading === "pending"}
            isLoading={conferenceLoading === "pending"}
            onClick={toggleCall}
            active={isActive}
          />
        </EuiHeaderSectionItem>
        <EuiHeaderSectionItem>
          <RoomMenu />
        </EuiHeaderSectionItem>
      </EuiHeaderSection>
    </>
  );
};

export default RoomHeader;
