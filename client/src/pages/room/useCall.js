import { useCallback } from "react";
import { useSelector } from "react-redux";
// Hooks
import useWebrtc from "../../webrtc/hooks/useWebrtc";
// Selectors
import { selectConferenceState } from "../../redux/slices/conference/selectors";

const useCall = (id) => {
  const conferenceState = useSelector(selectConferenceState);
  const { joinRoom, leaveRoom } = useWebrtc();

  const toggleCall = useCallback(() => {
    if (conferenceState?.status === "fulfilled" && id === conferenceState.id) {
      leaveRoom(id);
    } else {
      joinRoom(id);
    }
  }, [id, joinRoom, leaveRoom, conferenceState]);

  const endCall = useCallback(() => {
    leaveRoom(id);
  }, [id, leaveRoom]);

  return {
    toggleCall,
    endCall,
    loading: conferenceState.status,
    isActive:
      conferenceState?.status === "fulfilled" && id === conferenceState.id,
  };
};

export default useCall;
