import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import { EuiPageContent } from "@elastic/eui";
import RoomChat from "./chat/RoomChat";
// Hooks
import useSocket from "../../socket/useSocket";
// Selectors
import {
  selectRoomsListState,
  selectById,
} from "../../redux/slices/rooms/list/selectors";
import { selectSelectedRoomState } from "../../redux/slices/rooms/selected/selectors";
// Actions
import {
  setUsersStatus,
  reset,
} from "../../redux/slices/rooms/selected/selectedRoomSlice";
import { getRoomData } from "../../redux/slices/rooms/selected/thunks";

const Room = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();

  const { loading: listLoading } = useSelector(selectRoomsListState);
  const roomData = useSelector((state) => selectById(state, id));
  const { loading } = useSelector(selectSelectedRoomState);
  const { socket, isSocketConnected } = useSocket();

  useEffect(() => {
    dispatch(getRoomData({ id }));
    return () => {
      dispatch(reset());
    };
  }, [dispatch, id]);

  useEffect(() => {
    if (isSocketConnected && loading === "fulfilled") {
      socket.current.emit("getRoomUsersStatus", id).then((roomUsersInfo) => {
        dispatch(setUsersStatus(roomUsersInfo));
      });
    }
  }, [dispatch, socket, isSocketConnected, loading, id]);

  useEffect(() => {
    if (listLoading === "fulfilled" && !roomData) {
      history.push("/friends");
    }
  }, [listLoading, history, roomData]);

  return (
    <>
      <EuiPageContent className="room-layout">
        <RoomChat room_id={id} />
      </EuiPageContent>
    </>
  );
};

export default Room;
