import { largestRect } from "rect-scaler";
import { VIDEO_CONSTRAINS } from "../../../webrtc/config";

const minWidth = VIDEO_CONSTRAINS.low.width.ideal;
const minHeight = (minWidth * 9) / 16;

const calculateLayout = (
  containerWidth,
  containerHeight,
  num,
  pagesCount = 1
) => {
  const { rows, cols, width, height, area } = largestRect(
    containerWidth,
    containerHeight,
    num,
    minWidth,
    minHeight
  );
  // need a better solution
  if ((rows > 1 && height < minHeight) || (cols > 1 && width < minWidth)) {
    return calculateLayout(
      containerWidth,
      containerHeight,
      num - 1,
      pagesCount + 1
    );
  }

  return { rows, cols, width, height, area, pagesCount };
};

const chunkArray = (array, chunk_size) => {
  if (chunk_size === 1) {
    return [array];
  }

  if (chunk_size === array.length) {
    return array.map((elem) => [elem]);
  }

  return array.reduce((resultArray, item, index) => {
    const chunkIndex = Math.ceil(index / chunk_size);

    if (!resultArray[chunkIndex]) {
      resultArray[chunkIndex] = [];
    }

    resultArray[chunkIndex].push(item);

    return resultArray;
  }, []);
};

export { calculateLayout, chunkArray };
