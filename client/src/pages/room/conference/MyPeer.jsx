import React from "react";
import PeerItem from "./PeerItem";

const MyPeer = ({ webCamStream, screenStream, peer }) => {
  return (
    <PeerItem
      peer={peer}
      webCamStream={webCamStream}
      screenStream={screenStream}
    />
  );
};
export default MyPeer;
