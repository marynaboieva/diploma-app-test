import React, { useMemo } from "react";
import { useSelector } from "react-redux";
// Components
import GridView from "./GridView";
import ScreenShareView from "./ScreenShareView";
import MediaControlButton from "../components/MediaControlButton";
import CallButton from "../components/CallButton";

import { EuiModal, EuiModalBody, EuiModalHeader } from "@elastic/eui";
// Hooks
import useMediaContext from "../../../webrtc/hooks/useMediaContext";
// Selectors
import { selectAll } from "../../../redux/slices/conference/selectors";
// Style
import "./style.scss";

const Conference = ({ isModalVisible, toggleModal, endCall }) => {
  const { webcam, screenshare, microphone } = useMediaContext();

  const users = useSelector(selectAll);
  const screenSharingPeers = useMemo(
    () => users.filter(({ screen }) => screen) ?? [],
    [users]
  );
  const otherPeers = useMemo(
    () => users.filter(({ screen }) => !screen),
    [users]
  );

  return (
    isModalVisible && (
      <EuiModal
        maxWidth={false}
        className="conference-modal"
        onClose={toggleModal}
      >
        <EuiModalHeader>
          <div className="media-controls">
            <MediaControlButton
              source="mic"
              enable={microphone.enable}
              disable={microphone.disable}
              pause={microphone.pause}
              resume={microphone.resume}
              isEnabled={microphone.isEnabled}
              isPaused={microphone.isPaused}
              pauseOnToggle
            />
            <MediaControlButton
              source="webcam"
              enable={webcam.enable}
              disable={webcam.disable}
              pause={webcam.pause}
              resume={webcam.resume}
              isEnabled={webcam.isEnabled}
              isPaused={webcam.isPaused}
            />
            <MediaControlButton
              source="screen"
              enable={screenshare.enable}
              disable={screenshare.disable}
              pause={screenshare.pause}
              resume={screenshare.resume}
              isEnabled={screenshare.isEnabled}
              isPaused={screenshare.isPaused}
            />
            <CallButton
              disabled={false}
              isLoading={false}
              onClick={endCall}
              active
            />
          </div>
        </EuiModalHeader>
        <EuiModalBody>
          <div className="conference">
            {!!users.length &&
              (!!screenSharingPeers.length ? (
                <ScreenShareView
                  webCamStream={webcam.stream}
                  screenStream={screenshare.stream}
                  screenSharingPeers={screenSharingPeers}
                  otherPeers={otherPeers}
                />
              ) : (
                <GridView
                  webCamStream={webcam.stream}
                  screenStream={screenshare.stream}
                  users={users}
                />
              ))}
          </div>
        </EuiModalBody>
      </EuiModal>
    )
  );
};

export default Conference;
