import React, { useCallback } from "react";
import { useSelector } from "react-redux";
// Selectors
import { selectUser } from "../../../redux/slices/user/selectors";
// Components
import OtherPeer from "./OtherPeer";
import MyPeer from "./MyPeer";
import { Carousel } from "react-responsive-carousel";
import {
 EuiPanel
} from "@elastic/eui";
// Style
import "./style.scss";

const ScreenShareView = ({
  webCamStream,
  screenStream,
  screenSharingPeers,
  otherPeers,
}) => {
  const { id } = useSelector(selectUser);

  const renderPeer = useCallback(
    (peer) =>
      peer._id === id ? (
        <MyPeer
          key={id}
          peer={peer}
          webCamStream={webCamStream}
          screenStream={screenStream}
        />
      ) : (
        <OtherPeer key={peer._id} peer={peer} />
      ),
    [id, screenStream, webCamStream]
  );

  return (
    <div className="conference-view--screen">
      <Carousel showStatus={false} showThumbs={false}>
        {screenSharingPeers.map((peer) => (
          <div key={peer._id} className="conference-frame">
            <div className="conference-peer-item">{renderPeer(peer)}</div>
          </div>
        ))}
      </Carousel>
      {!!otherPeers.length&& <EuiPanel className="conference-peers-thumbs">
        <Carousel
          centerMode
          centerSlidePercentage={10}
          showStatus={false}
          showThumbs={false}
          dynamicHeight={false}
        >
          {otherPeers.map((peer) => (
            <div key={peer._id} className="conference-peer-item">
              {renderPeer(peer)}
            </div>
          ))}
        </Carousel>
      </EuiPanel>}
    </div>
  );
};

export default ScreenShareView;
