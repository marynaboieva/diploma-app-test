import React, { useCallback, useState } from "react";
import { useSelector } from "react-redux";
// Selectors
import { selectUser } from "../../../redux/slices/user/selectors";
// Components
import OtherPeer from "./OtherPeer";
import MyPeer from "./MyPeer";
import { Carousel } from "react-responsive-carousel";
import { EuiResizeObserver } from "@elastic/eui";
// Style
import "./style.scss";
// Util
import { calculateLayout, chunkArray } from "./utils";

const GridView = ({ webCamStream, screenStream, users }) => {
  const { id } = useSelector(selectUser);
  const [containerHeight, setContainerHeight] = useState(0);
  const [containerWidth, setContainerWidth] = useState(0);

  const renderSlides = useCallback(() => {
    const { width: maxWidth, height: maxHeight, pagesCount } = calculateLayout(
      containerWidth,
      containerHeight,
      users.length
    );
    return chunkArray(users, pagesCount).map((page, index) => (
      <div key={index} className="conference-frame">
        {page.map((peer) => (
          <div
            key={peer._id}
            className="conference-peer-item"
            style={{
              width: "100%",
              height: "100%",
              maxWidth,
              maxHeight,
            }}
          >
            {peer._id === id ? (
              <MyPeer
                peer={peer}
                webCamStream={webCamStream}
                screenStream={screenStream}
              />
            ) : (
              <OtherPeer peer={peer} />
            )}
          </div>
        ))}
      </div>
    ));
  }, [users, webCamStream, screenStream, id, containerHeight, containerWidth]);

  const onResize = ({ width, height }) => {
    setContainerWidth(width);
    setContainerHeight(height);
  };

  return (
    <EuiResizeObserver onResize={onResize}>
      {(resizeRef) => (
        <div className="conference-view--grid" ref={resizeRef}>
          <Carousel showStatus={false} showThumbs={false}>
            {renderSlides()}
          </Carousel>
        </div>
      )}
    </EuiResizeObserver>
  );
};

export default GridView;
