import React, { useMemo } from "react";
import { useSelector } from "react-redux";
// Components
import PeerAudio from "./PeerAudio";
// Selectors
import { selectAll } from "../../../../redux/slices/conference/selectors";
import { selectUser } from "../../../../redux/slices/user/selectors";

const AudioElements = () => {
  const { id } = useSelector(selectUser);
  const users = useSelector(selectAll);
  const usersWithAudio = useMemo(
    () => users.filter(({ mic }) => mic) ?? [],
    [users]
  );

  return usersWithAudio.map((peer) =>
    peer._id !== id ? <PeerAudio peer={peer} /> : null
  );
};

export default AudioElements;
