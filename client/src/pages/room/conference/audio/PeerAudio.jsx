import React, { useRef, useEffect } from "react";
// Hooks
import useConsumer from "../../../../webrtc/hooks/useConsumer";

const PeerAudio = ({ peer }) => {
  const audioConsumer = useConsumer(peer._id, "mic");
  const audioRef = useRef(null);

  useEffect(() => {
    if (audioConsumer?.track && audioRef.current) {
      audioRef.current.srcObject =
        audioConsumer?.track instanceof MediaStream
          ? audioConsumer?.track
          : new MediaStream([audioConsumer?.track]);
    }
  }, [audioConsumer?.track, audioRef]);

  return <audio autoPlay ref={audioRef} />;
};

export default PeerAudio;
