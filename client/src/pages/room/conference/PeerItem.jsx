import React, { useEffect, useRef } from "react";
import PeerLabel from "./PeerLabel";

const PeerItem = ({ webCamStream, screenStream, peer }) => {
  const videoRef = useRef(null);
  const screenRef = useRef(null);

  useEffect(() => {
    if (webCamStream && videoRef.current) {
      videoRef.current.srcObject =
        webCamStream instanceof MediaStream
          ? webCamStream
          : new MediaStream([webCamStream]);
    }
  }, [webCamStream, videoRef]);

  useEffect(() => {
    if (screenStream && screenRef.current) {
      screenRef.current.srcObject =
        screenStream instanceof MediaStream
          ? screenStream
          : new MediaStream([screenStream]);
    }
  }, [screenStream, screenRef]);

  return (
    <>
      <PeerLabel exand={!peer.webcam && !peer.screen} peer={peer} />
      <div
        className={`webcam ${peer.webcam ? "" : "hidden"}  ${
          peer.screen ? "small" : ""
        }`}
      >
        <video autoPlay ref={videoRef} />
      </div>

      <div className={`screen  ${peer.screen ? "" : "hidden"}`}>
        <video autoPlay ref={screenRef} />
      </div>
    </>
  );
};

export default PeerItem;
