import React from "react";
// Hooks
import useConsumer from "../../../webrtc/hooks/useConsumer";
import PeerItem from "./PeerItem";

const OtherPeer = ({ peer }) => {
  const videoConsumer = useConsumer(peer._id, "webcam");
  const screenConsumer = useConsumer(peer._id, "screen");

  return (
    <PeerItem
      peer={peer}
      webCamStream={videoConsumer?.track}
      screenStream={screenConsumer?.track}
    />
  );
};

export default OtherPeer;
