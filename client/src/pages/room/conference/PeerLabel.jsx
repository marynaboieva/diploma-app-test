import React from "react";
import { EuiAvatar } from "@elastic/eui";

const PeerLabel = ({ peer, size = "s", exand }) => {
  return (
    <div className={`peer-label ${exand ? "expand" : ""}`}>
      <EuiAvatar name={peer.username} size={exand ? "xl" : size} />
      <p>{peer.username}</p>
    </div>
  );
};

export default PeerLabel;
