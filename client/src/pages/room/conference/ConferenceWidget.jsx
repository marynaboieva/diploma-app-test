import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
// Components
import MediaControlButton from "../components/MediaControlButton";
import Draggable from "react-draggable"; // The default
import CallButton from "../components/CallButton";
import { EuiAvatar, EuiPanel, EuiButtonIcon } from "@elastic/eui";
// Hooks
import useMediaContext from "../../../webrtc/hooks/useMediaContext";
import useCall from "../useCall";
import Conference from "./Conference";
import AudioElements from "./audio/AudioElements";
// Selectors
import { selectConferenceState } from "../../../redux/slices/conference/selectors";
import { selectById } from "../../../redux/slices/rooms/list/selectors";

const ConferenceWidget = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const conferenceState = useSelector(selectConferenceState);
  const activeRoom = useSelector((state) =>
    selectById(state, conferenceState.id)
  );
  const { endCall, loading, isActive } = useCall(conferenceState.id);
  const { webcam, screenshare, microphone } = useMediaContext();

  const toggleModal = (e) => {
    setIsModalVisible((isVisible) => !isVisible);
  };

  useEffect(() => {
    if (conferenceState?.status === "fulfilled") {
      setIsModalVisible(true);
    }
  }, [conferenceState]);

  if (!activeRoom || !isActive) {
    return null;
  }

  if (isActive && isModalVisible) {
    return (
      <>
        <Conference
          isModalVisible={isModalVisible}
          toggleModal={toggleModal}
          endCall={endCall}
        />
        <AudioElements />
      </>
    );
  }

  return (
    <Draggable handle=".handle">
      <EuiPanel
        style={{ position: "absolute", bottom: 100, zIndex: 1, right: 10 }}
      >
        <EuiButtonIcon color="text" iconType="apps" className="handle" />
        <EuiAvatar
          onClick={toggleModal}
          size="l"
          type="space"
          name={activeRoom.title}
        />
        <CallButton
          disabled={loading === "pending"}
          isLoading={loading === "pending"}
          onClick={endCall}
          active={isActive}
        />
        <div className="media-controls">
          <MediaControlButton
            source="mic"
            enable={microphone.enable}
            disable={microphone.disable}
            pause={microphone.pause}
            resume={microphone.resume}
            isEnabled={microphone.isEnabled}
            isPaused={microphone.isPaused}
            pauseOnToggle
          />
          <MediaControlButton
            source="webcam"
            enable={webcam.enable}
            disable={webcam.disable}
            pause={webcam.pause}
            resume={webcam.resume}
            isEnabled={webcam.isEnabled}
            isPaused={webcam.isPaused}
          />
          <MediaControlButton
            source="screen"
            enable={screenshare.enable}
            disable={screenshare.disable}
            pause={screenshare.pause}
            resume={screenshare.resume}
            isEnabled={screenshare.isEnabled}
            isPaused={screenshare.isPaused}
          />
        </div>
        <AudioElements />
      </EuiPanel>
    </Draggable>
  );
};

export default ConferenceWidget;
