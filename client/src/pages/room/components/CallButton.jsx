import React from "react";
import { EuiButtonIcon, EuiLoadingSpinner } from "@elastic/eui";
import startCall from "../../../icons/phone-call.svg";
import endCall from "../../../icons/end-call.svg";

const CallButton = ({ isLoading, active, disabled = false, onClick }) => {
  return isLoading ? (
    <EuiLoadingSpinner size="m" style={{ margin: 10 }} />
  ) : (
    <EuiButtonIcon
      size="m"
      disabled={disabled}
      onClick={onClick}
      iconType={active ? endCall : startCall}
    />
  );
};

export default CallButton;
