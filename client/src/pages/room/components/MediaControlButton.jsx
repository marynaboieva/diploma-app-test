import React, { useCallback } from "react";
import { EuiButtonIcon } from "@elastic/eui";
import { toast } from "react-toastify";

import micIcon from "../../../icons/microphone.svg";
import camIcon from "../../../icons/webcam.svg";
import shareIcon from "../../../icons/share.svg";

const iconForSource = new Map([
  ["mic", micIcon],
  ["webcam", camIcon],
  ["screen", shareIcon],
]);

const MediaControlButton = ({
  source,
  isEnabled,
  isPaused,
  enable,
  disable,
  pause,
  resume,
  pauseOnToggle = false,
}) => {
  const icon = iconForSource.get(source);

  const handleClick = useCallback(async () => {
    try {
      if (!isEnabled) {
        try {
          await enable();
        } catch (error) {
          const { message } = error;
          toast.error(message);
        }
      } else {
        if (isPaused) {
          resume();
        } else {
          if (pauseOnToggle) {
            pause();
          } else {
            disable();
          }
        }
      }
    } catch (error) {
      toast.error(error.message ?? error.toString());
    }
  }, [isEnabled, isPaused, pause, enable, resume, disable, pauseOnToggle]);

  return (
    <EuiButtonIcon
      onClick={handleClick}
      className={`media-controls-button ${
        !isEnabled || isPaused ? "disabled" : ""
      }`}
      iconType={icon}
      aria-label={source}
      size="m"
    />
  );
};

export default MediaControlButton;
