import React, { useCallback, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
// Components
import ChatBox from "../../../components/chat/ChatBox";
// Selectors
import {
  selectAll,
  selectChatState,
} from "../../../redux/slices/chat/selectors";
// Actions
import {
  getMessages,
  sendMessage,
  deleteMessage,
  editMessage,
  markAsRead,
} from "../../../redux/slices/chat/thunks";
import { setPage, reset } from "../../../redux/slices/chat/chatSlice";

const RoomChat = ({ room_id }) => {
  const { loading, page, limit, total } = useSelector(selectChatState);
  const messages = useSelector(selectAll);

  const dispatch = useDispatch();

  const handleEdit = useCallback(
    (message, body) => {
      dispatch(editMessage({ room_id, message_id: message._id, body }));
    },
    [dispatch, room_id]
  );

  const handleDelete = useCallback(
    (message) => {
      dispatch(deleteMessage({ room_id, message_id: message._id }));
    },
    [dispatch, room_id]
  );

  const handleSendMessage = useCallback(
    (body) => {
      dispatch(sendMessage({ room_id, body }));
    },
    [dispatch, room_id]
  );

  const handleLoadMore = useCallback(() => {
    if (page * limit < total) {
      dispatch(setPage(page + 1));
    }
  }, [dispatch, page, limit, total]);

  useEffect(() => {
    dispatch(getMessages({ room_id, page, limit }));
  }, [dispatch, room_id, page, limit]);

  useEffect(() => {
    messages.forEach((message) => {
      if (message.read !== true) {
        dispatch(markAsRead({ room_id, message_id: message._id }));
      }
    });
  }, [dispatch, messages, room_id]);

  useEffect(() => {
    return () => {
      dispatch(reset());
    };
  }, [dispatch, room_id]);

  return (
    <ChatBox
      messages={messages}
      loading={loading}
      handleDelete={handleDelete}
      handleEdit={handleEdit}
      handleSendMessage={handleSendMessage}
      handleLoadMore={handleLoadMore}
    />
  );
};

export default RoomChat;
