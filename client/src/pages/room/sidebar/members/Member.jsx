import React, { useCallback } from "react";
import { useSelector } from "react-redux";
// Components
import { EuiBadge, EuiContextMenuItem, EuiText } from "@elastic/eui";
import removeUserIcon from "../../../../icons/remove-contact.svg";
import UserItem from "../../../../components/users-list/UserItem";
import UserItemMenu from "../../../../components/users-list/UserItemMenu";
// Selectors
import { selectUser } from "../../../../redux/slices/user/selectors";

const Member = ({ user, admin, onClick }) => {
  const { id } = useSelector(selectUser);

  const handleClick = useCallback(() => {
    onClick(user);
  }, [user, onClick]);

  return (
    <UserItem
      key={user._id}
      user={user}
      extraAction={
        <>
          {admin._id === user._id && <EuiBadge color="#9170B8">Admin</EuiBadge>}
          <UserItemMenu
            user={user}
            extraItems={
              admin._id === id && (
                <EuiContextMenuItem onClick={handleClick} icon={removeUserIcon}>
                  <EuiText color="danger"> Kick out of the room</EuiText>
                </EuiContextMenuItem>
              )
            }
          />
        </>
      }
    />
  );
};

export default Member;
