import React, { useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
// Actions
import { kickOutUser } from "../../../../redux/slices/rooms/selected/thunks";
// Components
import { EuiPanel } from "@elastic/eui";
import UsersList from "../../../../components/users-list/UsersList";
import Member from "./Member";
// Selects
import {
  selectAll,
  selectSelectedRoomState,
} from "../../../../redux/slices/rooms/selected/selectors";

const MembersList = () => {
  const { loading, data } = useSelector(selectSelectedRoomState);
  const participants = useSelector(selectAll);

  const dispatch = useDispatch();

  const handleRemoveUser = useCallback(
    (user) => {
      dispatch(kickOutUser({ room_id: data._id, user_id: user._id }));
    },
    [dispatch, data]
  );

  const renderUser = useCallback(
    (user) => (
      <Member
        key={user._id}
        user={user}
        admin={data.created_by}
        onClick={handleRemoveUser}
      />
    ),
    [handleRemoveUser, data]
  );

  return (
    <EuiPanel hasBorder={false} hasShadow={false}>
      <UsersList
        users={participants}
        loading={loading}
        renderItem={renderUser}
      />
    </EuiPanel>
  );
};

export default MembersList;
