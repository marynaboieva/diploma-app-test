import React, { useState, useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  EuiModal,
  EuiModalBody,
  EuiModalHeader,
  EuiModalHeaderTitle,
  EuiButton,
} from "@elastic/eui";
import { toast } from "react-toastify";
// components
import AddUserItem from "./AddUserItem";
import addUserIcon from "../../../../icons/add-contact.svg";
import UserSearch from "../../../../components/users-search/UserSearch";
// Selectors
import { selectAll as selectParticipants } from "../../../../redux/slices/rooms/selected/selectors";
import {
  selectInvitesState,
  selectInvitesByRoomId,
} from "../../../../redux/slices/rooms/invites/selectors";
import {
  selectSearchState,
  selectAll,
} from "../../../../redux/slices/rooms/search/selectors";
// Actions
import {
  cancelInvite,
  createInvite,
} from "../../../../redux/slices/rooms/invites/thunks";
import { kickOutUser } from "../../../../redux/slices/rooms/selected/thunks";
import {
  setPage,
  setUsername,
  reset,
} from "../../../../redux/slices/rooms/search/searchSlice";
import { searchParticipants } from "../../../../redux/slices/rooms/search/thunks";

const AddUserModal = ({ room }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const invites = useSelector(selectInvitesByRoomId(room._id));
  const participants = useSelector(selectParticipants);
  const { error } = useSelector(selectInvitesState);
  const {
    error: searchError,
    loading,
    username,
    page,
    total,
    limit,
  } = useSelector(selectSearchState);

  const users = useSelector(selectAll);

  const dispatch = useDispatch();

  const toggleModal = (e) => {
    setIsModalVisible((isVisible) => !isVisible);
  };

  const handleSearch = useCallback(
    (newUsername) => {
      dispatch(setUsername(newUsername));
    },
    [dispatch]
  );

  const onPageClick = useCallback(
    (newPage) => {
      dispatch(setPage(newPage));
    },
    [dispatch]
  );

  const handleCreateInvite = useCallback(
    (receiver) => {
      dispatch(createInvite({ receiver_id: receiver._id, room_id: room._id }));
    },
    [dispatch, room]
  );

  const handleCancelInvite = useCallback(
    (receiver) => {
      const invite = invites.find((inv) => receiver._id === inv.receiver._id);
      dispatch(cancelInvite(invite._id));
    },
    [dispatch, invites]
  );

  const handleRemoveUser = useCallback(
    (user) => {
      dispatch(kickOutUser({ user_id: user._id, room_id: room._id }));
    },
    [dispatch, room]
  );

  const renderUser = useCallback(
    (user) => (
      <AddUserItem
        key={user._id}
        user={user}
        isMember={participants.some(
          (participant) => participant._id === user._id
        )}
        isInvited={invites.some((inv) => user._id === inv.receiver._id)}
        handleCreate={handleCreateInvite}
        handleCancel={handleCancelInvite}
        handleRemove={handleRemoveUser}
      />
    ),
    [
      participants,
      invites,
      handleCreateInvite,
      handleCancelInvite,
      handleRemoveUser,
    ]
  );

  useEffect(() => {
    if (isModalVisible) {
      dispatch(
        searchParticipants({ room_id: room._id, username, page, limit })
      );
    }
  }, [username, page, limit, dispatch, room, isModalVisible]);

  useEffect(
    () => () => {
      dispatch(reset());
    },
    [dispatch]
  );

  useEffect(() => {
    if (error) {
      toast.error(error);
    }
  }, [error]);

  useEffect(() => {
    if (searchError) {
      toast.error(searchError);
    }
  }, [searchError]);

  return (
    <>
      <EuiButton
        onClick={toggleModal}
        iconType={addUserIcon}
        color="secondary"
        size="m"
      >
        Invite users
      </EuiButton>
      {isModalVisible && (
        <EuiModal onClose={toggleModal}>
          <EuiModalHeader>
            <EuiModalHeaderTitle>
              <h1>Invite users</h1>
            </EuiModalHeaderTitle>
          </EuiModalHeader>

          <EuiModalBody>
            <UserSearch
              users={users}
              username={username}
              loading={loading}
              limit={limit}
              page={page}
              total={total}
              onPageClick={onPageClick}
              handleSearch={handleSearch}
              renderUser={renderUser}
            />
          </EuiModalBody>
        </EuiModal>
      )}
    </>
  );
};

export default AddUserModal;
