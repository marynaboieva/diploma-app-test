import React, { useCallback } from "react";
// Components
import { EuiButton, EuiButtonEmpty } from "@elastic/eui";
import addUserIcon from "../../../../icons/add-contact.svg";
import removeUserIcon from "../../../../icons/remove-contact.svg";
import UserItem from "../../../../components/users-list/UserItem";

const AddUserItem = ({
  user,
  isInvited,
  isMember,
  handleCreate,
  handleCancel,
  handleRemove,
}) => {
  const onCreate = useCallback(() => {
    handleCreate(user);
  }, [user, handleCreate]);

  const onCancel = useCallback(() => {
    handleCancel(user);
  }, [user, handleCancel]);

  const onRemove = useCallback(() => {
    handleRemove(user);
  }, [user, handleRemove]);

  return (
    <UserItem
      key={user._id}
      user={user}
      extraAction={
        <>
          {!isMember &&
            (isInvited ? (
              <EuiButtonEmpty color="danger" onClick={onCancel}>
                Cancel
              </EuiButtonEmpty>
            ) : (
              <EuiButton
                iconType={addUserIcon}
                color="secondary"
                onClick={onCreate}
              >
                Invite
              </EuiButton>
            ))}
          {isMember && (
            <EuiButtonEmpty
              onClick={onRemove}
              color="danger"
              icon={removeUserIcon}
            >
              Kick out of the room
            </EuiButtonEmpty>
          )}
        </>
      }
    />
  );
};

export default AddUserItem;
