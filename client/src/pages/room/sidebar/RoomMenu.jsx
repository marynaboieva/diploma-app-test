import React, { useState } from "react";
import { useSelector } from "react-redux";
// Components
import {
  EuiFlyout,
  EuiButtonIcon,
  EuiFlyoutBody,
  EuiTabbedContent,
  EuiFlexGroup,
  EuiSpacer,
} from "@elastic/eui";
import MembersList from "./members/MembersList";
import IvitedUsersList from "./invites/IvitedUsersList";
import AddUserModal from "./add-user/AddUserModal";
import RoomActionsPopover from "./RoomActionsPopover";
// Selects
import { selectSelectedRoomState } from "../../../redux/slices/rooms/selected/selectors";
import { selectUser } from "../../../redux/slices/user/selectors";
// Style
import "./style.scss";

const tabs = [
  {
    id: "all-users",
    name: "Members",
    content: <MembersList />,
  },
  {
    id: "invites",
    name: "Invited",
    content: <IvitedUsersList />,
  },
];

const RoomMenu = () => {
  const [isOpen, setIsOpen] = useState();
  const { loading, data } = useSelector(selectSelectedRoomState);
  const user = useSelector(selectUser);

  return (
    <>
      <EuiButtonIcon
        size="m"
        isDisabled={loading === "pending"}
        aria-label="participants list"
        iconType="users"
        onClick={() => setIsOpen(!isOpen)}
      />
      {isOpen && (
        <EuiFlyout
          size="s"
          className="room-users-list --collapsible"
          onClose={() => {
            setIsOpen(false);
          }}
        >
          <EuiFlyoutBody>
            <EuiSpacer size="xl" />
            <EuiFlexGroup
              justifyContent={
                data?.created_by._id === user.id ? "spaceBetween" : "flexEnd"
              }
            >
              {data?.created_by._id === user.id && <AddUserModal room={data} />}
              <RoomActionsPopover room={data} user={user} />
            </EuiFlexGroup>
            <EuiSpacer />
            {data?.created_by._id === user.id ? (
              <EuiTabbedContent tabs={tabs} />
            ) : (
              <MembersList />
            )}
          </EuiFlyoutBody>
        </EuiFlyout>
      )}
    </>
  );
};

export default RoomMenu;
