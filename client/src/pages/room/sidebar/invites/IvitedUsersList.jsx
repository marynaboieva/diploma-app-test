import React from "react";
import { useSelector } from "react-redux";
// Components
import { EuiPanel, EuiEmptyPrompt } from "@elastic/eui";
import UsersList from "../../../../components/users-list/UsersList";
import InvitedUser from "./InvitedUser";
// Selects
import {
  selectInvitesByRoomId,
  selectInvitesState,
} from "../../../../redux/slices/rooms/invites/selectors";
import { selectSelectedRoomState } from "../../../../redux/slices/rooms/selected/selectors";

const renderUser = (invite) => <InvitedUser invite={invite} />;

const IvitedUsersList = () => {
  const { data } = useSelector(selectSelectedRoomState);

  const { loading } = useSelector(selectInvitesState);
  const invites = useSelector(selectInvitesByRoomId(data?._id));

  return (
    <EuiPanel hasBorder={false} hasShadow={false}>
      <UsersList
        users={invites}
        loading={loading}
        renderItem={renderUser}
        emptyPrompt={
          <EuiEmptyPrompt
            iconType="users"
            title={<h2>You haven't invited anyone yet</h2>}
          />
        }
      />
    </EuiPanel>
  );
};

export default IvitedUsersList;
