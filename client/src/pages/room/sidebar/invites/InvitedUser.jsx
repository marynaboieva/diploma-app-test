import React, { useCallback } from "react";
import { useDispatch } from "react-redux";
// Actions
import { cancelInvite } from "../../../../redux/slices/rooms/invites/thunks";
// Components
import { EuiButtonEmpty } from "@elastic/eui";
import UserItem from "../../../../components/users-list/UserItem";

const InvitedUser = ({ invite }) => {
  const dispatch = useDispatch();

  const onCancel = useCallback(() => {
    dispatch(cancelInvite(invite._id));
  }, [dispatch, invite]);

  return (
    <UserItem
      key={invite._id}
      user={invite.receiver}
      extraAction={
        <EuiButtonEmpty onClick={onCancel} color="danger">
          Cancel
        </EuiButtonEmpty>
      }
    />
  );
};

export default InvitedUser;
