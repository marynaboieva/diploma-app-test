import React, { useCallback, useState } from "react";
import { useDispatch } from "react-redux";
// Components
import {
  EuiPopover,
  EuiContextMenuItem,
  EuiButtonIcon,
  EuiText,
} from "@elastic/eui";
import dotsIcon from "../../../icons/ellipsis.svg";
// Actions
import {
  leaveRoom,
  deleteRoom,
} from "../../../redux/slices/rooms/selected/thunks";

const RoomActionsPopover = ({ room, user }) => {
  const [isPopoverOpen, setPopover] = useState(false);

  const dispatch = useDispatch();

  const onButtonClick = () => {
    setPopover(!isPopoverOpen);
  };

  const closePopover = () => {
    setPopover(false);
  };

  const onDeleteRoom = useCallback(() => {
    dispatch(deleteRoom(room._id));
  }, [room?._id, dispatch]);

  const onLeaveRoom = useCallback(() => {
    dispatch(leaveRoom(room._id));
  }, [room?._id, dispatch]);

  return (
    <EuiPopover
      id="room-actions"
      button={
        <EuiButtonIcon
          area-aria-label="room actions menu"
          onClick={onButtonClick}
          iconType={dotsIcon}
        />
      }
      isOpen={isPopoverOpen}
      closePopover={closePopover}
      panelPaddingSize="none"
      anchorPosition="downLeft"
    >
      {user.id === room.created_by._id ? (
        <EuiContextMenuItem onClick={onDeleteRoom} icon="trash">
          <EuiText color="danger">Delete room</EuiText>
        </EuiContextMenuItem>
      ) : (
        <EuiContextMenuItem onClick={onLeaveRoom} icon="exit">
          <EuiText color="danger">Leave room</EuiText>
        </EuiContextMenuItem>
      )}
    </EuiPopover>
  );
};

export default RoomActionsPopover;
