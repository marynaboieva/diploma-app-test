import React from "react";
import {
  EuiHeader,
  EuiHeaderLogo,
  EuiHeaderSectionItem,
  EuiHeaderSection,
} from "@elastic/eui";

const Header = ({ children, toggleMenuOpen }) => {
  return (
    <EuiHeader position="fixed">
      <EuiHeaderSection grow={false}>
        <EuiHeaderSectionItem border="right" className="logo-section">
          <EuiHeaderLogo
            onClick={toggleMenuOpen}
            component="span"
            iconType="menu"
          />
        </EuiHeaderSectionItem>
      </EuiHeaderSection>
      {children}
    </EuiHeader>
  );
};

export default Header;
