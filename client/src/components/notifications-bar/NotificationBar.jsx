import React, { useMemo } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
// Components
import { EuiShowFor, EuiFlexItem } from "@elastic/eui";
import NotificationIcon from "./NotificationIcon";
import channelIcon from "../../icons/loud-speaker.svg";
import friendsIcon from "../../icons/friends.svg";
import sendIcon from "../../icons/send.svg";

// Selectors
import { selectReceivedInvites } from "../../redux/slices/rooms/invites/selectors";
import { selectAll as selectAllRequests } from "../../redux/slices/friends/requests/selectors";
import { selectAll as selectAllRooms } from "../../redux/slices/rooms/list/selectors";

// Styles
import "./icon-styles.scss";

const NotificationBar = ({ toggleMenuOpen }) => {
  const invites = useSelector(selectReceivedInvites);
  const requests = useSelector(selectAllRequests);
  const rooms = useSelector(selectAllRooms);

  const messages = useMemo(
    () =>
      rooms.reduce(
        (messagesCount, room) => (messagesCount += room?.messages ?? 0),
        0
      ),
    [rooms]
  );

  const history = useHistory();

  const onRequestsClick = () => {
    history.push("/requests");
  };

  return (
    <EuiShowFor sizes={["xs", "s"]}>
      <>
        <EuiFlexItem grow={false}>
          <NotificationIcon
            onClick={toggleMenuOpen}
            icon={sendIcon}
            count={messages}
          />
        </EuiFlexItem>
        <EuiFlexItem grow={false}>
          <NotificationIcon
            onClick={onRequestsClick}
            icon={friendsIcon}
            count={requests.length}
          />
        </EuiFlexItem>
        <EuiFlexItem grow={false}>
          <NotificationIcon
            onClick={toggleMenuOpen}
            icon={channelIcon}
            count={invites.length}
          />
        </EuiFlexItem>
      </>
    </EuiShowFor>
  );
};

export default NotificationBar;
