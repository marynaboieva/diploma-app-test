import React from "react";
// Components
import { EuiIcon, EuiBadge } from "@elastic/eui";
// Styles
import "./icon-styles.scss";

const NotificationIcon = ({ count, icon, color, onClick }) => {
  return (
    <div className="notification-icon" onClick={onClick}>
      <EuiIcon color="text" size="l" type={icon} />
      {!!count && (
        <EuiBadge className="small-badge" color={color ?? "danger"}>
          {count}
        </EuiBadge>
      )}
    </div>
  );
};

export default NotificationIcon;
