import React, { useCallback, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// Components
import {
  EuiPopover,
  EuiContextMenuItem,
  EuiButtonIcon,
  EuiText,
  EuiHorizontalRule,
} from "@elastic/eui";
import addFriendIcon from "../../icons/add-contact.svg";
import removeFriendIcon from "../../icons/remove-contact.svg";
// import phoneIcon from "../../icons/phone-call.svg";
// import chatIcon from "../../icons/chat.svg";
import dotsIcon from "../../icons/ellipsis.svg";
// Actions
import {
  cancelRequest,
  createRequest,
} from "../../redux/slices/friends/requests/thunks";
import { deleteFriend } from "../../redux/slices/friends/list/thunks";
// Selectors
import { selectUser } from "../../redux/slices/user/selectors";
import { selectById } from "../../redux/slices/friends/list/selectors";
import { selectAll } from "../../redux/slices/friends/requests/selectors";

const UserItemMenu = ({ user, extraItems, renderButton }) => {
  const [isPopoverOpen, setPopover] = useState(false);

  const { id } = useSelector(selectUser);
  const friend = useSelector((state) => selectById(state, user._id));
  const requests = useSelector(selectAll);
  const request = useMemo(
    () => requests.find(({ receiver }) => receiver._id === user._id),
    [requests, user._id]
  );

  const dispatch = useDispatch();

  const onButtonClick = () => {
    setPopover(!isPopoverOpen);
  };

  const closePopover = () => {
    setPopover(false);
  };

  const addToFriends = useCallback(() => {
    dispatch(createRequest(user._id));
  }, [user, dispatch]);

  const removeFromFriends = useCallback(() => {
    dispatch(deleteFriend(user._id));
  }, [user, dispatch]);

  const onCancelRequest = useCallback(() => {
    dispatch(cancelRequest(request._id));
  }, [request, dispatch]);

  if (user._id === id) {
    return null;
  }

  return (
    <EuiPopover
      id="singlePanel"
      button={
        <EuiButtonIcon
          area-aria-label="user menu"
          onClick={onButtonClick}
          iconType={dotsIcon}
        />
      }
      isOpen={isPopoverOpen}
      closePopover={closePopover}
      panelPaddingSize="none"
      anchorPosition="downLeft"
    >
      {!friend && !request && (
        <EuiContextMenuItem onClick={addToFriends} icon={addFriendIcon}>
          <EuiText color="secondary">Add to friends</EuiText>
        </EuiContextMenuItem>
      )}
      {!!friend && (
        <EuiContextMenuItem onClick={removeFromFriends} icon={removeFriendIcon}>
          <EuiText color="danger">Remove from friends</EuiText>
        </EuiContextMenuItem>
      )}
      {!!request && (
        <EuiContextMenuItem onClick={onCancelRequest}>
          <EuiText color="danger">Cancel request </EuiText>
        </EuiContextMenuItem>
      )}
      {/* <EuiContextMenuItem disabled icon={chatIcon}>
        <EuiText>Message</EuiText>
      </EuiContextMenuItem>
      <EuiContextMenuItem disabled icon={phoneIcon}>
        <EuiText> Call</EuiText>
      </EuiContextMenuItem> */}
      {extraItems && <EuiHorizontalRule />}
      {extraItems}
    </EuiPopover>
  );
};

export default UserItemMenu;
