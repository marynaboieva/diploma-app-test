import React from "react";
// Components
import { EuiFlexItem, EuiAvatar, EuiFlexGroup } from "@elastic/eui";
import UserStateIndicator from "./UserStateIndicator";

const UserItem = ({ user, extraAction }) => {
  return (
    <EuiFlexGroup
      gutterSize="none"
      alignItems="center"
      className="users-list-item"
      responsive={false}
    >
      <UserStateIndicator user={user} />
      <EuiFlexItem grow={false} className="user-avatar">
        <EuiAvatar size="l" name={user.username} />
      </EuiFlexItem>
      <EuiFlexItem>{user.username}</EuiFlexItem>
      {extraAction}
    </EuiFlexGroup>
  );
};

export default UserItem;
