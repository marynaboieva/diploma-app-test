import React from "react";
// Components
import { EuiFlexItem, EuiFlexGroup, EuiHealth, EuiBeacon } from "@elastic/eui";

const UserStateIndicator = ({ user }) => {
  return (
    <div className="user-state-indicator">
      {user?.streaming ? (
        <EuiFlexGroup>
          <EuiFlexItem>
            <EuiBeacon size={8} />
          </EuiFlexItem>
        </EuiFlexGroup>
      ) : (
        user?.online && <EuiHealth color="success" size="s" />
      )}
    </div>
  );
};

export default UserStateIndicator;
