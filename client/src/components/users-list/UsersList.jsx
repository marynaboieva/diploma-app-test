import React from "react";
// Components
import UserItem from "./UserItem";
import UserItemMenu from "./UserItemMenu";
import LoadingItem from "../loading/LoadingItem";
import { EuiEmptyPrompt, EuiFlexGroup } from "@elastic/eui";
// Utils
import { randomArray } from "../../utils/randomArray";
import "./styles.scss";

const renderUser = (user) => (
  <UserItem
    key={user._id}
    user={user}
    extraAction={<UserItemMenu user={user} />}
  />
);

const UsersList = ({
  users = [],
  loading,
  emptyPrompt,
  renderItem = renderUser,
}) => {
  if (loading !== "pending" && !users.length) {
    return (
      emptyPrompt ?? (
        <EuiFlexGroup
          grow
          className="users-list"
          direction="column"
          gutterSize="l"
        >
          <EuiEmptyPrompt iconType="users" title={<h2>No users found</h2>} />
        </EuiFlexGroup>
      )
    );
  }

  return (
    <EuiFlexGroup className="users-list" direction="column" gutterSize="l">
      {loading === "pending"
        ? randomArray(5, 5).map((value) => (
            <LoadingItem key={value} lines={1} />
          ))
        : users.map((user) => renderItem(user))}
    </EuiFlexGroup>
  );
};

export default UsersList;
