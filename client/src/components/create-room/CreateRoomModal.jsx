import React, { useState, useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import {
  EuiButton,
  EuiButtonEmpty,
  EuiForm,
  EuiFormRow,
  EuiFieldText,
  EuiModal,
  EuiModalBody,
  EuiModalFooter,
  EuiModalHeader,
  EuiModalHeaderTitle,
  EuiIcon,
  EuiListGroupItem,
} from "@elastic/eui";
// Selectors
import { selectCreateRoomState } from "../../redux/slices/rooms/create/selectors";
// Actions
import { createRoom } from "../../redux/slices/rooms/create/thunks";
import { toast } from "react-toastify";

const CreateRoomModal = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const { submit, error } = useSelector(selectCreateRoomState);
  const { register, handleSubmit } = useForm();
  const dispatch = useDispatch();

  const toggleModal = (e) => {
    setIsModalVisible((isVisible) => !isVisible);
  };

  const handleCreate = useCallback(
    (data) => {
      dispatch(createRoom(data));
      setIsModalVisible(false);
    },
    [dispatch]
  );

  useEffect(() => {
    if (error) {
      toast.error(error);
    }
  }, [error]);

  return (
    <>
      <EuiListGroupItem
        icon={
          <EuiIcon
            type="plus"
            color="success"
            size="xl"
            aria-label="create room"
          />
        }
        onClick={toggleModal}
        label="Create room"
      />
      {isModalVisible && (
        <EuiModal onClose={toggleModal}>
          <EuiModalHeader>
            <EuiModalHeaderTitle>
              <h1>Create room</h1>
            </EuiModalHeaderTitle>
          </EuiModalHeader>

          <EuiModalBody>
            <EuiForm
              id="createRoomForm"
              component="form"
              onSubmit={handleSubmit(handleCreate)}
            >
              <EuiFormRow label="Title">
                <EuiFieldText
                  inputRef={register({ required: true })}
                  name="title"
                  type="title"
                  placeholder="New room 111"
                />
              </EuiFormRow>
            </EuiForm>
          </EuiModalBody>

          <EuiModalFooter>
            <EuiButtonEmpty onClick={toggleModal}>Cancel</EuiButtonEmpty>
            <EuiButton
              isLoading={submit === "pending"}
              type="submit"
              form="createRoomForm"
              fill
            >
              Create
            </EuiButton>
          </EuiModalFooter>
        </EuiModal>
      )}
    </>
  );
};

export default CreateRoomModal;
