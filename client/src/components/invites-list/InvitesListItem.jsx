import React, { useCallback } from "react";
// Components
import {
  EuiFlexGroup,
  EuiFlexItem,
  EuiAvatar,
  EuiIcon,
  EuiButtonEmpty,
} from "@elastic/eui";

const InvitesListItem = ({ invite, onAccept, onDecline }) => {
  const onAcceptClick = useCallback(() => {
    onAccept(invite);
  }, [invite, onAccept]);

  const onDeclineClick = useCallback(() => {
    onDecline(invite);
  }, [invite, onDecline]);

  return (
    <EuiFlexGroup
      gutterSize="none"
      alignItems="center"
      className="invites-list-item"
      responsive={false}
    >
      <EuiFlexItem className="ivite-avatar" grow={false}>
        <EuiAvatar type="space" size="l" name={invite.room.title} />
      </EuiFlexItem>
      <EuiIcon type="sortLeft" size="m" color="secondary" />
      <EuiFlexItem className="ivite-avatar" grow={false}>
        <EuiAvatar size="l" name={invite.sender.username} />
      </EuiFlexItem>
      <EuiFlexItem>
        <EuiButtonEmpty size="xs" color="danger" onClick={onDeclineClick}>
          Decline
        </EuiButtonEmpty>
      </EuiFlexItem>
      <EuiFlexItem>
        <EuiButtonEmpty size="xs" color="success" onClick={onAcceptClick}>
          Accept
        </EuiButtonEmpty>
      </EuiFlexItem>
    </EuiFlexGroup>
  );
};

export default InvitesListItem;
