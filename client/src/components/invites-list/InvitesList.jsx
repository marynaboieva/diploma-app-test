import React, { useCallback, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
// Actions
import {
  acceptInvite,
  declineInvite,
} from "../../redux/slices/rooms/invites/thunks";
// Components
import { EuiCollapsibleNavGroup, EuiListGroup } from "@elastic/eui";
import InvitesListItem from "./InvitesListItem";
import NotificationIcon from "../notifications-bar/NotificationIcon";
import { toast } from "react-toastify";
import channelIcon from "../../icons/loud-speaker.svg";
// Selectors
import {
  selectInvitesState,
  selectReceivedInvites,
} from "../../redux/slices/rooms/invites/selectors";

// Styles
import "./style.scss";

const InvitesList = () => {
  const { error } = useSelector(selectInvitesState);
  const invites = useSelector(selectReceivedInvites);

  const dispatch = useDispatch();

  const handleAccept = useCallback(
    (invite) => {
      dispatch(acceptInvite(invite._id));
    },
    [dispatch]
  );

  const handleDecline = useCallback(
    (invite) => {
      dispatch(declineInvite(invite._id));
    },
    [dispatch]
  );

  useEffect(() => {
    if (error) {
      toast.error(error);
    }
  }, [error]);

  if (!invites.length) {
    return <></>;
  }

  return (
    <EuiCollapsibleNavGroup
      titleSize="s"
      title={<NotificationIcon icon={channelIcon} count={invites.length} />}
      isCollapsible={true}
      className="invites-list"
    >
      <EuiListGroup>
        {!!invites.length &&
          invites.map((invite) => (
            <InvitesListItem
              key={invite._id}
              invite={invite}
              onAccept={handleAccept}
              onDecline={handleDecline}
            />
          ))}
      </EuiListGroup>
    </EuiCollapsibleNavGroup>
  );
};

export default InvitesList;
