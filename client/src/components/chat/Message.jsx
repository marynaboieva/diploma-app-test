import React, { useCallback } from "react";
import { useSelector } from "react-redux";
// Components
import { EuiAvatar, EuiMarkdownFormat, EuiText } from "@elastic/eui";
import MessageActions from "./MessageActions";
// Selectors
import { selectUser } from "../../redux/slices/user/selectors";
// Utils
import moment from "moment";

const getDate = (date) => moment(date).fromNow();

const Message = ({ message, handleEdit, handleDelete }) => {
  const { id } = useSelector(selectUser);

  const onEdit = useCallback(() => {
    handleEdit(message);
  }, [message, handleEdit]);

  const onDelete = useCallback(() => {
    handleDelete(message);
  }, [message, handleDelete]);

  return (
    <div className="chat-message">
      <EuiAvatar
        className="user-avatar"
        size="m"
        name={message.author.username}
      />
      <EuiText size="m" className="author-username">
        {message.author.username}
      </EuiText>
      {id === message.author._id && (
        <MessageActions onDelete={onDelete} onEdit={onEdit} />
      )}
      <div className="message-body">
        <EuiMarkdownFormat>{message.body}</EuiMarkdownFormat>
      </div>
      <EuiText className="message-edited" color="subdued" size="xs">
        {message.edited ? "(edited)" : ""}
      </EuiText>

      <EuiText color="subdued" size="xs" className="message-date">
        {getDate(message.created)}
      </EuiText>
    </div>
  );
};

export default Message;
