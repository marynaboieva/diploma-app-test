import React, { useCallback, useEffect } from "react";
// Components
import { EuiTextArea, EuiForm, EuiButtonIcon } from "@elastic/eui";
import { useForm } from "react-hook-form";
import sendIcon from "../../icons/send.svg";
// Style
import "./style.scss";

const ChatInput = ({ handleSend, initialValue = "" }) => {
  const { register, handleSubmit, reset } = useForm();

  const onSend = useCallback(
    (data) => {
      handleSend(data);
      reset();
    },
    [handleSend, reset]
  );

  useEffect(() => {
    reset({ body: initialValue });
  }, [reset, initialValue]);

  return (
    <div className="chat-input">
      <EuiForm component="form" onSubmit={handleSubmit(onSend)}>
        <EuiTextArea
          rows={2}
          compressed
          fullWidth
          resize="none"
          inputRef={register({ required: true })}
          name="body"
        />

        <EuiButtonIcon
          size="m"
          className="send-message-button"
          iconType={sendIcon}
          type="submit"
        />
      </EuiForm>
    </div>
  );
};

export default ChatInput;
