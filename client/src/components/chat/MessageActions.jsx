import React, { useCallback, useState } from "react";
// Components
import {
  EuiPopover,
  EuiContextMenuItem,
  EuiButtonIcon,
  EuiText,
  EuiIcon,
} from "@elastic/eui";

import dotsIcon from "../../icons/ellipsis.svg";

const MessageActions = ({ onEdit, onDelete }) => {
  const [isPopoverOpen, setPopover] = useState(false);

  const onButtonClick = () => {
    setPopover(!isPopoverOpen);
  };

  const closePopover = () => {
    setPopover(false);
  };

  const onEditClick = useCallback(() => {
    onEdit();
  }, [onEdit]);

  const onDeleteClick = useCallback(() => {
    onDelete();
  }, [onDelete]);

  return (
    <EuiPopover
      className="message-actions"
      button={
        <EuiButtonIcon
          area-aria-label="message menu"
          onClick={onButtonClick}
          iconType={dotsIcon}
        />
      }
      isOpen={isPopoverOpen}
      closePopover={closePopover}
      panelPaddingSize="none"
      anchorPosition="downLeft"
    >
      <EuiContextMenuItem
        onClick={onEditClick}
        icon={<EuiIcon type="pencil" color="warning" />}
      >
        <EuiText color="warning">Edit message</EuiText>
      </EuiContextMenuItem>

      <EuiContextMenuItem
        onClick={onDeleteClick}
        icon={<EuiIcon color="danger" type="trash" />}
      >
        <EuiText color="danger">Delete message</EuiText>
      </EuiContextMenuItem>
    </EuiPopover>
  );
};

export default MessageActions;
