import React, { useCallback, useEffect, useRef, useState } from "react";
// Components
import chatIcon from "../../icons/chat.svg";
import { Scrollbars } from "react-custom-scrollbars";
import ChatInput from "./ChatInput";
import Message from "./Message";
import LoadingItem from "../loading/LoadingItem";
import { EuiEmptyPrompt, EuiFlexGroup } from "@elastic/eui";
// Utils
import { randomArray } from "../../utils/randomArray";
// Style
import "./style.scss";

const ChatBox = ({
  messages = [],
  loading,
  handleEdit,
  handleDelete,
  handleSendMessage,
  handleLoadMore,
}) => {
  const [editableMessage, setEditableMessage] = useState(null);

  const containerRef = useRef(null);

  const onEditRequest = useCallback((message) => {
    setEditableMessage(message);
  }, []);

  const handleSend = useCallback(
    (newMessage) => {
      if (editableMessage) {
        handleEdit(editableMessage, newMessage.body);
        setEditableMessage(null);
      } else {
        handleSendMessage(newMessage.body);
      }
    },
    [editableMessage, handleEdit, handleSendMessage]
  );

  const onScrollFrame = useCallback(
    ({ top }) => {
      if (top === 0) {
        handleLoadMore();
      }
    },
    [handleLoadMore]
  );

  useEffect(() => {
    if (containerRef.current && messages.length) {
      console.log("scroll");
      containerRef.current.scrollToBottom();
    }
  }, [containerRef, messages.length]);

  return (
    <div className="chat">
      <div className="chat-box">
        <Scrollbars
          onScrollFrame={onScrollFrame}
          hideTracksWhenNotNeeded
          height="100%"
          ref={containerRef}
        >
          {loading === "pending" && (
            <div className="chat-box" style={{ overflow: "hidden" }}>
              {randomArray(20).map((value) => (
                <LoadingItem key={value} lines={2} />
              ))}
            </div>
          )}
          <div className="chat-messages-list">
            {loading !== "pending" && !messages.length ? (
              <EuiFlexGroup
                grow
                className="chat-box"
                direction="column"
                gutterSize="l"
              >
                <EuiEmptyPrompt
                  iconType={chatIcon}
                  title={<h2>No messages found</h2>}
                />
              </EuiFlexGroup>
            ) : (
              messages.map((message) => (
                <Message
                  key={message._id}
                  message={message}
                  handleEdit={onEditRequest}
                  handleDelete={handleDelete}
                />
              ))
            )}
          </div>
        </Scrollbars>
      </div>
      <ChatInput initialValue={editableMessage?.body} handleSend={handleSend} />
    </div>
  );
};

export default ChatBox;
