import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  EuiAvatar,
  EuiHeaderSectionItemButton,
  EuiFlexGroup,
  EuiFlexItem,
  EuiText,
  EuiButtonIcon,
  EuiHideFor,
} from "@elastic/eui";
import AvatarLoading from "../loading/AvatarLoading";
import NotificationBar from "../notifications-bar/NotificationBar";
// Selectors
import { selectUser } from "../../redux/slices/user/selectors";
// Actions
import { logout } from "../../redux/slices/user/thunks";

const CurrentUser = ({ toggleMenuOpen }) => {
  const { loading, name } = useSelector(selectUser);

  const dispatch = useDispatch();

  const onLogout = () => {
    dispatch(logout());
  };

  if (!name || loading === "pending") {
    return (
      <EuiHeaderSectionItemButton disabled>
        <AvatarLoading />
      </EuiHeaderSectionItemButton>
    );
  }

  return (
    <EuiFlexGroup responsive={false} alignItems="center">
      <EuiFlexItem grow={false}>
        <EuiAvatar name={name} size="l" />
      </EuiFlexItem>
      <EuiFlexItem grow>
        <EuiHideFor sizes={["xs"]}>
          <EuiText color="default">{name}</EuiText>
        </EuiHideFor>
      </EuiFlexItem>

      <NotificationBar toggleMenuOpen={toggleMenuOpen} />

      <EuiFlexItem grow={false}>
        <EuiButtonIcon
          size="m"
          disabled
          iconType="user"
          area-label="user settings page"
          color="text"
        />
      </EuiFlexItem>
      <EuiFlexItem grow={false}>
        <EuiButtonIcon
          size="m"
          iconType="exit"
          area-label="log out"
          onClick={onLogout}
          color="text"
        />
      </EuiFlexItem>
    </EuiFlexGroup>
  );
};

export default CurrentUser;
