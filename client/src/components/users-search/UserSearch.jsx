import React from "react";
// Components
import UsersList from "../users-list/UsersList";
import {
  EuiFieldSearch,
  EuiPagination,
  EuiSpacer,
  EuiEmptyPrompt,
} from "@elastic/eui";

const UserSearch = ({
  renderUser,
  users,
  loading,
  username,
  page,
  limit,
  total,
  onPageClick,
  handleSearch,
}) => {
  return (
    <>
      <EuiFieldSearch
        fullWidth
        onSearch={handleSearch}
        placeholder="Search by username"
        defaultValue={username}
      />
      <EuiSpacer size="l" />

      <UsersList
        users={users}
        loading={loading}
        renderItem={renderUser}
        emptyPrompt={
          <EuiEmptyPrompt
            iconType="search"
            title={<h2>No users found for your request</h2>}
          />
        }
      />
      <EuiSpacer size="l" />

      <EuiPagination
        pageCount={Math.ceil(total / limit)}
        activePage={page}
        onPageClick={onPageClick}
      />
    </>
  );
};

export default UserSearch;
