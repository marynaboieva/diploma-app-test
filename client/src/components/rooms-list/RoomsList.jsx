import React, { useEffect, useCallback } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
// Components
import { EuiCollapsibleNavGroup, EuiIcon, EuiListGroup } from "@elastic/eui";
import LoadingItem from "../loading/LoadingItem";
import RoomsListItem from "./RoomsListItem";
import { toast } from "react-toastify";
import channelIcon from "../../icons/loud-speaker.svg";
// Selectors
import {
  selectRoomsListState,
  selectAll,
} from "../../redux/slices/rooms/list/selectors";
// Utils
import { randomArray } from "../../utils/randomArray";
// Styles
import "./rooms-list.scss";

const RoomsList = () => {
  const { error, loading } = useSelector(selectRoomsListState);
  const rooms = useSelector(selectAll);

  const history = useHistory();

  const onClick = useCallback(
    (room_id) => {
      history.push(`/room/${room_id}`);
    },
    [history]
  );

  useEffect(() => {
    if (error) {
      toast.error(error);
    }
  }, [error]);

  if (loading !== "pending" && !rooms.length) {
    return null;
  }

  return (
    <EuiCollapsibleNavGroup
      titleSize="s"
      title={<EuiIcon size="l" type={channelIcon} />}
      isCollapsible={true}
      className="rooms-list"
    >
      <EuiListGroup>
        {loading === "pending"
          ? randomArray(5, 5).map((value) => (
              <LoadingItem key={value} lines={1} type="space" />
            ))
          : rooms.map((room) => (
              <RoomsListItem key={room._id} room={room} onClick={onClick} />
            ))}
      </EuiListGroup>
    </EuiCollapsibleNavGroup>
  );
};

export default RoomsList;
