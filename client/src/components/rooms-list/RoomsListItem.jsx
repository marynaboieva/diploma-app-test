import React, { useCallback } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
// Components
import { EuiListGroupItem, EuiAvatar } from "@elastic/eui";
import RoomStateIndicator from "./RoomStateIndicator";
// Selectors
import { selectById } from "../../redux/slices/rooms/list/selectors";

const RoomsListItem = ({ room, onClick }) => {
  const roomState = useSelector((state) => selectById(state, room._id));

  const { id } = useParams();

  const handleClick = useCallback(() => {
    onClick(room._id);
  }, [onClick, room]);

  return (
    <EuiListGroupItem
      className="room-list-item "
      isActive={room._id === id}
      icon={
        <>
          <EuiAvatar size="l" type="space" name={room.title} />{" "}
          <RoomStateIndicator
            active={roomState?.streaming > 0}
            messages={roomState?.messages}
          />
        </>
      }
      onClick={handleClick}
      label={room.title}
    />
  );
};

export default RoomsListItem;
