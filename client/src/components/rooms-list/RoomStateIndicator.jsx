import React from "react";
// Components
import { EuiBeacon, EuiBadge } from "@elastic/eui";

const RoomStateIndicator = ({ active, messages }) => {
  return (
    <div className="room-state-indicator">
      {active && <EuiBeacon size={10} />}
      {messages > 0 && (
        <EuiBadge className="small-badge" color="danger">
          {messages}
        </EuiBadge>
      )}
    </div>
  );
};

export default RoomStateIndicator;
