import React from "react";
import { EuiButton, EuiButtonEmpty } from "@elastic/eui";
import { useHistory } from "react-router";

export default function ButtonLink({ to, empty, ...props }) {
  const history = useHistory();

  function onClick() {
    history.push(to);
  }

  return empty ? (
    <EuiButtonEmpty onClick={onClick} {...props} />
  ) : (
    <EuiButton onClick={onClick} {...props} />
  );
}
