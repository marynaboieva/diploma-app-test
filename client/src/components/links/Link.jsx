import React from "react";
import { EuiLink } from "@elastic/eui";
import { useHistory } from "react-router";

export default function Link({ to, ...rest }) {
  const history = useHistory();

  function onClick(event) {
    if (event.defaultPrevented) {
      return;
    }
    event.preventDefault();
    history.push(to);
  }
  const href = history.createHref({ pathname: to });

  const props = { ...rest, href, onClick };
  return <EuiLink {...props} />;
}
