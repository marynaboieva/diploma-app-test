import React from "react";
import { useLocation, useHistory } from "react-router-dom";
import { EuiButtonEmpty } from "@elastic/eui";
// Style
import "./style.scss";

const NavButton = ({ to, children }) => {
  const location = useLocation();
  const history = useHistory();

  const onClick = () => {
    history.push(to);
  };

  return (
    <EuiButtonEmpty
      className={`nav-button ${location.pathname === to ? "isActive" : ""}`}
      size="xs"
      onClick={onClick}
    >
      {children}
    </EuiButtonEmpty>
  );
};

export default NavButton;
