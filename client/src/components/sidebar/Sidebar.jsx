import React from "react";
import {
  EuiPageSideBar,
  EuiFlexGroup,
  EuiShowFor,
  EuiHideFor,
  EuiFlyout,
  EuiFlyoutBody,
  EuiSpacer,
} from "@elastic/eui";

const Sidebar = ({ children, isOpen, onClose, side }) => {
  return (
    <>
      <EuiShowFor sizes={["s", "xs"]}>
        {isOpen && (
          <EuiFlyout paddingSize="none" size="s" side={side} onClose={onClose}>
            <EuiFlyoutBody>
              <EuiSpacer />
              <EuiFlexGroup
                className="sidebar-content"
                gutterSize="none"
                direction="column"
              >
                {children}
              </EuiFlexGroup>
            </EuiFlyoutBody>
          </EuiFlyout>
        )}
      </EuiShowFor>
      <EuiHideFor sizes={["xs", "s"]}>
        <EuiPageSideBar
          className={`navigation-sidebar ${isOpen ? "open" : ""}`}
        >
          <EuiFlexGroup
            className="sidebar-content"
            direction="column"
            style={{ height: "100%" }}
          >
            {children}
          </EuiFlexGroup>
        </EuiPageSideBar>
      </EuiHideFor>
    </>
  );
};

export default Sidebar;
