import React from "react";
import { EuiFlexItem, EuiFlexGroup, EuiLoadingContent } from "@elastic/eui";
import AvatarLoading from "./AvatarLoading";

const LoadingItem = ({ lines, size, type = "user" }) => {
  return (
    <EuiFlexGroup>
      <EuiFlexItem grow={false}>
        <AvatarLoading size={size} type={type} />
      </EuiFlexItem>
      <EuiFlexItem>
        <EuiLoadingContent lines={lines} />
      </EuiFlexItem>
    </EuiFlexGroup>
  );
};

export default LoadingItem;
