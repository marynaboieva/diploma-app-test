import React from "react";

// Styles
import "./style.scss";

const AvatarLoading = ({ size = "m", type = "user" }) => {
  return (
    <div className={`avatar-loading  euiAvatar--${size} euiAvatar--${type}`}>
      <span className="euiLoadingContent__singleLineBackground" />
    </div>
  );
};

export default AvatarLoading;
