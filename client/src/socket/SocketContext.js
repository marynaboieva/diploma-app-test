import React, { createContext, useState, useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
// Actions
import {
  addPeer,
  removePeer,
  setActiveSpeaker,
  removeRoom,
  addParticipant,
  removeParticipant,
  newMessage,
  deletedMessage,
  editedMessage,
} from "../redux/slices/rooms/roomsActions";
import { addOnlineUser, removeOnlineUser } from "../redux/actions";
import {
  addRequest,
  removeRequest,
} from "../redux/slices/friends/requests/requestsSlice";
import {
  addFriend,
  removeFriend,
} from "../redux/slices/friends/list/friendsListSlice";
import {
  addInvite,
  removeInvite,
} from "../redux/slices/rooms/invites/invitesSlice";
import { addRoomInfo } from "../redux/slices/rooms/list/roomsSlice";
// Services
import SocketService from "../services/SocketService";
// Selectors
import { selectUser } from "../redux/slices/user/selectors";

export const SocketContext = createContext();

const SocketContextProvider = ({ children }) => {
  const socket = useRef(null);
  const [isSocketConnected, setIsSocketConnected] = useState(false);
  const [error, setError] = useState(null);
  const { token, id } = useSelector(selectUser);

  const dispatch = useDispatch();

  useEffect(() => {
    if (token && id && !socket.current) {
      socket.current = new SocketService();
      socket.current.connect();
      socket.current.on("connect", () => {
        setIsSocketConnected(true);
      });
      socket.current.on("disconnect", () => {
        setIsSocketConnected(false);
      });
      socket.current.on("error", (error) => {
        setError(error);
      });
      // Conference
      socket.current.on("newPeer", ({ user, room_id }) => {
        dispatch(addPeer({ user, room_id }));
      });
      socket.current.on("peerClosed", ({ room_id, user_id }) => {
        dispatch(removePeer({ room_id, user_id }));
      });

      socket.current.on("activeSpeaker", ({ room_id, user_id }) => {
        dispatch(setActiveSpeaker({ room_id, user_id }));
      });
      // Room
      socket.current.on("participantJoined", ({ room_id, user }) => {
        dispatch(addParticipant({ room_id, user }));
      });
      socket.current.on("participantLeft", ({ room_id, user_id }) => {
        if (id === user_id) {
          dispatch(removeRoom(room_id));
          return;
        }
        dispatch(removeParticipant({ room_id, user_id }));
      });
      socket.current.on("roomClosed", ({ room_id }) => {
        dispatch(removeRoom(room_id));
      });
      socket.current.on("newRoom", (room) => {
        dispatch(addRoomInfo(room));
      });
      socket.current.on("canceledInvite", ({ invite_id }) => {
        dispatch(removeInvite(invite_id));
      });
      socket.current.on("declinedInvite", ({ invite_id }) => {
        dispatch(removeInvite(invite_id));
      });
      socket.current.on("appeptedInvite", ({ invite_id }) => {
        dispatch(removeInvite(invite_id));
      });
      socket.current.on("newInvite", (invite) => {
        dispatch(addInvite(invite));
      });
      // Room chat
      socket.current.on("newChatMessage", ({ room_id, message }) => {
        dispatch(newMessage({ room_id, message }));
      });
      socket.current.on("deletedChatMessage", ({ room_id, messageId }) => {
        dispatch(deletedMessage({ room_id, messageId }));
      });
      socket.current.on("editedChatMessage", ({ room_id, message }) => {
        dispatch(editedMessage({ room_id, message }));
      });

      // Common
      socket.current.on("userOnline", ({ room_id, user }) => {
        dispatch(addOnlineUser({ room_id, user }));
      });
      socket.current.on("userOffline", ({ room_id, user_id }) => {
        dispatch(removeOnlineUser({ room_id, user_id }));
      });
      // Friendship
      socket.current.on("canceledFriendRequest", (request_id) => {
        dispatch(removeRequest(request_id));
      });
      socket.current.on("declinedFriendRequest", (request_id) => {
        dispatch(removeRequest(request_id));
      });
      socket.current.on("acceptedFriendRequest", (request_id) => {
        dispatch(removeRequest(request_id));
      });
      socket.current.on("newFriendRequest", (request) => {
        dispatch(addRequest(request));
      });
      socket.current.on("newFriend", (friend) => {
        dispatch(addFriend(friend));
      });
      socket.current.on("removedFriend", (friend) => {
        dispatch(removeFriend(friend));
      });
    }
  }, [token, dispatch, id]);

  useEffect(() => {
    if (error) {
      toast.error(error);
    }
  }, [error]);

  useEffect(() => {
    if (!token && socket.current) {
      return () => {
        socket.current.close();
      };
    }
  }, [token, socket]);

  return (
    <SocketContext.Provider value={{ socket, isSocketConnected }}>
      {children}
    </SocketContext.Provider>
  );
};

export default SocketContextProvider;
