import { useContext } from "react";
import { SocketContext } from "./SocketContext";

const useSocket = () => {
  const { socket, isSocketConnected } = useContext(SocketContext);
  return { socket, isSocketConnected };
};

export default useSocket;
