import axios from "axios";
import { baseURL } from "./config";
import { getToken } from "./TokenService";

const BaseApiService = axios.create({
  baseURL,
});
BaseApiService.interceptors.request.use(
  async (config) => {
    config.headers = {
      Authorization: `Bearer ${getToken()}`,
      "Content-type": "application/json",
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

export default BaseApiService;
