const TOKEN_KEY ="diploma_app_token";

const getToken =()=> localStorage.getItem(TOKEN_KEY);
const setToken =(token)=>localStorage.setItem(TOKEN_KEY, token);

export {getToken, setToken};