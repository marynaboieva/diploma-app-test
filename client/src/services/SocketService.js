import io from "socket.io-client";
import { baseURL } from "./config";
import { getToken } from "./TokenService";

class SocketService {
  constructor() {
    this.socket = null;
  }
  connect(namespace = "") {
    const token = getToken();

    this.socket = io(`${baseURL}/${namespace}`, {
      extraHeaders: { Authorization: `Bearer ${token}` },
    });
    return new Promise((resolve, reject) => {
      this.socket.on("connect", () => resolve());
      this.socket.on("connect_error", (error) => reject(error));
    });
  }
  emit(event, data) {
    return new Promise((resolve, reject) => {
      if (!this.socket) return reject("No socket connection.");
      return this.socket.emit(event, data, (response) => {
        if (response.error) {
          console.error(response.error);
          return reject(response.error);
        }

        return resolve(response);
      });
    });
  }

  on(event, fun) {
    return new Promise((resolve, reject) => {
      if (!this.socket) return reject("No socket connection.");

      this.socket.on(event, fun);
      resolve();
    });
  }
  close() {
    this.socket.close();
  }
}

export default SocketService;
