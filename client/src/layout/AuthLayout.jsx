import React from "react";
// Components
import { EuiPage, EuiPageBody } from "@elastic/eui";
// Styles
import "./style.scss";

const AuthLayout = ({ children }) => {
  return (
    <>
      <EuiPage className="app-content">
        <EuiPageBody component="main">{children}</EuiPageBody>
      </EuiPage>
    </>
  );
};

export default AuthLayout;
