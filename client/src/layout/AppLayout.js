import React, { useState } from "react";
import { useHistory } from "react-router-dom";
// Components
import Header from "../components/header/Header";
import {
  EuiPage,
  EuiPageBody,
  EuiBottomBar,
  EuiIcon,
  EuiListGroup,
  EuiListGroupItem,
  EuiShowFor,
  EuiHideFor,
} from "@elastic/eui";
import RoomsList from "../components/rooms-list/RoomsList";
import InvitesList from "../components/invites-list/InvitesList";
import CreateRoomModal from "../components/create-room/CreateRoomModal";
import Sidebar from "../components/sidebar/Sidebar";
import CurrentUser from "../components/current-user/CurrentUser";
import { Scrollbars } from "react-custom-scrollbars";
import ConferenceWidget from "../pages/room/conference/ConferenceWidget";
// Styles
import "./style.scss";

const AppLayout = ({ children, header }) => {
  const [isOpen, setIsOpen] = useState(false);

  const history = useHistory();

  const toggleMenuOpen = () => {
    setIsOpen(!isOpen);
  };

  const onClose = () => {
    setIsOpen(false);
  };

  const handleGoHome = (e) => {
    e.preventDefault();
    history.push("/friends");
  };

  return (
    <>
      <Header toggleMenuOpen={toggleMenuOpen}>{header}</Header>
      <EuiPage restrictWidth={false} className="app-content">
        <Sidebar side="left" isOpen={isOpen} onClose={onClose}>
          <EuiListGroup>
            <EuiListGroupItem
              icon={
                <EuiIcon
                  type="home"
                  color="success"
                  size="xl"
                  aria-label="go home"
                />
              }
              onClick={handleGoHome}
              label="Home"
            />
            <CreateRoomModal />
          </EuiListGroup>
          <EuiHideFor sizes={["xs", "s"]}>
            <Scrollbars height="calc(100% - 104px)">
              <RoomsList />
              <InvitesList />
            </Scrollbars>
          </EuiHideFor>
          <EuiShowFor sizes={["xs", "s"]}>
            <RoomsList />
            <InvitesList />
          </EuiShowFor>
        </Sidebar>
        <EuiPageBody component="main">{children}</EuiPageBody>
      </EuiPage>
      <EuiBottomBar paddingSize="s">
        <CurrentUser toggleMenuOpen={toggleMenuOpen} />
      </EuiBottomBar>
      <ConferenceWidget />
    </>
  );
};

export default AppLayout;
