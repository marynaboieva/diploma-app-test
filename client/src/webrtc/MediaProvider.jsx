import React, { createContext } from "react";
// Hooks
import useMic from "./hooks/useMic";
import useWebcam from "./hooks/useWebcam";
import useScreen from "./hooks/useScreen";

export const MediaContext = createContext();

const MediaContextProvider = ({ children }) => {
  const webcam = useWebcam();
  const screenshare = useScreen();
  const microphone = useMic();

  return (
    <MediaContext.Provider value={{ webcam, screenshare, microphone }}>
      {children}
    </MediaContext.Provider>
  );
};

export default MediaContextProvider;
