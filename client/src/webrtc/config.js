export const VIDEO_CONSTRAINS = {
  low: {
    width: { ideal: 320 },
  },
  medium: {
    width: { ideal: 640 },
  },
  high: {
    width: { ideal: 1280 },
  },
  veryhigh: {
    width: { ideal: 1920 },
  },
  ultra: {
    width: { ideal: 3840 },
  },
};

export const PC_PROPRIETARY_CONSTRAINTS = {
  optional: [{ googDscp: true }],
};

export const VIDEO_SIMULCAST_ENCODINGS = [
  { rid: "m", maxBitrate: 96000, scaleResolutionDownBy: 4 },
  { rid: "h", maxBitrate: 680000, scaleResolutionDownBy: 1 },
];

// Used for VP9 webcam video.
export const VIDEO_KSVC_ENCODINGS = [{ scalabilityMode: "S3T3_KEY" }];

// Used for VP9 desktop sharing.
export const VIDEO_SVC_ENCODINGS = [{ scalabilityMode: "S3T3", dtx: true }];

export const SCREEN_VIDEO_SIMULCAST_ENCODINGS = [
  {
    rid: "h",
    maxBitrate: 4300 * 1000,
    scaleResolutionDownBy: 1,
  },
];
