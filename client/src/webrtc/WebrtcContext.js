import React, {
  createContext,
  useCallback,
  useState,
  useEffect,
  useRef,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
// Services
import WebrtcConnection from "./WebrtcConnection";
import SocketService from "../services/SocketService";
// Actions
import { addSource, removeSource } from "../redux/slices/rooms/roomsActions";
import {
  setStatus,
  setPeers,
  reset,
} from "../redux/slices/conference/conferenceSlice";
// Selectors
import { selectUser } from "../redux/slices/user/selectors";
import { selectStreamingUsers } from "../redux/slices/rooms/selected/selectors";
import {
  selectConferenceState,
  selectAll as selectConferencePeers,
} from "../redux/slices/conference/selectors";

import useSocket from "../socket/useSocket";

export const WebrtcContext = createContext();

const WebrtcContextProvider = ({ children }) => {
  const { socket } = useSocket();
  const conferenceSocket = useRef(null);
  const [connection, setConnection] = useState(null);
  const [error, setError] = useState(null);

  const conferenceState = useSelector(selectConferenceState);
  const peers = useSelector(selectStreamingUsers);
  const conferencePeers = useSelector(selectConferencePeers);
  const { token } = useSelector(selectUser);

  const dispatch = useDispatch();

  const leaveRoom = useCallback(async () => {
    try {
      await connection.close();
      conferenceSocket.current = null;
      setConnection(null);
      dispatch(reset());
    } catch (error) {
      setError(error);
    }
  }, [connection, dispatch]);

  const createConnection = useCallback(
    async (room_id) => {
      try {
        conferenceSocket.current = new SocketService();
        await conferenceSocket.current.connect(`room-${room_id}`);

        const webrtcConnection = new WebrtcConnection();
        webrtcConnection.init(conferenceSocket.current);
        setConnection(webrtcConnection);

        dispatch(setStatus({ room_id, status: "fulfilled" }));

        webrtcConnection.eventEmitter.addListener(
          "newConsumer",
          ({ producer_user_id, source }) => {
            dispatch(addSource({ user_id: producer_user_id, room_id, source }));
          }
        );
        webrtcConnection.eventEmitter.addListener(
          "consumerClosed",
          ({ producer_user_id, source }) => {
            dispatch(
              removeSource({ user_id: producer_user_id, room_id, source })
            );
          }
        );
        webrtcConnection.eventEmitter.addListener(
          "consumerPaused",
          ({ producer_user_id, source }) => {
            dispatch(
              removeSource({ user_id: producer_user_id, room_id, source })
            );
          }
        );
        webrtcConnection.eventEmitter.addListener(
          "consumerResumed",
          ({ producer_user_id, source }) => {
            dispatch(addSource({ user_id: producer_user_id, room_id, source }));
          }
        );
      } catch (error) {
        setError(error);
      }
    },
    [dispatch]
  );

  const joinRoom = useCallback(
    async (room_id) => {
      try {
        if (socket.current) {
          if (conferenceState.status === "fulfilled") {
            await leaveRoom();
          }
          dispatch(setStatus({ room_id, status: "pending" }));

          const { error } = await socket.current.emit("join", {
            room_id,
          });
          if (error) {
            throw new Error(error);
          }
          createConnection(room_id);
        }
      } catch (error) {
        setError(error);
        dispatch(setStatus({ room_id, status: "rejected" }));
      }
    },
    [dispatch, socket, conferenceState, leaveRoom, createConnection]
  );

  useEffect(() => {
    if (conferenceState.status === "fulfilled" && !conferencePeers.length) {
      dispatch(setPeers(peers));
    }
  }, [conferenceState.status, peers, conferencePeers, dispatch]);

  useEffect(() => {
    if (connection) {
      connection.eventEmitter.addListener("disconnect", (disconnectedFrom) => {
        if (disconnectedFrom === conferenceState.id) {
          leaveRoom();
        }
      });
    }
  }, [connection, conferenceState.id, leaveRoom]);

  useEffect(() => {
    if (error) {
      toast.error(error);
    }
  }, [error]);

  useEffect(() => {
    if (!token && connection) {
      return () => {
        connection.close();
      };
    }
  }, [token, connection]);

  return (
    <WebrtcContext.Provider value={{ connection, joinRoom, leaveRoom }}>
      {children}
    </WebrtcContext.Provider>
  );
};

export default WebrtcContextProvider;
