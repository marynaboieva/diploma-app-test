import * as mediasoupClient from "mediasoup-client";
import { EventEmitter } from "events";
// Consts
import { PC_PROPRIETARY_CONSTRAINTS } from "./config";

export class WebrtcConnection {
  constructor() {
    // Socket.io peer connection
    this._signalingSocket = null;

    // mediasoup-client Device instance.
    this._mediasoupDevice = null;

    // Transport for sending.
    this._sendTransport = null;

    // Transport for receiving.
    this._recvTransport = null;

    // mediasoup Consumers.
    this._consumers = new Map();

    this.eventEmitter = new EventEmitter();
  }

  async init(socketConnection) {
    this._signalingSocket = socketConnection;

    await this._initTransports();
    await this._handleWebrtcEvents();
  }

  async close() {
    await this._sendTransport?.close();
    this._sendTransport = null;
    await this._recvTransport?.close();
    this._recvTransport = null;
    for (const consumer of this._consumers.values()) {
      await consumer.close();
    }
    this.eventEmitter.removeAllListeners();
    this._signalingSocket.socket.removeAllListeners();
    this._signalingSocket?.close();
    await this._consumers.clear();

    return;
  }

  async closeProducer(producer_id) {
    return await this._signalingSocket.emit("closeProducer", {
      producer_id,
    });
  }

  async pauseProducer(producer_id) {
    return await this._signalingSocket.emit("pauseProducer", {
      producer_id,
    });
  }

  async resumeProducer(producer_id) {
    return await this._signalingSocket.emit("resumeProducer", {
      producer_id,
    });
  }

  async _handleWebrtcEvents() {
    this._signalingSocket.on("disconnect", () => {
      this.eventEmitter.emit(
        "disconnect",
        this._signalingSocket.socket.nsp.replace("/room-", "")
      );
    });
    const producers = await this._signalingSocket.emit("getProducers");
    producers.forEach((producer) => {
      this._consumeTransport(
        producer.producer_id,
        producer.producer_user_id,
        producer.source
      );
    });

    this._signalingSocket.on(
      "newConsumer",
      ({ producer_id, producer_user_id, source }) => {
        this._consumeTransport(producer_id, producer_user_id, source);
      }
    );

    this._signalingSocket.on(
      "consumerClosed",
      ({ consumer_id, producer_user_id }) => {
        if (this._consumers.has(consumer_id)) {
          const source = this._consumers.get(consumer_id).appData.source;
          this._consumers.get(consumer_id).close();
          this._consumers.delete(consumer_id);
          this.eventEmitter.emit("consumerClosed", {
            consumer_id,
            producer_user_id,
            source,
          });
        }
      }
    );

    this._signalingSocket.on(
      "consumerPaused",
      ({ consumer_id, producer_user_id }) => {
        if (this._consumers.has(consumer_id)) {
          const source = this._consumers.get(consumer_id).appData.source;
          this._consumers.get(consumer_id).pause();
          this.eventEmitter.emit("consumerPaused", {
            consumer_id,
            producer_user_id,
            source,
          });
        }
      }
    );

    this._signalingSocket.on(
      "consumerResumed",
      ({ consumer_id, producer_user_id }) => {
        if (this._consumers.has(consumer_id)) {
          const source = this._consumers.get(consumer_id).appData.source;

          this._consumers.get(consumer_id).resume();
          this.eventEmitter.emit("consumerResumed", {
            consumer_id,
            producer_user_id,
            source,
          });
        }
      }
    );
  }

  async _consumeTransport(producerId, producer_user_id, source) {
    try {
      const { rtpCapabilities } = this._mediasoupDevice;
      const { id, kind, rtpParameters } = await this._signalingSocket.emit(
        "consume",
        {
          consumerTransportId: this._recvTransport.id,
          producerId,
          rtpCapabilities,
        }
      );

      const consumer = await this._recvTransport.consume({
        id,
        producerId,
        kind,
        rtpParameters,
        appData: { user_id: producer_user_id, source },
      });
      // _store in the map.
      this._consumers.set(consumer.id, consumer);
      this.eventEmitter.emit("newConsumer", {
        producer_user_id,
        source,
      });

      consumer.on("transportclose", () => {
        this._consumers.delete(consumer.id);
      });
    } catch (err) {
      console.log(err);
    }
  }

  async _initTransports() {
    this._mediasoupDevice = new mediasoupClient.Device();

    const routerRtpCapabilities = await this._signalingSocket.emit(
      "getRouterRtpCapabilities"
    );

    routerRtpCapabilities.headerExtensions =
      routerRtpCapabilities.headerExtensions.filter(
        (ext) => ext.uri !== "urn:3gpp:video-orientation"
      );

    await this._mediasoupDevice.load({ routerRtpCapabilities });

    // init transport for sending.

    const sendTransportInfo = await this._signalingSocket.emit(
      "createWebRtcTransport",
      {
        producing: true,
        consuming: false,
      }
    );
    this._sendTransport = this._mediasoupDevice.createSendTransport({
      ...sendTransportInfo,
      proprietaryConstraints: PC_PROPRIETARY_CONSTRAINTS,
    });

    this._sendTransport.on(
      "connect",
      async ({ dtlsParameters }, callback, errback) => {
        try {
          await this._signalingSocket.emit("connectTransport", {
            transport_id: sendTransportInfo.id,
            dtlsParameters,
          });
          callback();
        } catch (error) {
          errback(error);
        }
      }
    );

    this._sendTransport.on(
      "produce",
      async ({ kind, rtpParameters, appData }, callback, errback) => {
        try {
          const { producer_id } = await this._signalingSocket.emit("produce", {
            transportId: this._sendTransport.id,
            kind,
            rtpParameters,
            appData,
          });
          callback({ id: producer_id });
        } catch (error) {
          errback(error);
        }
      }
    );
    // init transport for reciving.

    const reciveTransportInfo = await this._signalingSocket.emit(
      "createWebRtcTransport",
      {
        producing: false,
        consuming: true,
      }
    );

    this._recvTransport = this._mediasoupDevice.createRecvTransport({
      ...reciveTransportInfo,
      proprietaryConstraints: PC_PROPRIETARY_CONSTRAINTS,
    });

    this._recvTransport.on(
      "connect",
      ({ dtlsParameters }, callback, errback) => {
        this._signalingSocket
          .emit("connectTransport", {
            transport_id: reciveTransportInfo.id,
            dtlsParameters,
          })
          .then(callback)
          .catch(errback);
      }
    );
  }
}

export default WebrtcConnection;
