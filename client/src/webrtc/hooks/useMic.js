import { useCallback } from "react";
import useMedia from "./useMedia";

const useMic = () => {
  const getMic = useCallback(async (deviceId) => {
    const stream = await navigator.mediaDevices.getUserMedia({
      audio: true,
      deviceId,
    });

    return stream.getAudioTracks()[0];
  }, []);

  return useMedia({ source: "mic", getMediaTrack: getMic });
};

export default useMic;
