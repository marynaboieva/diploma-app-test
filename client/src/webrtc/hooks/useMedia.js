import { useCallback, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// Action
import { addSource, removeSource } from "../../redux/slices/rooms/roomsActions";
// Hooks
import useWebrtc from "./useWebrtc";
// Selectors
import { selectUser } from "../../redux/slices/user/selectors";
import { selectSelectedRoomState } from "../../redux/slices/rooms/selected/selectors";

const useMedia = ({ source, getMediaTrack, options }) => {
  const { id: user_id } = useSelector(selectUser);
  const { data } = useSelector(selectSelectedRoomState);
  const { connection } = useWebrtc();

  const [isEnabled, setIsEnabled] = useState(false);
  const [isPaused, setIsPaused] = useState(false);
  const deviceId = useRef(null);
  const producerRef = useRef(null);

  const dispatch = useDispatch();

  const notify = useCallback(
    (isAdding = false) => {
      if (data?._id) {
        dispatch(
          isAdding
            ? addSource({ source, room_id: data._id, user_id })
            : removeSource({ source, room_id: data._id, user_id })
        );
      }
    },
    [dispatch, user_id, data?._id, source]
  );

  const closeProducer = useCallback(() => {
    producerRef.current.close();
    producerRef.current = null;
    setIsEnabled(false);
    notify(false);
  }, [producerRef, notify]);

  const disable = useCallback(async () => {
    if (producerRef.current) {
      const producerId = producerRef.current.id;
      closeProducer();
      await connection.closeProducer(producerId);
    }
  }, [producerRef, closeProducer, connection]);

  const enable = useCallback(async () => {
    if (producerRef.current) return;
    if (!connection._sendTransport) {
      throw new Error("Send transport must first be initialized");
    }

    const track = await getMediaTrack(deviceId.current);
    const producer = await connection._sendTransport.produce({
      ...(options ?? {}),
      track,
      appData: { source, user_id },
    });
    producerRef.current = producer;

    if (!deviceId.current) {
      deviceId.current = track.getSettings().deviceId;
    }

    producer.on("transportclose", () => {
      producerRef.current = null;
      setIsEnabled(false);
    });

    producer.on("trackended", () => {
      closeProducer();
    });

    setIsEnabled(true);
    notify(true);
  }, [
    producerRef,
    getMediaTrack,
    deviceId,
    source,
    closeProducer,
    connection,
    options,
    notify,
    user_id,
  ]);

  const pause = useCallback(async () => {
    if (producerRef.current) {
      producerRef.current.pause();
      setIsPaused(true);
      notify(false);

      await connection.pauseProducer(producerRef.current.id);
    }
  }, [producerRef, connection, notify]);

  const resume = useCallback(async () => {
    if (!connection) return;

    if (producerRef.current) {
      producerRef.current.resume();
      setIsPaused(false);

      await connection.resumeProducer(producerRef.current.id);
      notify(true);
    }
  }, [producerRef, connection, notify]);

  const switchDevice = useCallback(
    async (newDeviceId) => {
      if (newDeviceId === deviceId.current) return;
      deviceId.current = newDeviceId;

      if (!producerRef.current) return;

      const producer = producerRef.current;
      const track = await getMediaTrack(deviceId.current);

      await producer.replaceTrack({ track });
    },
    [producerRef, deviceId, getMediaTrack]
  );

  return {
    disable,
    enable,
    pause,
    resume,
    switchDevice,
    isEnabled,
    isPaused,
    deviceId,
    producerId: producerRef?.current?.id,
  };
};

export default useMedia;
