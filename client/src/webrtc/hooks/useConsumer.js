import { useCallback, useEffect, useState } from "react";
import useWebrtc from "./useWebrtc";

export default function useConsumer(consumer_user_id, source) {
  const [consumer, setConsumer] = useState(null);

  const { connection } = useWebrtc();

  const findConsumer = useCallback(
    (consumers) => {
      for (const c of consumers) {
        if (
          c.appData.user_id === consumer_user_id &&
          c.appData.source === source
        ) {
          return c;
        }
      }

      return null;
    },
    [consumer_user_id, source]
  );

  const updateListener = useCallback(
    ({ producer_user_id }) => {
      if (producer_user_id === consumer_user_id) {
        const consumer = findConsumer(connection._consumers.values());
        setConsumer(consumer);
      }
    },
    [connection, findConsumer, consumer_user_id]
  );

  useEffect(() => {
    if (connection) {
      setConsumer(findConsumer(connection._consumers.values()));
      connection.eventEmitter.addListener("newConsumer", updateListener);
      connection.eventEmitter.addListener("consumerPaused", updateListener);
      connection.eventEmitter.addListener("consumerResumed", updateListener);
      connection.eventEmitter.addListener("consumerClosed", updateListener);
      return () => {
        connection.eventEmitter.removeListener("newConsumer", updateListener);
        connection.eventEmitter.removeListener(
          "consumerPaused",
          updateListener
        );
        connection.eventEmitter.removeListener(
          "consumerResumed",
          updateListener
        );
        connection.eventEmitter.removeListener(
          "consumerClosed",
          updateListener
        );
      };
    } else {
      setConsumer(null);
    }
  }, [connection, consumer_user_id, updateListener, findConsumer]);

  return consumer;
}
