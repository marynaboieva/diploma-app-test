import { useContext } from "react";
import { MediaContext } from "../MediaProvider";

const useMediaContext = () => {
  const { webcam, screenshare, microphone } = useContext(MediaContext);
  return { webcam, screenshare, microphone };
};

export default useMediaContext;
