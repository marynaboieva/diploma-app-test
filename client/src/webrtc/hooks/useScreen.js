import { useState } from "react";
import useMedia from "./useMedia";
import { VIDEO_CONSTRAINS, SCREEN_VIDEO_SIMULCAST_ENCODINGS } from "../config";

const useScreen = () => {
  const [stream, setStream] = useState(null);

  const getScreen = async () => {
    const stream = await navigator.mediaDevices.getDisplayMedia({
      video: VIDEO_CONSTRAINS,
    });
    setStream(stream);

    return stream.getVideoTracks()[0];
  };

  return {
    ...useMedia({
      source: "screen",
      getMediaTrack: getScreen,
      options: {
        encodings: SCREEN_VIDEO_SIMULCAST_ENCODINGS,
      },
    }),
    stream,
  };
};

export default useScreen;
