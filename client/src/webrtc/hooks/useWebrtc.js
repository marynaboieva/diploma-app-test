import { useContext } from "react";
import { WebrtcContext } from "../WebrtcContext";

const useWebrtc = () => {
  const { joinRoom, leaveRoom, connection } = useContext(WebrtcContext);
  return { joinRoom, leaveRoom, connection };
};

export default useWebrtc;
