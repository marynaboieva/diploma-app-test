import { useState, useCallback } from "react";
import useMedia from "./useMedia";
import { VIDEO_CONSTRAINS, VIDEO_SIMULCAST_ENCODINGS } from "../config";

const useWebcam = () => {
  const [stream, setStream] = useState(null);

  const getCam = useCallback(async (deviceId) => {
    const stream = await navigator.mediaDevices.getUserMedia({
      video: VIDEO_CONSTRAINS.high,
      frameRate: 25,
      deviceId,
    });
    setStream(stream);

    return stream.getVideoTracks()[0];
  }, []);

  return {
    ...useMedia({
      source: "webcam",
      getMediaTrack: getCam,
      options: {
        encodings: VIDEO_SIMULCAST_ENCODINGS,
      },
    }),
    stream,
  };
};

export default useWebcam;
