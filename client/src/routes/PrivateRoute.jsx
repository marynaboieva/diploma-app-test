import React from "react";
import { useSelector } from "react-redux";
import { Route, Redirect } from "react-router-dom";
import { selectUser } from "../redux/slices/user/selectors";

const PrivateRoute = ({ children, ...rest }) => {
  const { token } = useSelector(selectUser);

  return token ? <Route {...rest}>{children}</Route> : <Redirect to="/login" />;
};

export default PrivateRoute;
