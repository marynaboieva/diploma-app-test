import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
// Actions
import { setRoomsInfo } from "../redux/slices/rooms/list/roomsSlice";
import { setUsersInfo } from "../redux/slices/friends/list/friendsListSlice";
import { getFriends } from "../redux/slices/friends/list/thunks";
import { getRooms } from "../redux/slices/rooms/list/thunks";
import { getMe } from "../redux/slices/user/thunks";
import { getRequests } from "../redux/slices/friends/requests/thunks";
import { getInvites } from "../redux/slices/rooms/invites/thunks";

// Components
import AppLayout from "../layout/AppLayout";
import AuthLayout from "../layout/AuthLayout";
import PrivateRoute from "./PrivateRoute";
import AuthPage from "../pages/auth/AuthPage";
import Home from "../pages/home/Home";
import HomeHeader from "../pages/home/HomeHeader";
import FriendsList from "../pages/home/FriendsList";
import RequestsList from "../pages/home/RequestsList";
import FriendsSearch from "../pages/home/FriendsSearch";
import Room from "../pages/room/Room";
import RoomHeader from "../pages/room/RoomHeader";
import NotFound from "../pages/notfound/NotFound";
// Selectors
import { selectUser } from "../redux/slices/user/selectors";
import { selectFriendsListState } from "../redux/slices/friends/list/selectors";
// Hooks
import useSocket from "../socket/useSocket";

const history = createBrowserHistory();

const Routes = () => {
  const { token } = useSelector(selectUser);
  const { loading } = useSelector(selectFriendsListState);

  const dispatch = useDispatch();

  const { socket, isSocketConnected } = useSocket();

  useEffect(() => {
    if (token) {
      dispatch(getRooms());
      dispatch(getMe());
      dispatch(getFriends());
      dispatch(getRequests());
      dispatch(getInvites());
    }
  }, [token, dispatch]);

  useEffect(() => {
    if (isSocketConnected) {
      socket.current.emit("getRoomsStatus").then((roomsInfo) => {
        dispatch(setRoomsInfo(roomsInfo));
      });
    }
  }, [socket, isSocketConnected, dispatch]);

  useEffect(() => {
    if (isSocketConnected && loading === "fulfilled") {
      socket.current.emit("getFriendStatus").then((friends) => {
        dispatch(setUsersInfo(friends));
      });
    }
  }, [socket, isSocketConnected, loading, dispatch]);

  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/login">
          <AuthLayout>
            <AuthPage />
          </AuthLayout>
        </Route>
        <Route exact path="/signup">
          <AuthLayout>
            <AuthPage />
          </AuthLayout>
        </Route>
        <PrivateRoute exact path="/">
          <Redirect to="/friends" />
        </PrivateRoute>
        <PrivateRoute exact path="/friends">
          <AppLayout header={<HomeHeader />}>
            <Home>
              <FriendsList />
            </Home>
          </AppLayout>
        </PrivateRoute>
        <PrivateRoute exact path="/requests">
          <AppLayout header={<HomeHeader />}>
            <Home>
              <RequestsList />
            </Home>
          </AppLayout>
        </PrivateRoute>
        <PrivateRoute exact path="/search">
          <AppLayout header={<HomeHeader />}>
            <Home>
              <FriendsSearch />
            </Home>
          </AppLayout>
        </PrivateRoute>
        <PrivateRoute exact path="/room/:id">
          <AppLayout header={<RoomHeader />}>
            <Room />
          </AppLayout>
        </PrivateRoute>
        <Route path="*">
          <AuthLayout>
            <NotFound />
          </AuthLayout>
        </Route>
      </Switch>
    </Router>
  );
};

export default Routes;
