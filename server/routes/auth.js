const express = require("express");
const passport = require("passport");
const jwt = require("jsonwebtoken");

const router = express.Router();

router.post("/signup", async (req, res, next) => {
  passport.authenticate(
    "signup",
    { session: false },
    async (err, user, info) => {
      if (info) {
        return res.status(401).send(info);
      }
      if (err || !user) {
        return next(err);
      }
      return res.json({
        message: "Signup successful",
        user,
      });
    }
  )(req, res, next);
});

router.post("/login", async (req, res, next) => {
  passport.authenticate("login", async (err, user, info) => {
    try {
      if (info) {
        return res.status(401).send(info);
      }
      if (err || !user) {
        return next(err);
      }

      req.login(user, { session: false }, async (error) => {
        if (error) return next(error);

        const body = {
          user_id: user.id,
          email: user.email,
          username: user.username,
        };
        const token = jwt.sign({ user: body }, process.env.JWT_SECRET_KEY);

        return res.json({ token, user: body });
      });
    } catch (error) {
      return next(error);
    }
  })(req, res, next);
});

module.exports = router;
