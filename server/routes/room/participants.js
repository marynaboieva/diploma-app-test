const express = require("express");
const RoomService = require("../../services/RoomService");
const UserService = require("../../services/UserService");
const router = express.Router({ mergeParams: true });

const { getRoom, checkIsAdmin } = require("../../middlewares/room");

router.delete("/:participantId", getRoom, checkIsAdmin, async (req, res) => {
  try {
    const { roomId, participantId } = req.params;
    const room = req.room;

    await RoomService.removeParticipant(roomId, participantId);

    res.status(204).send();
  } catch (error) {
    res.status(500).send(error);
  }
});

router.get("/search", getRoom, async (req, res) => {
  try {
    const { page, limit, username } = req.query;
    const { roomId } = req.params;

    const users = await UserService.searchParticipants({
      searchByUsername: username,
      page,
      limit: Number(limit),
      user_id: req.user.user_id,
      room_id: roomId,
    });
    res.status(200).send(users);
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = router;
