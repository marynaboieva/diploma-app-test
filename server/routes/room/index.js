const express = require("express");
const router = express.Router();
// Services
const RoomService = require("../../services/RoomService");
// Middlewares
const { getRoom, checkIsAdmin } = require("../../middlewares/room");
// Routes
const participantsRouter = require("./participants");
const chatRouter = require("./chat");

router.post("/", async (req, res) => {
  try {
    const { user_id } = req.user;
    const { title } = req.body;
    const room = await RoomService.createRoom({
      title,
      created_by: user_id,
    });
    res.status(201).send(room);
  } catch (error) {
    res.status(500).send("And error ocurred while creating the room");
  }
});

router.get("/", async (req, res) => {
  try {
    const { user_id } = req.user;
    const rooms = await RoomService.findByUser(user_id);
    if (!rooms) {
      return res.status(404).send("No rooms found");
    }

    res.status(200).send(rooms);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.get("/:roomId", getRoom, async (req, res) => {
  try {
    res.status(200).send(req.room);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

router.delete("/:roomId/", getRoom, checkIsAdmin, async (req, res) => {
  try {
    const { roomId } = req.params;

    await RoomService.deleteRoom(roomId);

    res.status(204).send();
  } catch (error) {
    res.status(500).send("And error ocurred while deleting the room");
  }
});

router.put("/:roomId/leave", getRoom, async (req, res) => {
  try {
    const { user_id } = req.user;
    const { roomId } = req.params;

    await RoomService.removeParticipant(roomId, user_id);

    res.status(204).send();
  } catch (error) {
    res.status(500).send(error);
  }
});

router.use("/:roomId/participants", participantsRouter);
router.use("/:roomId/chat", chatRouter);

module.exports = router;
