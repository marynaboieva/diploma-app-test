const express = require("express");
const RoomService = require("../../services/RoomService");
const router = express.Router({ mergeParams: true });

const { getRoom } = require("../../middlewares/room");

router.get("/", getRoom, async (req, res) => {
  try {
    const { user_id } = req.user;

    const { page, limit } = req.query;
    const { roomId } = req.params;

    const messages = await RoomService.getChatMessages(roomId, {
      page,
      limit: Number(limit),
      user_id,
    });
    res.status(200).send(messages);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

router.post("/", getRoom, async (req, res) => {
  try {
    const { user_id } = req.user;
    const { body } = req.body;
    const { roomId } = req.params;

    const messages = await RoomService.sendChatMessage(roomId, {
      body,
      author: user_id,
    });
    res.status(200).send(messages);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.put("/:messageId", getRoom, async (req, res) => {
  try {
    const { user_id } = req.user;
    const { body } = req.body;
    const { roomId, messageId } = req.params;

    const message = await RoomService.getChatMessageById(roomId, messageId);

    if (message?.author?.toString() !== user_id) {
      return res.status(403).send("You do not have permission for this action");
    }

    const newMessage = await RoomService.editChatMessage(
      roomId,
      messageId,
      body
    );
    res.status(200).send(newMessage);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.delete("/:messageId", getRoom, async (req, res) => {
  try {
    const { user_id } = req.user;
    const { roomId, messageId } = req.params;

    const message = await RoomService.getChatMessageById(roomId, messageId);

    if (message?.author?.toString() !== user_id) {
      return res.status(403).send("You do not have permission for this action");
    }

    await RoomService.removeChatMessage(roomId, messageId);
    res.status(204).send();
  } catch (error) {
    res.status(500).send(error);
  }
});

router.patch("/:messageId/read", getRoom, async (req, res) => {
  try {
    const { user_id } = req.user;
    const { roomId, messageId } = req.params;

    const newMessage = await RoomService.markChatMessageAsRead(
      roomId,
      messageId,
      user_id
    );
    res.status(200).send(newMessage);
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = router;
