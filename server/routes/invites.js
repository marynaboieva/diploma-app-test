const express = require("express");
const RoomService = require("../services/RoomService");
const RoomInviteService = require("../services/RoomInviteService");

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const { user_id } = req.user;
    const requests = await RoomInviteService.findByUser(user_id);

    res.status(200).send(requests);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.post("/", async (req, res) => {
  try {
    const { user_id } = req.user;
    const { receiver_id, room_id } = req.body;

    const room = await RoomService.findById(room_id);
    if (room.created_by._id.toString() !== user_id) {
      return res.status(403).send("You do not have permission for this action");
    }

    if (room.participants.find((user) => user._id.toString() === receiver_id)) {
      return res.status(400).send("User already joined this room");
    }
    const invite = await RoomInviteService.create(
      receiver_id,
      {
        _id: room.created_by._id,
        username: room.created_by.username,
      },
      room_id
    );
    res.status(200).send(invite);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.delete("/:inviteId", async (req, res) => {
  try {
    const { user_id } = req.user;
    const { inviteId } = req.params;

    const invite = await RoomInviteService.findById(inviteId);
    if (!inviteId) {
      return res.status(404).send("Invite not found");
    }
    if (invite.sender.toString() !== user_id) {
      return res.status(403).send("You do not have permission for this action");
    }

    if (invite.status !== "pending") {
      return res
        .status(409)
        .send(
          "You can not cancel this invite, because its status is already changed"
        );
    }
    await RoomInviteService.cancelInvite(inviteId);

    res.status(204).send();
  } catch (error) {
    res.status(500).send(error);
  }
});

router.patch("/:inviteId/accept", async (req, res) => {
  try {
    const { user_id } = req.user;
    const { inviteId } = req.params;

    const invite = await RoomInviteService.findById(inviteId);
    if (invite?.receiver.toString() !== user_id) {
      return res.status(403).send("You do not have permission for this action");
    }

    if (invite.status !== "pending") {
      return res.status(409).send("Invite is already used");
    }

    await RoomInviteService.acceptInvite(inviteId);
    await RoomService.addParticipant(invite.room, user_id);
    const room = await RoomService.findById(invite.room);
    res.status(200).send({
      _id: invite.room,
      title: room.title,
      participants: room.participants.length + 1,
    });
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

router.patch("/:inviteId/decline", async (req, res) => {
  try {
    const { user_id } = req.user;
    const { inviteId } = req.params;

    const invite = await RoomInviteService.findById(inviteId);
    if (invite?.receiver.toString() !== user_id) {
      return res.status(403).send("You do not have permission for this action");
    }

    if (invite.status !== "pending") {
      return res.status(409).send("Invite is already used");
    }

    await RoomInviteService.declineInvite(inviteId);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});
module.exports = router;
