const express = require("express");
const UserService = require("../services/UserService");
const FriendshipRequestService = require("../services/FriendshipRequestService");
const router = express.Router();


router.get("/", async (req, res) => {
  try {
    const { user_id } = req.user;
    const requests = await FriendshipRequestService.findByUser(user_id);

    res.status(200).send(requests);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.patch("/:requestId/accept", async (req, res) => {
  try {
    const { user_id } = req.user;
    const { requestId } = req.params;

    const request = await FriendshipRequestService.findById(requestId);

    if (!request) {
      return res.status(404).send("Request not found");
    }

    if (request.receiver.toString() !== user_id) {
      return res.status(403).send("You do not have permission for this action");
    }

    await FriendshipRequestService.acceptRequest(requestId);
    await UserService.addFriend(user_id, request.sender);
    await UserService.addFriend(request.sender, user_id);

    res.status(204).send();
  } catch (error) {
    res.status(500).send(error);
  }
});

router.patch("/:requestId/decline", async (req, res) => {
  try {
    const { user_id } = req.user;
    const { requestId } = req.params;
    const request = await FriendshipRequestService.findById(requestId);
    if (!request) {
      return res.status(404).send("Request not found");
    }
    if (request.receiver.toString() !== user_id) {
      return res.status(403).send("You do not have permission for this action");
    }

    await FriendshipRequestService.declineRequest(requestId);
    res.status(204).send();
  } catch (error) {

    res.status(500).send(error);
  }
});

router.post("/", async (req, res) => {
  try {
    const { user_id } = req.user;
    const { id } = req.body;

    if (id === user_id) {
      return res.status(400).send("Unable to create a request");
    }

    const friends = await UserService.findUsersFriendById(user_id, id);

    if (friends?.includes(id)) {
      return res.status(409).send("This user is already a friend of yours");
    }

    const request = await FriendshipRequestService.create(user_id, id);
    res.status(200).send(request);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

router.delete("/:requestId", async (req, res) => {
  try {
    const { user_id } = req.user;
    const { requestId } = req.params;

    const request = await FriendshipRequestService.findById(requestId);
    if (!request) {
      return res.status(404).send("Request not found");
    }

    if (request.sender.toString() !== user_id) {
      return res.status(403).send("You do not have permission for this action");
    }

    await FriendshipRequestService.cancelRequest(requestId);
    res.status(204).send();
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = router;
