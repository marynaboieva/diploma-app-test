const express = require("express");
const UserService = require("../services/UserService");
const router = express.Router();

router.get("/me", async (req, res) => {
  try {
    const { user_id } = req.user;
    const user = await UserService.findById(user_id);

    res.status(200).send(user);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.get("/me/friends/search", async (req, res) => {
  try {
    const { page, limit, username } = req.query;

    const users = await UserService.searchFriends({
      searchByUsername: username,
      page,
      limit: Number(limit),
      user_id: req.user.user_id,
    });
    res.status(200).send(users);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.delete("/me/friends/:friendId", async (req, res) => {
  try {
    const { user_id } = req.user;
    const { friendId } = req.params;

    await UserService.removeFriend(user_id, friendId);
    await UserService.removeFriend(friendId, user_id);

    res.status(204).send();
  } catch (error) {
    res.status(500).send(error);
  }
});

router.get("/me/friends/", async (req, res) => {
  try {
    const { user_id } = req.user;
    const friends = await UserService.findUsersFriends(user_id);

    res.status(200).send(friends);
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = router;
