const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const config = require("./config");

require("./db");

const server = require("http").createServer(app);

const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});

const { createWorkers } = require("./mediasoup-workers");

const passport = require("./middlewares/auth");
const authRoutes = require("./routes/auth");
const roomRoutes = require("./routes/room");
const userRoutes = require("./routes/user");
const requestsRoutes = require("./routes/requests");
const invitesRoutes = require("./routes/invites");

require("./socket/index")(io);
require("./socket/conference-controller")(io);

server.listen(config.listenPort, () => {
  console.log("listening http " + config.listenPort);
});

(async () => {
  await createWorkers();
})();

app.use(bodyParser.json());
app.use(cors());
app.use("/", authRoutes);

app.use("/rooms", passport.authenticate("jwt", { session: false }), roomRoutes);
app.use("/users", passport.authenticate("jwt", { session: false }), userRoutes);
app.use("/requests", passport.authenticate("jwt", { session: false }), requestsRoutes);
app.use("/invites", passport.authenticate("jwt", { session: false }), invitesRoutes);
