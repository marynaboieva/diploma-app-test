const passportJwtSocketIo = require("passport-jwt.socketio");
const ExtractJWT = require("passport-jwt").ExtractJwt;

const socketAuthMiddleware = passportJwtSocketIo.authorize(
  {
    secretOrKey: process.env.JWT_SECRET_KEY,
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  },
  async (socket, done) => {
    try {
      return done(null, socket.user);
    } catch (error) {
      done(error);
    }
  }
);

module.exports = { socketAuthMiddleware };
