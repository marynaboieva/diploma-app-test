const passport = require("passport");
const localStrategy = require("passport-local").Strategy;
const UserService = require("../services/UserService");

const JWTstrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;

passport.use(
  "login",
  new localStrategy(
    {
      usernameField: "email",
      passwordField: "password",
    },
    async (email, password, done) => {
      try {
        const user = await UserService.find({ email });
        if (!user) {
          return done(null, null, "User not found");
        }

        const validate = await user.checkPassword(password);

        if (!validate) {
          return done(null, null, "Wrong Password");
        }

        return done(null, user);
      } catch (error) {
        return done(error);
      }
    }
  )
);

passport.use(
  "signup",
  new localStrategy(
    {
      usernameField: "email",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        const { username } = req.body;
        const user = await UserService.find({ email, username });
        if (!user) {
          const newUser = await UserService.create({
            email,
            username,
            password,
          });
          return done(null, newUser);
        }
        return done(null, null, "User already exists");
      } catch (error) {
        return done(error);
      }
    }
  )
);

passport.use(
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_SECRET_KEY,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        return done(null, token.user);
      } catch (error) {
        done(error);
      }
    }
  )
);

module.exports = passport;
