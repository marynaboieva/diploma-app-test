const RoomService = require("../services/RoomService");
const UserService = require("../services/UserService");

const getRoom = async (req, res, next) => {
  try {
    const { user_id } = req.user;

    const { roomId } = req.params;
    const room = await RoomService.findById(roomId);
    if (
      !room ||
      !room?.participants?.find((user) => user._id.toString() === user_id)
    ) {
      return res.status(404).send("Room not found");
    }
    req.room = room;
    next();
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
};

const checkIsAdmin = async (req, res, next) => {
  try {
    const { user_id } = req.user;
    const room = req.room;
    if (room.created_by._id.toString() !== user_id) {
      return res.status(403).send("You do not have permission for this action");
    }
    next();
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
};

module.exports = { getRoom, checkIsAdmin };
