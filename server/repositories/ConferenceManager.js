const { EventEmitter } = require("events");
const { getMediasoupWorker } = require("../mediasoup-workers");
const Room = require("./Room");
const Peer = require("./Peer");

class ConferenceManager extends EventEmitter {
  constructor() {
    super();
    this.roomList = new Map();
  }
  getById = (id) => {
    return this.roomList.get(id);
  };

  createRoom = async (id) => {
    let worker = await getMediasoupWorker();
    const room = await Room.create(id, worker);
    this.roomList.set(id, room);

    [
      "newConsumer",
      "consumerClosed",
      "consumerPaused",
      "consumerResumed",
      "peerClosed",
      "activeSpeaker",
    ].forEach((event) => {
      room.on(event, (args) => {
        this.emit(event, { room_id: id, ...args });
      });
    });
  };

  closeRoom = async (id) => {
    this.getById(id)?.close();
    this.roomList.delete(id);
  };

  getPeer = async (id, user_id) => this.getById(id)?.getPeer(user_id);

  getPeers = async (id) => this.getById(id)?.getPeers();

  addPeer = (room_id, user_id, { username }) => {
    this.getById(room_id)?.addPeer(new Peer(user_id, username));
    this.emit("newPeer", { room_id, peer: { user_id, username } });
  };

  removePeer = (room_id, user_id) => {
    this.getById(room_id)?.removePeer(user_id);
  };

  getProducersList = (room_id, user_id) => {
    return this.getById(room_id)?.getProducerListForPeer(user_id);
  };

  getRtpCapabilities = (room_id) => {
    return this.getById(room_id)?.getRtpCapabilities();
  };

  createWebRtcTransport = async (room_id, user_id) => {
    const { params } = await this.getById(room_id)?.createWebRtcTransport(
      user_id
    );
    return params;
  };

  connectTransport = async (
    room_id,
    user_id,
    { transport_id, dtlsParameters }
  ) => {
    await this.getById(room_id)?.connectPeerTransport(
      user_id,
      transport_id,
      dtlsParameters
    );
  };

  restartIce = async (room_id, user_id, { transport_id }) => {
    return await this.getById(room_id)?.restartIce(user_id, transport_id);
  };

  produce = async (
    room_id,
    user_id,
    { transportId, rtpParameters, kind, appData }
  ) => {
    return await this.getById(room_id)?.produce(
      user_id,
      transportId,
      rtpParameters,
      kind,
      appData
    );
  };

  consume = async (
    room_id,
    user_id,
    { consumerTransportId, producerId, rtpCapabilities }
  ) => {
    return await this.getById(room_id)?.consume(
      user_id,
      consumerTransportId,
      producerId,
      rtpCapabilities
    );
  };

  closeProducer = (room_id, user_id, { producer_id }) => {
    this.getById(room_id)?.closeProducer(user_id, producer_id);
  };

  pauseProducer = (room_id, user_id, { producer_id }) => {
    this.getById(room_id)?.pauseProducer(user_id, producer_id);
  };

  resumeProducer = (room_id, user_id, { producer_id }) => {
    this.getById(room_id)?.resumeProducer(user_id, producer_id);
  };

  pauseConsumer = (room_id, user_id, { consumer_id }) => {
    this.getById(room_id)?.pauseConsumer(user_id, consumer_id);
  };

  resumeConsumer = (room_id, user_id, { consumer_id }) => {
    this.getById(room_id)?.resumeConsumer(user_id, consumer_id);
  };
}

const conferenceManager = new ConferenceManager();
module.exports = conferenceManager;
