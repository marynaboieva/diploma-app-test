module.exports = class Peer {
  constructor(user_id, username) {
    this.user_id = user_id;
    this.username = username;
    this.transports = new Map();
    this.consumers = new Map();
    this.producers = new Map();
  }

  addTransport(transport) {
    this.transports.set(transport.id, transport);
  }

  async connectTransport(transport_id, dtlsParameters) {
    if (!this.transports.has(transport_id)) return;
    await this.transports.get(transport_id).connect({
      dtlsParameters,
    });
  }

  async createProducer(producerTransportId, rtpParameters, kind, appData) {
    let producer = await this.transports.get(producerTransportId).produce({
      kind,
      rtpParameters,
      appData: { ...appData, user_id: this.user_id },
    });

    this.producers.set(producer.id, producer);

    producer.on("transportclose", () => {
      console.log(
        `producer transport close username: ${this.username} consumer_id: ${producer.id}`
      );
      producer.close();
      this.producers.delete(producer.id);
    });

    return producer;
  }

  async createConsumer(consumer_transport_id, producer_id, rtpCapabilities) {
    let consumerTransport = this.transports.get(consumer_transport_id);

    let consumer = null;
    try {
      consumer = await consumerTransport.consume({
        producerId: producer_id,
        rtpCapabilities,
        paused: false,
      });
    } catch (error) {
      console.error("consume failed", error);
      return;
    }
    if (consumer.type === "simulcast") {
      await consumer.setPreferredLayers({
        spatialLayer: 2,
        temporalLayer: 2,
      });
    }

    this.consumers.set(consumer.id, consumer);

    consumer.on("transportclose", () => {
      console.log(
        `consumer transport close username: ${this.username} consumer_id: ${consumer.id}`
      );
      this.consumers.delete(consumer.id);
    });

    return {
      consumer,
      params: {
        producerId: producer_id,
        id: consumer.id,
        kind: consumer.kind,
        rtpParameters: consumer.rtpParameters,
        type: consumer.type,
        producerPaused: consumer.producerPaused,
      },
    };
  }

  closeProducer(producer_id) {
    try {
      this.producers.get(producer_id).close();
    } catch (e) {
      console.warn(e);
    }

    this.producers.delete(producer_id);
  }

  async pauseProducer(producer_id) {
    try {
      await this.producers.get(producer_id).pause();
    } catch (e) {
      console.warn(e);
    }
  }

  async resumeProducer(producer_id) {
    try {
      await this.producers.get(producer_id).resume();
    } catch (e) {
      console.warn(e);
    }
  }

  async pauseConsumer(consumer_id) {
    try {
      await this.consumers.get(consumer_id).pause();
    } catch (e) {
      console.warn(e);
    }
  }

  async resumeConsumer(consumer_id) {
    try {
      await this.consumers.get(consumer_id).resume();
    } catch (e) {
      console.warn(e);
    }
  }

  getProducer(producer_id) {
    return this.producers.get(producer_id);
  }

  close() {
    this.transports.forEach((transport) => transport.close());
  }

  removeConsumer(consumer_id) {
    this.consumers.delete(consumer_id);
  }
};
