const { EventEmitter } = require("events");
const config = require("../config");

module.exports = class Room extends EventEmitter {
  static async create(room_id, worker) {
    // Router media codecs.
    const mediaCodecs = config.mediasoup.router.mediaCodecs;
    // Create a mediasoup Router.
    const router = await worker.createRouter({ mediaCodecs });
    // Create a mediasoup AudioLevelObserver.
    const audioLevelObserver = await router.createAudioLevelObserver({
      maxEntries: 1,
      threshold: -80,
      interval: 800,
    });
    return new Room({
      room_id,
      router,
      audioLevelObserver,
    });
  }
  constructor({ room_id, router, audioLevelObserver }) {
    super();
    // Room id.
    this.room_id = room_id;
    // Map of broadcasters indexed by id
    this.peers = new Map();
    // mediasoup Router instance.
    this.router = router;
    // mediasoup AudioLevelObserver.
    this._audioLevelObserver = audioLevelObserver;
    // Handle audioLevelObserver.
    this._handleAudioLevelObserver();
  }

  getPeer(user_id) {
    return this.peers.get(user_id);
  }

  async addPeer(peer) {
    this.peers.set(peer.user_id, peer);
  }

  getProducerListForPeer(user_id) {
    let producerList = [];
    this.peers.forEach((peer) => {
      if (peer.user_id !== user_id) {
        peer.producers.forEach((producer) => {
          producerList.push({
            producer_id: producer.id,
            producer_user_id: producer.appData.user_id,
            source: producer.appData.source,
          });
        });
      }
    });
    return producerList;
  }

  getRtpCapabilities() {
    return this.router.rtpCapabilities;
  }

  async createWebRtcTransport(user_id) {
    const { maxIncomingBitrate, initialAvailableOutgoingBitrate } =
      config.mediasoup.webRtcTransport;

    const transport = await this.router.createWebRtcTransport({
      listenIps: config.mediasoup.webRtcTransport.listenIps,
      enableUdp: true,
      enableTcp: true,
      preferUdp: true,
      initialAvailableOutgoingBitrate,
    });
    if (maxIncomingBitrate) {
      try {
        await transport.setMaxIncomingBitrate(maxIncomingBitrate);
      } catch (error) {}
    }

    transport.on("dtlsstatechange", (dtlsState) => {
      if (dtlsState === "closed") {
        console.log(
          `transport close
              ${this.peers.get(user_id).username}
              closed`
        );
        transport.close();
      }
    });

    transport.on("close", () => {
      console.log(`transport close ${this.peers.get(user_id).username} closed`);
    });
    console.log("adding transport", transport.id);
    this.peers.get(user_id).addTransport(transport);
    return {
      params: {
        id: transport.id,
        iceParameters: transport.iceParameters,
        iceCandidates: transport.iceCandidates,
        dtlsParameters: transport.dtlsParameters,
      },
    };
  }

  async restartIce(user_id, transport_id) {
    return await this.peers
      .get(user_id)
      .transports.get(transport_id)
      .restartIce();
  }

  async connectPeerTransport(user_id, transport_id, dtlsParameters) {
    if (!this.peers.has(user_id)) return;
    await this.peers
      .get(user_id)
      .connectTransport(transport_id, dtlsParameters);
  }

  async produce(user_id, producerTransportId, rtpParameters, kind, appData) {
    const peer = this.peers.get(user_id);
    let producer = await peer.createProducer(
      producerTransportId,
      rtpParameters,
      kind,
      appData
    );
    this.emit("newConsumer", {
      producer_id: producer.id,
      producer_user_id: user_id,
      source: appData.source,
    });
    return producer.id;
  }

  async consume(user_id, consumer_transport_id, producer_id, rtpCapabilities) {
    if (
      !this.router.canConsume({
        producerId: producer_id,
        rtpCapabilities,
      })
    ) {
      throw new Error("can not consume");
    }

    const peer = this.peers.get(user_id);

    let { consumer, params } = await peer.createConsumer(
      consumer_transport_id,
      producer_id,
      rtpCapabilities
    );
    let producer_user_id;
    this.peers.forEach((peer) => {
      if (peer.producers.has(producer_id)) {
        producer_user_id = peer.user_id;
      }
    });
    consumer.on("producerclose", () => {
      console.log(
        `consumer closed due to producerclose event  username:${peer.username} consumer_id: ${consumer.id}`
      );
      peer.removeConsumer(consumer.id);

      // tell client consumer is dead

      this.emit("consumerClosed", {
        consumer_id: consumer.id,
        user_id,
        producer_user_id,
      });
    });

    consumer.on("producerpause", () => {
      console.log(
        `consumer paused due to producerpause event  username:${peer.username} consumer_id: ${consumer.id}`
      );

      this.emit("consumerPaused", {
        consumer_id: consumer.id,
        user_id,
        producer_user_id,
      });
    });

    consumer.on("producerresume", () => {
      console.log(
        `consumer resumed due to producerresume event  username:${peer.username} consumer_id: ${consumer.id}`
      );

      this.emit("consumerResumed", {
        consumer_id: consumer.id,
        user_id,
        producer_user_id,
      });
    });

    return { params, producer_user_id };
  }

  async removePeer(user_id) {
    const peer = this.peers.get(user_id);

    this.emit("peerClosed", { user_id });
    if (peer) {
      peer.close();
    }
    this.peers.delete(user_id);
  }

  closeProducer(user_id, producer_id) {
    const peer = this.peers.get(user_id);
    if (peer) peer.closeProducer(producer_id);
  }

  pauseProducer(user_id, producer_id) {
    this.peers.get(user_id).pauseProducer(producer_id);
  }

  resumeProducer(user_id, producer_id) {
    this.peers.get(user_id).resumeProducer(producer_id);
  }

  pauseConsumer(user_id, consumer_id) {
    this.peers.get(user_id).pauseConsumer(consumer_id);
  }

  resumeConsumer(user_id, consumer_id) {
    this.peers.get(user_id).resumeConsumer(consumer_id);
  }

  getPeers() {
    const peersInfo = [];
    for (let peer of this.peers.values()) {
      peersInfo.push({ user_id: peer.user_id, username: peer.username });
    }
    return peersInfo;
  }

  close() {
    if (this.worker) {
      this.worker.close();
    }
    this.peers.forEach((peer) => peer.close());
  }

  _handleAudioLevelObserver() {
    this._audioLevelObserver.on("volumes", (volumes) => {
      const { producer, volume } = volumes[0];

      this.emit("activeSpeaker", {
        user_id: producer.appData.producer_user_id,
        volume: volume,
      });
    });

    this._audioLevelObserver.on("silence", () => {
      // Notify all Peers.
      this.emit("activeSpeaker", { user_id: null });
    });
  }
};
