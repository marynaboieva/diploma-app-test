const { EventEmitter } = require("events");

const ChatModel = require("../models/Chat");
const MessageModel = require("../models/Message");

const mongoose = require("mongoose");
const UserService = require("./UserService");

class ChatService extends EventEmitter {
  constructor() {
    super();
  }
  findById = async (_id) => {
    return await ChatModel.findById(_id);
  };

  getMessages = async (_id, { page = 0, limit = 10, user_id, date }) => {
    const [result = { messages: [], total: 0 }] = await ChatModel.aggregate([
      {
        $match: {
          _id: mongoose.Types.ObjectId(_id),
        },
      },
      {
        $lookup: {
          from: "messages",
          let: { messages: "$messages" },
          pipeline: [
            { $match: { $expr: { $in: ["$_id", "$$messages"] } } },
            {
              $lookup: {
                from: "users",
                let: { author: "$author" },
                pipeline: [
                  { $match: { $expr: { $eq: ["$_id", "$$author"] } } },
                  {
                    $project: { password: false, email: false, friends: false },
                  },
                ],
                as: "author",
              },
            },

            {
              $project: {
                body: 1,
                read: {
                  $or: [
                    { $eq: ["$author._id", mongoose.Types.ObjectId(user_id)] },
                    { $in: [mongoose.Types.ObjectId(user_id), "$read"] },
                  ],
                },
                author: { $arrayElemAt: ["$author", 0] },
                created: 1,
                edited: 1,
              },
            },
          ],
          as: "messages",
        },
      },
      {
        $sort: {
          "messages.created": 1,
        },
      },
      {
        $project: {
          _id: 0,
          messages: {
            $slice: ["$messages", page * limit, limit],
          },
          total: { $size: "$messages" },
        },
      },
    ]);
    return result;
  };
  getMessageById = async (id) => {
    return await MessageModel.findById(id);
  };
  createChat = async () => {
    return await new ChatModel().save();
  };

  addMessage = async (_id, { author, body }) => {
    let message = await new MessageModel({ author, body }).save();
    await ChatModel.updateOne(
      { _id },
      {
        $addToSet: { messages: [message] },
      }
    ).exec();
    message = await message.populate("author", "username").execPopulate();
    return message;
  };

  editMessage = async (_id, messageId, body) => {
    return await MessageModel.findOneAndUpdate(
      { _id: messageId },
      { body, edited: true },
      { new: true, useFindAndModify: false }
    );
  };

  markAsRead = async (_id, messageId, userId) => {
    return await MessageModel.findOneAndUpdate(
      { _id: messageId },
      { $addToSet: { read: userId } },
      { new: true, useFindAndModify: false }
    );
  };

  removeMessage = async (_id, messageId) => {
    return await ChatModel.updateOne(
      { _id },
      { $pull: { messages: messageId } }
    ).exec();
  };
}

const chatService = new ChatService();
module.exports = chatService;
