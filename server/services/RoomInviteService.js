const { EventEmitter } = require("events");
const RoomInviteModel = require("../models/RoomInvite");
const mongoose = require("mongoose");

class RoomInviteService extends EventEmitter {
  constructor() {
    super();
  }
  create = async (receiver, sender, room) => {
    const invite = await RoomInviteModel.findOneAndUpdate(
      {
        sender: sender._id,
        receiver,
        room,
      },
      {
        sender: sender._id,
        receiver,
        room,
        status: "pending",
        created: new Date(),
      },
      {
        upsert: true,
        setDefaultsOnInsert: true,
        useFindAndModify: false,
        new: true,
      }
    )
      .populate("sender", "id username")
      .populate("receiver", "id username")
      .populate("room", "id title")
      .exec();
    this.emit("newInvite", {
      invite,
      receiver_id: invite.receiver._id.toString(),
    });
    return invite;
  };

  findById = async (_id) => {
    return await RoomInviteModel.findById(_id).exec();
  };

  findByUser = async (user_id) => {
    return await RoomInviteModel.find({
      status: "pending",
      $or: [
        { sender: mongoose.Types.ObjectId(user_id) },
        { receiver: mongoose.Types.ObjectId(user_id) },
      ],
    })
      .populate("sender", "id username")
      .populate("receiver", "id username")
      .populate("room", "id title")
      .select({
        sender: 1,
        receiver: 1,
        room: 1,
        status: 1,
        created: 1,
      })
      .exec();
  };

  cancelInvite = async (_id) => {
    const invite = await RoomInviteModel.findOneAndDelete({ _id });

    this.emit("canceledInvite", {
      invite_id: invite._id,
      receiver: invite.receiver.toString(),
    });

    return invite;
  };

  acceptInvite = async (_id) => {
    const invite = await RoomInviteModel.findByIdAndUpdate(
      { _id },
      { status: "accepted" }
    );

    this.emit("appeptedInvite", {
      invite_id: invite._id,
      sender: invite.sender.toString(),
    });

    return invite;
  };

  declineInvite = async (_id) => {
    const invite = await RoomInviteModel.findByIdAndUpdate(
      { _id },
      { status: "declined" }
    );

    this.emit("declinedInvite", {
      invite_id: invite._id,
      sender: invite.sender.toString(),
    });

    return invite;
  };
}
const roomInviteService = new RoomInviteService();
module.exports = roomInviteService;
