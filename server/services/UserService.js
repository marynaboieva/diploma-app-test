const { EventEmitter } = require("events");

const mongoose = require("mongoose");
const UserModel = require("../models/User");
const RoomModel = require("../models/Room");

class UserService extends EventEmitter {
  constructor() {
    super();
  }

  find = async ({ email = "", username = "" }) =>
    await UserModel.findOne({
      $or: [{ email }, { username }],
    }).exec();

  create = async ({ email, username, password }) => {
    const { id } = await UserModel.create({ email, username, password });
    return { email, id, username };
  };

  findById = async (_id) =>
    await UserModel.findOne({
      _id,
    })
      .select("-password -friends")
      .exec();

  findUsersFriends = async (_id) => {
    const { friends = [] } = await UserModel.findOne({
      _id,
    })
      .populate("friends", "id username")
      .select("friends");
    return friends;
  };

  findUsersFriendById = async (_id, friend) => {
    const user = await UserModel.findOne(
      {
        _id,
        friends: mongoose.Types.ObjectId(friend),
      },
      { "friends.$": 1 }
    );
    return user?.friends;
  };

  addFriend = async (_id, friend) => {
    const user = await UserModel.updateOne(
      { _id },
      { $addToSet: { friends: friend } }
    );
    const newFriend = await this.findById(friend);
    this.emit("newFriend", { user_id: _id.toString(), friend: newFriend });
    return user;
  };

  removeFriend = async (_id, friend) => {
    const user = await UserModel.updateOne(
      { _id },
      { $pull: { friends: friend } }
    );

    this.emit("removedFriend", { user_id: _id, friend });

    return user;
  };

  searchFriends = async ({
    page = 0,
    limit = 10,
    searchByUsername,
    user_id,
  }) => {
    const { friends = [] } = await UserModel.findById(user_id);

    const [result = { users: [], count: 0 }] = await UserModel.aggregate([
      {
        $lookup: {
          from: "friendshiprequests",
          localField: "_id",
          foreignField: "receiver",
          as: "request",
        },
      },
      {
        $match: {
          $or: [
            { "request.sender": { $ne: mongoose.Types.ObjectId(user_id) } },
            {
              "request.sender": mongoose.Types.ObjectId(user_id),
              "request.status": { $ne: "pending" },
            },
          ],
        },
      },
      {
        $match: {
          username: { $regex: searchByUsername },
          _id: {
            $ne: mongoose.Types.ObjectId(user_id),
            $nin: friends.map((id) => mongoose.Types.ObjectId(id)),
          },
        },
      },
      {
        $facet: {
          users: [
            { $project: { password: false, email: false } },
            { $skip: page * limit },
            { $limit: limit },
          ],
          totalCount: [{ $group: { _id: null, count: { $sum: 1 } } }],
        },
      },
      { $unwind: "$totalCount" },
      {
        $project: {
          count: "$totalCount.count",
          users: "$users",
        },
      },
    ]);
    return result;
  };

  searchParticipants = async ({
    page = 0,
    limit = 10,
    searchByUsername,
    room_id,
    user_id,
  }) => {
    const { participants = [] } = await RoomModel.findById(room_id);

    const [result = { users: [], count: 0 }] = await UserModel.aggregate([
      {
        $lookup: {
          from: "roominvites",
          localField: "_id",
          foreignField: "receiver",
          as: "invite",
        },
      },
      {
        $match: {
          $or: [
            { "invite.sender": { $ne: mongoose.Types.ObjectId(user_id) } },
            { "invite.room": { $ne: room_id } },
            {
              "invite.sender": mongoose.Types.ObjectId(user_id),
              "invite.status": { $ne: "pending" },
              "invite.room": room_id,
            },
          ],
        },
      },
      {
        $match: {
          username: { $regex: searchByUsername },
          _id: {
            $ne: mongoose.Types.ObjectId(user_id),
            $nin: participants.map(({ user }) =>
              mongoose.Types.ObjectId(user._id)
            ),
          },
        },
      },
      {
        $facet: {
          users: [
            { $project: { password: false, email: false } },
            { $skip: page * limit },
            { $limit: limit },
          ],
          totalCount: [{ $group: { _id: null, count: { $sum: 1 } } }],
        },
      },
      { $unwind: "$totalCount" },
      {
        $project: {
          count: "$totalCount.count",
          users: "$users",
        },
      },
    ]);
    return result;
  };
}

const userService = new UserService();
module.exports = userService;
