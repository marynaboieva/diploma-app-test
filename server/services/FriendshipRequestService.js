const { EventEmitter } = require("events");

const mongoose = require("mongoose");
const FriendshipRequestModel = require("../models/FriendshipRequest");

class FriendshipRequestService extends EventEmitter {
  constructor() {
    super();
  }
  create = async (sender, receiver) => {
    const request = await FriendshipRequestModel.findOneAndUpdate(
      {
        sender,
        receiver,
      },
      { sender, receiver, status: "pending", created: new Date() },
      {
        upsert: true,
        setDefaultsOnInsert: true,
        useFindAndModify: false,
        new: true,
      }
    )
      .populate("sender", "id username")
      .populate("receiver", "id username")
      .exec();

    this.emit("newFriendRequest", { receiver_id: receiver, request });

    return request;
  };

  findById = async (_id) => {
    return await FriendshipRequestModel.findById(
      _id,
    ).exec();
  };

  findByUser = async (user_id) => {
    return await FriendshipRequestModel.find({
      status: "pending",
      $or: [
        { sender: mongoose.Types.ObjectId(user_id) },
        { receiver: mongoose.Types.ObjectId(user_id) },
      ],
    })
      .populate("sender", "id username")
      .populate("receiver", "id username")
      .select({
        sender: 1,
        receiver: 1,
        status: 1,
        created: 1,
      })
      .exec();
  };

  acceptRequest = async (_id) => {
    const request = await FriendshipRequestModel.findByIdAndUpdate(
       _id ,
      { status: "accepted" }
    ).exec();
    this.emit("acceptedFriendRequest", {_id:request._id.toString(), sender:request.sender.toString()});
    return request;
  };

  declineRequest = async (_id) => {
    const request = await FriendshipRequestModel.findByIdAndUpdate(
      _id ,
      { status: "declined" }
    );
    this.emit("declinedFriendRequest", {_id:request._id.toString(), sender:request.sender.toString()});
    return request;
  };

  cancelRequest = async (_id) => {
    const request = await FriendshipRequestModel.findOneAndDelete({
      _id,
    });
    this.emit("canceledFriendRequest", {_id:request._id.toString(), receiver:request.receiver.toString()});

    return request;
  };
}

const friendshipRequestService = new FriendshipRequestService();
module.exports = friendshipRequestService;
