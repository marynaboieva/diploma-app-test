const { EventEmitter } = require("events");

const RoomModel = require("../models/Room");

const mongoose = require("mongoose");
const UserService = require("./UserService");
const ChatService = require("./ChatService");

class RoomService extends EventEmitter {
  constructor() {
    super();
  }
  findById = async (_id) => {
    const [room] = await RoomModel.aggregate([
      { $match: { _id, isClosed: false } },
      {
        $lookup: {
          from: "users",
          localField: "created_by",
          foreignField: "_id",
          as: "created_by",
        },
      },
      {
        $lookup: {
          from: "users",
          let: {
            participants: "$participants",
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $in: ["$_id", "$$participants.user"],
                },
              },
            },
          ],
          as: "participants",
        },
      },
      {
        $project: {
          created: 1,
          created_by: { $arrayElemAt: ["$created_by", 0] },
          title: 1,
          participants: {
            _id: 1,
            username: 1,
          },
        },
      },
      {
        $project: {
          created: 1,
          created_by: { username: 1, _id: 1 },
          title: 1,
          participants: {
            _id: 1,
            username: 1,
          },
        },
      },
    ]).exec();

    return room;
  };

  findByUser = async (user_id) => {
    return await RoomModel.aggregate([
      {
        $match: {
          isClosed: false,
          participants: {
            $elemMatch: { user: mongoose.Types.ObjectId(user_id) },
          },
        },
      },

      {
        $lookup: {
          from: "chats",
          let: { chats: "$chat" },
          pipeline: [
            { $match: { $expr: { $eq: ["$_id", "$$chats"] } } },
            {
              $lookup: {
                from: "messages",
                let: { messages: "$messages" },
                pipeline: [
                  {
                    $match: {
                      $and: [
                        {
                          $expr: { $in: ["$_id", "$$messages"] },
                        },
                        { author: { $ne: mongoose.Types.ObjectId(user_id) } },
                        {
                          read: {
                            $nin: [mongoose.Types.ObjectId(user_id), "$read"],
                          },
                        },
                      ],
                    },
                  },
                ],
                as: "messages",
              },
            },
            { $addFields: { messages: { $size: "$messages" } } },
          ],
          as: "chats",
        },
      },
      {
        $project: {
          chats: { $arrayElemAt: ["$chats", 0] },
          title: 1,
          created_by: 1,
          created: 1,
          participants: { $size: "$participants" },
        },
      },
      {
        $project: {
          title: 1,
          created_by: 1,
          created: 1,
          participants: 1,
          messages: "$chats.messages",
        },
      },
    ]).exec();
  };

  createRoom = async ({ title, created_by }) => {
    const { id, created } = await new RoomModel({
      title,
      created_by,
    }).save();
    await this.addParticipant(id, created_by);
    return { _id: id, title, created_by, created, participants: 1 };
  };

  deleteRoom = async (_id) => {
    await RoomModel.updateOne({ _id }, { isClosed: true });
    this.emit("roomClosed", { room_id: _id });
    return;
  };

  addParticipant = async (_id, user_id) => {
    const room = await RoomModel.findByIdAndUpdate(
      _id,
      { $addToSet: { participants: [{ user: user_id }] } },
      { new: true }
    ).exec();
    const user = await UserService.findById(user_id);
    this.emit("participantJoined", {
      room,
      user: {
        _id: user_id,
        username: user.username,
      },
    });
    return;
  };

  removeParticipant = async (_id, user) => {
    await RoomModel.updateOne({ _id }, { $pull: { participants: { user } } });
    this.emit("participantLeft", { room_id: _id, user_id: user });

    return;
  };

  getChatMessages = async (_id, { page, limit, user_id }) => {
    const room = await RoomModel.findById(_id).exec();

    if (!room?.chat) {
      return { messages: [], count: 0 };
    }
    const [{ participant }] = await RoomModel.aggregate([
      { $match: { _id } },
      {
        $project: {
          participant: {
            $arrayElemAt: [
              {
                $filter: {
                  input: "$participants",
                  as: "participant",
                  cond: {
                    $eq: [
                      "$$participant.user",
                      mongoose.Types.ObjectId(user_id),
                    ],
                  },
                },
              },
              0,
            ],
          },
        },
      },
    ]);
    const messages = await ChatService.getMessages(room.chat, {
      page,
      limit,
      user_id,
      date: participant.joined,
    });
    return messages;
  };

  sendChatMessage = async (_id, { author, body }) => {
    const room = await RoomModel.findById(_id).exec();

    let chatId = room.chat;
    if (!chatId) {
      const chat = await ChatService.createChat();
      chatId = chat._id;

      await RoomModel.updateOne({ _id }, { chat: chat._id });
    }
    const message = await ChatService.addMessage(chatId, { author, body });
    this.emit("newChatMessage", { room_id: _id, message });
    return message;
  };

  getChatMessageById = async (_id, messageId) => {
    const room = await RoomModel.findById(_id).exec();
    const chatId = room.chat;
    if (!chatId) {
      throw Error("Message not found for the room");
    }
    const message = await ChatService.getMessageById(messageId);
    return message;
  };

  removeChatMessage = async (_id, messageId) => {
    const room = await RoomModel.findById(_id).exec();
    const chatId = room.chat;
    if (!chatId) {
      throw Error("Message not found for the room");
    }
    await ChatService.removeMessage(chatId, messageId);
    this.emit("deletedChatMessage", { room_id: _id, messageId });
    return;
  };

  editChatMessage = async (_id, messageId, body) => {
    const room = await RoomModel.findById(_id).exec();
    const chatId = room.chat;
    if (!chatId) {
      throw Error("Message id not found for the room");
    }
    const message = await ChatService.editMessage(chatId, messageId, body);
    this.emit("editedChatMessage", { room_id: _id, message });
    return message;
  };

  markChatMessageAsRead = async (_id, messageId, userId) => {
    const room = await RoomModel.findById(_id).exec();
    const chatId = room.chat;
    if (!chatId) {
      throw Error("Message id not found for the room");
    }
    const message = await ChatService.markAsRead(chatId, messageId, userId);
    return {
      _id: message._id,
      body: message.body,
      author: message.author,
      read: true,
    };
  };
}

const roomService = new RoomService();
module.exports = roomService;
