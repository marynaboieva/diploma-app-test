const { EventEmitter } = require("events");

const DialogModel = require("../models/Dialog");

const mongoose = require("mongoose");
const UserService = require("./UserService");
const ChatService = require("./ChatService");

class DialogService extends EventEmitter {
  constructor() {
    super();
  }
  findById = async (_id) => {
    return await DialogModel.findOne({ _id, isClosed: false })
      .populate("participants", "id username ")
      .exec();
  };

  findByUser = async (user_id) => {
    return await DialogModel.aggregate()
      .match({
        participants: mongoose.Types.ObjectId(user_id),
      })
      .project({
        created: 1,
        participants: 1,
        chat: { $size: "$chat" },
      })
      .exec();
  };

  getMessages = async (_id, { page, limit }) => {
    const dialog = await DialogModel.findById(_id).exec();
    if (!dialog.chat) {
      return [];
    }
    const messages = ChatService.getMessages(dialog.chat, {
      page,
      limit,
    });
    return messages;
  };

  sendMessage = async (_id, { author, body }) => {
    const room = await DialogModel.findById(_id).exec();
    let chatId = room.chat;
    if (!chatId) {
      const chat = await ChatService.createChat();
      chatId = chat._id;
    }
    const message = await ChatService.addMessage(chatId, { author, body });
    this.emit("newMessage", { room_id: _id, message });
    return message;
  };

  removeMessage = async (_id, messageId) => {
    const room = await DialogModel.findById(_id).exec();
    const chatId = room.chat;
    if (!chatId) {
      throw Error("Chat id not found for the room");
    }
    await ChatService.removeMessage(chatId, messageId);
    this.emit("deletedMessage", { room_id: _id, messageId });
    return;
  };
}

const DialogService = new DialogService();
module.exports = DialogService;
