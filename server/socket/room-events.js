const { disconnect } = require("./helpers");

const RoomService = require("../services/RoomService");
const RoomInviteService = require("../services/RoomInviteService");
const conferenceManager = require("../repositories/ConferenceManager");

module.exports = (io) => {
  RoomService.on("roomClosed", ({ room_id }) => {
    io.to(`room-${room_id}`).emit("roomClosed", { room_id });
    io.socketsLeave(`room-${room_id}`);
    console.log("disconnect everyone from deleted room", room_id);
    conferenceManager.closeRoom(room_id);
    io.of(`/room-${room_id}`).disconnectSockets();
  });

  RoomService.on("participantJoined", async ({ room, user }) => {
    await io.in(user._id).socketsJoin(`room-${room._id}`);
    io.to(`room-${room._id}`).except(user._id).emit("participantJoined", {
      user,
      room_id: room._id,
    });

    const peers = await conferenceManager.getPeers(room._id);
    const sockets = await io.in(`room-${room._id}`).fetchSockets();

    io.to(user._id).emit("newRoom", {
      _id: room._id,
      title: room.title,
      streaming: peers?.length,
      online: sockets?.length,
      participants: room.participants.length,
    });
  });

  RoomService.on("participantLeft", async ({ room_id, user_id }) => {
    io.to(`room-${room_id}`).emit("participantLeft", {
      user_id,
      room_id,
    });
    await io.in(user_id).socketsLeave(`room-${room_id}`);
    disconnect(io.of(`/room-${room_id}`), user_id);
  });

  // Chat
  RoomService.on("newChatMessage", async ({ room_id, message }) => {
    io.to(`room-${room_id}`)
      .except(message.author.toString())
      .emit("newChatMessage", {
        message,
        room_id,
      });
  });

  RoomService.on("deletedChatMessage", async ({ room_id, messageId }) => {
    io.to(`room-${room_id}`).emit("deletedChatMessage", {
      messageId,
      room_id,
    });
  });

  RoomService.on("editedChatMessage", async ({ room_id, message }) => {
    io.to(`room-${room_id}`)
      .except(message.author.toString())
      .emit("editedChatMessage", {
        message,
        room_id,
      });
  });

  // Invites
  RoomInviteService.on("newInvite", async ({ invite, receiver_id }) => {
    io.to(receiver_id).emit("newInvite", invite);
  });

  RoomInviteService.on("canceledInvite", async ({ invite_id, receiver }) => {
    await io.in(receiver).emit("canceledInvite", { invite_id });
  });

  RoomInviteService.on("appeptedInvite", async ({ invite_id, sender }) => {
    await io.in(sender).emit("appeptedInvite", { invite_id });
  });

  RoomInviteService.on("declinedInvite", async ({ invite_id, sender }) => {
    await io.in(sender).emit("declinedInvite", { invite_id });
  });
};
