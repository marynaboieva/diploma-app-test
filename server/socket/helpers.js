const send = async (io, user_id, event, args) => {
  const sockets = await io.fetchSockets();
  for (const socket of sockets) {
    if (socket.handshake.user.user_id === user_id) {
      socket.emit(event, args);
    }
  }
};

const broadcast = async (io, user_id, event, args) => {
  const sockets = await io.fetchSockets();
  for (const socket of sockets) {
    if (socket.handshake.user.user_id !== user_id) {
      socket.emit(event, args);
    }
  }
};

const checkOnline = async (io, user_id) => {
  const sockets = await io.fetchSockets();
  for (const socket of sockets) {
    if (socket.handshake.user.user_id === user_id) {
      return true;
    }
  }
  return false;
};

const disconnect = async (io, user_id) => {
  const sockets = await io.fetchSockets();
  for (const socket of sockets) {
    if (socket.handshake.user.user_id === user_id) {
      socket.disconnect();
    }
  }
  return false;
};

module.exports = { send, broadcast, checkOnline, disconnect };
