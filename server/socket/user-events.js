const { checkOnline } = require("./helpers");

const FriendshipRequestService = require("../services/FriendshipRequestService");
const UserService = require("../services/UserService");

module.exports = (io) => {
  UserService.on("newFriend", async ({ user_id, friend }) => {
    const online = await checkOnline(io, friend._id.toString());

    io.in(user_id).emit("newFriend", {
      _id: friend._id,
      username: friend.username,
      online,
    });
  });

  UserService.on("removedFriend", async ({ user_id, friend }) => {
    io.in(user_id).emit("removedFriend", friend);
  });

  FriendshipRequestService.on(
    "newFriendRequest",
    async ({ receiver_id, request }) => {
      io.in(receiver_id).emit("newFriendRequest", request);
    }
  );

  FriendshipRequestService.on(
    "canceledFriendRequest",
    async ({ _id, receiver }) => {
      io.in(receiver).emit("canceledFriendRequest", _id);
    }
  );

  FriendshipRequestService.on(
    "acceptedFriendRequest",
    async ({ _id, sender }) => {
      io.in(sender).emit("acceptedFriendRequest", _id);
    }
  );
  FriendshipRequestService.on(
    "declinedFriendRequest",
    async ({ _id, sender }) => {
      io.in(sender).emit("declinedFriendRequest", _id);
    }
  );
};
