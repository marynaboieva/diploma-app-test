const { socketAuthMiddleware } = require("../middlewares/socket");
const { checkOnline, send } = require("./helpers");
const RoomService = require("../services/RoomService");
const conferenceManager = require("../repositories/ConferenceManager");
const userService = require("../services/UserService");
const roomEvents = require("./room-events");
const userEvents = require("./user-events");

module.exports = (io) => {
  roomEvents(io);
  userEvents(io);

  io.use(socketAuthMiddleware).on("connection", async (socket) => {
    const { user_id, username } = socket.handshake.user;

    socket.join(user_id);

    socket.on("getFriendStatus", async (_, callback) => {
      const usersFriends = await userService.findUsersFriends(user_id);
      if (!usersFriends) {
        return callback([]);
      }
      const friends = await Promise.all(
        usersFriends.map(async (user) => {
          const online = await checkOnline(io, user._id.toString());
          return {
            _id: user._id,
            username: user.username,
            online,
          };
        })
      );

      callback(friends);
    });

    socket.on("getRoomsStatus", async (_, callback) => {
      try {
        if (!usersRooms) {
          return callback([]);
        }
        const roomsOnline = [];
        for (const room of usersRooms) {
          let roomData = { ...room };
          const conference = conferenceManager.getById(room._id);
          if (conference) {
            // streaming users count
            const peers = conference.getPeers();
            roomData = { ...roomData, streaming: peers.length };
          }
          // online users count
          const sockets = await io.in(`room-${room._id}`).fetchSockets();
          roomsOnline.push({ ...roomData, online: sockets.length });
        }
        callback(roomsOnline);
      } catch (error) {
        callback({ error });
      }
    });

    socket.on("getRoomUsersStatus", async (room_id, callback) => {
      try {
        const room = await RoomService.findById(room_id);

        const peers = await conferenceManager.getPeers(room_id);

        const participants = await Promise.all(
          room.participants.map(async (user) => {
            const online = await checkOnline(io, user._id.toString());
            return {
              _id: user._id,
              username: user.username,
              streaming: peers?.some(
                (peer) => peer.user_id === user._id.toString()
              ),
              online,
            };
          })
        );
        callback(participants);
      } catch (error) {
        callback({ error });
      }
    });

    socket.on("join", async ({ room_id }, callback) => {
      try {
        const dbRoom = await RoomService.findById(room_id);
        if (!dbRoom) {
          return callback({ error: "Room not found" });
        }

        if (
          !dbRoom.participants.find((user) => user._id.toString() === user_id)
        ) {
          return callback({ error: "Failed to connect" });
        }

        const room = conferenceManager.getById(room_id);
        if (!room) {
          await conferenceManager.createRoom(room_id);
        } else if (room.getPeer(user_id)) {
          return callback({ error: "Failed to connect" });
        }

        await conferenceManager.addPeer(room_id, user_id, {
          username,
        });

        callback({ room_id });
        console.log(`${username} joined ${room_id}`);
      } catch (err) {
        console.log(err);
        callback({ error: "Failed to connect" });
      }
    });

    socket.on("disconnect", async () => {
      const { user_id } = socket.handshake.user;
      const usersRooms = await RoomService.findByUser(user_id);
      usersRooms.forEach(({ _id }) => {
        io.in(`room-${_id}`).emit("userOffline", { room_id: _id, user_id });
      });

      const usersFriends = await userService.findUsersFriends(user_id);

      usersFriends.forEach(({ _id }) => {
        send(io, _id, "userOffline", {
          user_id,
        });
      });
    });

    const usersRooms = await RoomService.findByUser(user_id);
    usersRooms.forEach(({ _id }) => {
      socket.join(`room-${_id}`);

      io.in(`room-${_id}`)
        .except(user_id)
        .emit("userOnline", {
          room_id: _id,
          user: {
            _id: user_id,
            username,
          },
        });
    });

    const usersFriends = await userService.findUsersFriends(user_id);

    usersFriends.forEach(({ _id }) => {
      send(io, _id, "userOnline", {
        user: {
          _id: user_id,
          username,
        },
      });
    });
  });
};
