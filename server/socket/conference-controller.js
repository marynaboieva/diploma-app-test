const conferenceManager = require("../repositories/ConferenceManager");
const { socketAuthMiddleware } = require("../middlewares/socket");
const { broadcast, send } = require("./helpers");

module.exports = (io) => {
  // subscription to conference events
  conferenceManager.on("newPeer", async ({ room_id, peer }) => {
    const { user_id, username } = peer;
    io.of("/")
      .in(`room-${room_id}`)
      .emit("newPeer", {
        user: { _id: user_id, username },
        room_id,
      });
  });

  conferenceManager.on("peerClosed", ({ room_id, user_id }) => {
    io.of("/").in(`room-${room_id}`).emit("peerClosed", {
      user_id,
      room_id,
    });
  });

  conferenceManager.on("activeSpeaker", ({ room_id, user_id, volume }) => {
    io.of("/").in(`room-${room_id}`).emit("activeSpeaker", {
      user_id,
      volume,
      room_id,
    });
  });

  conferenceManager.on(
    "newConsumer",
    async ({ room_id, producer_id, producer_user_id, source }) => {
      broadcast(io.of(`/room-${room_id}`), producer_user_id, "newConsumer", {
        producer_id,
        producer_user_id,
        source,
      });
    }
  );

  conferenceManager.on(
    "consumerClosed",
    async ({ room_id, consumer_id, user_id, producer_user_id }) => {
      send(io.of(`/room-${room_id}`), user_id, "consumerClosed", {
        consumer_id,
        user_id,
        producer_user_id,
      });
    }
  );

  conferenceManager.on(
    "consumerPaused",
    ({ room_id, consumer_id, user_id, producer_user_id }) => {
      send(io.of(`/room-${room_id}`), user_id, "consumerPaused", {
        consumer_id,
        user_id,
        producer_user_id,
      });
    }
  );

  conferenceManager.on(
    "consumerResumed",
    ({ room_id, consumer_id, user_id, producer_user_id }) => {
      send(io.of(`/room-${room_id}`), user_id, "consumerResumed", {
        consumer_id,
        user_id,
        producer_user_id,
      });
    }
  );

  io.of(/^\/room-[A-Za-z0-9-_]{10}$/)
    .use(socketAuthMiddleware)
    .use((socket, next) => {
      const room_id = socket.nsp.name.replace("/room-", "");
      const { user_id } = socket.handshake.user;

      if (
        conferenceManager.getById(room_id) &&
        conferenceManager.getPeer(room_id, user_id)
      ) {
        next();
      } else {
        next(new Error("Failed to connect"));
      }
    })
    .on("connection", async (socket) => {
      const { user_id, username } = socket.handshake.user;
      const room_id = socket.nsp.name.replace("/room-", "");

      socket.on("getProducers", async (_, callback) => {
        console.log(`get producers for new peer ${username}`);
        const producerList = await conferenceManager.getProducersList(
          room_id,
          user_id
        );
        callback(producerList);
      });

      socket.on("getRouterRtpCapabilities", (_, callback) => {
        console.log(`get RouterRtpCapabilities ${username}`);
        try {
          callback(conferenceManager.getRtpCapabilities(room_id));
        } catch (e) {
          callback({
            error: e.message,
          });
        }
      });

      socket.on("createWebRtcTransport", async (_, callback) => {
        console.log(`create webrtc transport for ${user_id}`);
        try {
          const params = await conferenceManager.createWebRtcTransport(
            room_id,
            user_id
          );

          callback(params);
        } catch (err) {
          console.log(err);
          callback({
            error: err.message,
          });
        }
      });

      socket.on(
        "connectTransport",
        async ({ transport_id, dtlsParameters }, callback) => {
          console.log(`connect transport ${transport_id}`);

          await conferenceManager.connectTransport(room_id, user_id, {
            transport_id,
            dtlsParameters,
          });

          callback("success");
        }
      );

      socket.on("restartIce", async ({ transport_id }, callback) => {
        const iceParameters = await conferenceManager.restartIce(
          room_id,
          user_id,
          {
            transport_id,
          }
        );
        callback(iceParameters);
      });

      socket.on(
        "produce",
        async ({ kind, rtpParameters, transportId, appData }, callback) => {
          let producer_id = await conferenceManager.produce(room_id, user_id, {
            transportId,
            rtpParameters,
            kind,
            appData,
          });
          console.log(
            `produce type: ${appData.source} name: ${socket.handshake.user.username} id: ${producer_id}`
          );
          callback({
            producer_id,
          });
        }
      );

      socket.on(
        "consume",
        async (
          { consumerTransportId, producerId, rtpCapabilities },
          callback
        ) => {
          try {
            let { params, producer_user_id } = await conferenceManager.consume(
              room_id,
              user_id,
              {
                consumerTransportId,
                producerId,
                rtpCapabilities,
              }
            );

            console.log(
              `consuming produser_id:${producerId} consumer_id:${params.id}`
            );
            callback({ ...params, producer_user_id });
          } catch (error) {
            callback({ error });
          }
        }
      );

      socket.on("disconnect", async () => {
        await conferenceManager.removePeer(room_id, user_id);
        console.log(
          `disconnect ${socket.handshake.user.username} from ${room_id}`
        );
        const peers = await conferenceManager.getPeers(room_id);
        if (!peers?.length) {
          console.log("closing empty room ", room_id);
          conferenceManager.closeRoom(room_id);
        }
      });

      socket.on("closeProducer", ({ producer_id }) => {
        console.log(`producer close name: ${socket.handshake.user.username}`);
        conferenceManager.closeProducer(room_id, user_id, {
          producer_id,
        });
      });

      socket.on("pauseProducer", ({ producer_id }) => {
        conferenceManager.pauseProducer(room_id, user_id, {
          producer_id,
        });
      });

      socket.on("resumeProducer", ({ producer_id }) => {
        conferenceManager.resumeProducer(room_id, user_id, {
          producer_id,
        });
      });

      socket.on("pauseConsumer", ({ consumer_id }) => {
        conferenceManager.pauseConsumer(room_id, user_id, {
          consumer_id,
        });
      });

      socket.on("resumeConsumer", ({ consumer_id }) => {
        conferenceManager.resumeConsumer(room_id, user_id, {
          consumer_id,
        });
      });
    });
};
