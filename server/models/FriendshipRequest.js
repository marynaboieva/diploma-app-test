const mongoose = require("mongoose");

const FriendshipRequestSchema = new mongoose.Schema({
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  receiver: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  created: { type: Date, default: new Date() },
  status: {
    type: String,
    enum: ["pending", "accepted", "declined"],
    default: "pending",
  },
});

const FriendshipRequest = mongoose.model(
  "FriendshipRequest",
  FriendshipRequestSchema
);
module.exports = FriendshipRequest;
