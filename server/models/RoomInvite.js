const mongoose = require("mongoose");

const RoomInviteSchema = new mongoose.Schema({
  receiver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  sender: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  room: {
    type: String,
    ref: "Room",
  },
  created: { type: Date, default: new Date() },
  status: {
    type: String,
    enum: ["pending", "accepted", "declined"],
    default: "pending",
  },
});

const RoomInvite = mongoose.model("RoomInvite", RoomInviteSchema);
module.exports = RoomInvite;
