const mongoose = require("mongoose");

const DialogSchema = new mongoose.Schema({
  created: { type: Date, default: new Date() },
  participants: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  ],
  chat: { type: mongoose.Schema.Types.ObjectId, ref: "Chat" },
});

const Dialog = mongoose.model("Dialog", DialogSchema);
module.exports = Dialog;
