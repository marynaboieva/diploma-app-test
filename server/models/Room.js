const mongoose = require("mongoose");

const { customAlphabet } = require("nanoid");
const alphabet =
  "_-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
const nanoid = customAlphabet(alphabet, 10);

const RoomSchema = new mongoose.Schema({
  _id: {
    type: String,
    default: () => nanoid(),
  },
  title: {
    type: String,
    default: function () {
      return `Room ${this._id}`;
    },
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  created: { type: Date, default: new Date() },
  isClosed: { type: Boolean, default: false },
  participants: [
    {
      joined: { type: Date, default: new Date() },
      user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    },
  ],
  chat: { type: mongoose.Schema.Types.ObjectId, ref: "Chat" },
});

const Room = mongoose.model("Room", RoomSchema);
module.exports = Room;
