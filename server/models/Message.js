const mongoose = require("mongoose");

const MessageSchema = new mongoose.Schema({
  body: { type: String },
  created: { type: Date, default: new Date() },
  edited: { type: Boolean, default: false },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  read: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }],
});

const Message = mongoose.model("Message", MessageSchema);
module.exports = Message;
