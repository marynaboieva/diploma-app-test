const mediasoup = require("mediasoup");
const config = require("./config");


// all mediasoup workers
let workers = [];
let nextMediasoupWorkerIdx = 0;

async function createWorkers() {
    let { numWorkers } = config.mediasoup;

    for (let i = 0; i < numWorkers; i++) {
        let worker = await mediasoup.createWorker({
            logLevel: config.mediasoup.worker.logLevel,
            logTags: config.mediasoup.worker.logTags,
            rtcMinPort: config.mediasoup.worker.rtcMinPort,
            rtcMaxPort: config.mediasoup.worker.rtcMaxPort,
        });

        worker.on("died", () => {
            console.log(
                "mediasoup worker died, exiting in 2 seconds... [pid:%d]",
                worker.pid
            );
            setTimeout(() => process.exit(1), 2000);
        });
        workers.push(worker);
    }
}

/**
 * Get next mediasoup Worker.
 */
function getMediasoupWorker() {
    const worker = workers[nextMediasoupWorkerIdx];

    if (++nextMediasoupWorkerIdx === workers.length) nextMediasoupWorkerIdx = 0;

    return worker;
}

module.exports ={getMediasoupWorker, createWorkers};