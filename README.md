This project contains 2 folders:

- server

- client

Clone this repository, then open the folders in terminal and install dependencies using **`npm i`**. Folow the instructions in their **README** files or have a look on `scripts` section in **package.json**